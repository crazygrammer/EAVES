<?php date_default_timezone_set('Asia/Manila'); ?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#">
                    <em class="fa fa-home"></em>
                </a></li>
            <li class="active">Employee Attendance</li>
        </ol>
    </div><!--/.row-->

   

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Employee Attendance
                    <div class="pull-right"> <span id='ct' ></span></div>
                </div>
                <a href="" class="btn btn-primary" id="btn_print">Print</a>
                <div class="panel-body">
                    <div id="printThis">
                    <table class="table table-striped table-bordered table-responsive" cellspacing="0" width="100%"
                           id="rooms">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Employee Name</th>
                            <th>Time IN</th>
                            <th>Time OUT</th>
                            <?php if ($_SESSION['restrictions'] == 0) : ?>
                            <th>Action</th>
                            <?php endif ?>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $query      = "SELECT * FROM user";
                        $result     = mysqli_query($connection, $query);
                        $res        = mysqli_fetch_assoc($result);
                        $useID      = $_SESSION['user_id'];
                        $restrict   = $_SESSION['restrictions'];

                        if($restrict == 0){

                            $attendanceQuery = "
                                        SELECT 
                                            a.id, 
                                            a.employee_id,
                                            a.time_in,
                                            a.time_out,
                                            a.present,
                                            u.name,
                                            u.user_restrictions
                                        FROM 
                                            attendance a 
                                        LEFT JOIN 
                                            user u ON u.id = a.employee_id
                                        WHERE
                                            a.employee_id = $useID
                                        ";
                            $att_result = mysqli_query($connection, $attendanceQuery);

                            if (mysqli_num_rows($att_result) > 0) {
                                while ($attendance = mysqli_fetch_assoc($att_result)) { ?>
                                    <tr>

                                        <td><?php echo $attendance['id']; ?></td>
                                        <td><?php echo $attendance['name']; ?></td>
                                        <!-- <td><?php //echo date('F j, Y, g:i a', strtotime($attendance['time_in'])); ?></td> -->
                                        <td><?php echo date('F j, Y, g:i a', strtotime($attendance['time_in'])); ?></td>

                                        <td><?php if(!empty($attendance['time_out'])){
                                                echo date('F j, Y, g:i a', strtotime($attendance['time_out'])); 
                                        }?> </td>

                                        <?php if($attendance['present'] == 0) {?>
                                        <td>
                                        <form role="form" id="time_out" method="POST" data-toggle="validator">
                                            <input type="hidden" id="timeOut" value="<?php echo $attendance['id']; ?>">
                                            <button type="submit" class="btn btn-danger" style="border-radius:0%" data-id="<?php echo $attendance['id']; ?>" id="time_Out" onclick="alertyy()">Time Out</button>
                                        </form>
                                        </td>
                                        <?php }else { ?>
                                            <td>PRESENT</td>
                                        <?php } ?>
                                    </tr>
                                    <?php
                                }
                            }
                        } else {
                            $attendanceQuery2 = "
                                        SELECT 
                                            a.id, 
                                            a.employee_id,
                                            a.time_in,
                                            a.time_out,
                                            u.name,
                                            u.user_restrictions
                                        FROM 
                                            attendance a 
                                        LEFT JOIN 
                                            user u ON u.id = a.employee_id
                                        ";
                            $att_result2 = mysqli_query($connection, $attendanceQuery2);

                            if (mysqli_num_rows($att_result2) > 0) {
                                while ($attendance2 = mysqli_fetch_assoc($att_result2)) { ?>
                                    <tr>

                                        <td><?php echo $attendance2['id']; ?></td>
                                        <td><?php echo $attendance2['name']; ?></td>
                                        <td><?php echo date('F j, Y, g:i a', strtotime($attendance2['time_in'])); ?></td>
                                        <td><?php echo $attendance2['time_out']; ?></td>
                                    </tr>
                                    <?php
                                }
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
        <p class="back-link"></p>
        </div>
    </div>

</div>    <!--/.main-->

<script>
function alertyy() {
    window.location.reload();
}

function samp() {
    var timeout_id = $('#timeOut').val();

    alert(timeout_id);
}
</script>