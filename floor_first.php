<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">2nd Floor</li>
				<div class="pull-right"> <span id='ct' ></span></div>
			</ol>
		</div><!--/.row-->

		<div class="panel panel-container">
			<div class="row">
					<?php
						$room_one_query = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 1";
						$rooms_one_result = mysqli_query($connection, $room_one_query); 
					?>
					<?php if (mysqli_num_rows($rooms_one_result) > 0) {
                        	while ($rooms_one = mysqli_fetch_assoc($rooms_one_result)) { 
					?>
					<?php 	if ($rooms_one['status']  == 0) {	

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-blue"></em>
												<a class="btn btn-primary btn-lg" href="index.php?reservation&amp;room_id=1&amp;room_type_id=1">Room 201</a>
											</div>
										</div>
									  </div>';
							}else{

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-red"></em>
												<button class="btn btn-danger btn-lg" id="extendBtn" data-id="" data-toggle="modal" data-target="#extendHrs" href="">Room 201</button>
											</div>
										</div>
									  </div>';
							}
					?>
					<?php } } ?>

					<?php
						$room_two_query = "
                        SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					
					WHERE 
						deleteStatus = 0 AND a.room_id = 2";
						$rooms_two_result = mysqli_query($connection, $room_two_query); 
					?>
					<?php if (mysqli_num_rows($rooms_two_result) > 0) {
                        	while ($rooms_two = mysqli_fetch_assoc($rooms_two_result)) { 
					?>
					<?php 	if ($rooms_two['status']  == 0) {	

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-blue"></em>
												<a class="btn btn-primary btn-lg" href="index.php?reservation&amp;room_id=2&amp;room_type_id=1">Room 202</a>
											</div>
										</div>
									  </div>';
							}else{

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-red"></em>
												<button class="btn btn-danger btn-lg" id="extendBtnTwo" data-id="" data-toggle="modal" data-target="#extendHrsTwo" href="">Room 202</button>
											</div>
										</div>
									  </div>';
							}
					?>
					<?php } } ?>

					<?php
						$room_three_query = "
                        SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					
					WHERE 
						deleteStatus = 0 AND a.room_id = 3";
						$rooms_three_result = mysqli_query($connection, $room_three_query); 
					?>
					<?php if (mysqli_num_rows($rooms_three_result) > 0) {
                        	while ($rooms_three = mysqli_fetch_assoc($rooms_three_result)) { 
					?>
					<?php 	if ($rooms_three['status']  == 0) {	

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-blue"></em>
												<a class="btn btn-primary btn-lg" href="index.php?reservation&amp;room_id=3&amp;room_type_id=1">Room 203</a>
											</div>
										</div>
									  </div>';
							}else{

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-red"></em>
												<button class="btn btn-danger btn-lg" id="extendBtnThree" data-id="" data-toggle="modal" data-target="#extendHrsThree" href="">Room 203</button>
											</div>
										</div>
									  </div>';
							}
					?>
					<?php } } ?>
			</div>
			<div class="row">
					<?php
						$room_four_query = "
                        SELECT 
                            a.room_id,
                            a.room_type_id,
                            a.room_no,
                            a.status,
                            a.check_in_status,
                            a.check_out_status,
                            a.deleteStatus,
                            b.room_type_id,
                            b.room_type,
                            b.price,
                            b.max_person
                        FROM 
                            room a
                        LEFT JOIN 
                            room_type b ON b.room_type_id = a.room_type_id
                        WHERE 
                            deleteStatus = 0 AND room_id = 4";
						$rooms_four_result = mysqli_query($connection, $room_four_query); 
					?>
					<?php if (mysqli_num_rows($rooms_four_result) > 0) {
                        	while ($rooms_four = mysqli_fetch_assoc($rooms_four_result)) { 
					?>
					<?php 	if ($rooms_four['status']  == 0) {	

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-blue"></em>
												<a class="btn btn-primary btn-lg" href="index.php?reservation&amp;room_id=4&amp;room_type_id=1">Room 204</a>
											</div>
										</div>
									  </div>';
							}else{

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-red"></em>
												<button class="btn btn-danger btn-lg" id="extendBtnFour" data-id="" data-toggle="modal" data-target="#extendHrsFour" href="">Room 204</button>
											</div>
										</div>
									  </div>';
							}
					?>
					<?php } } ?>

					<?php
						$room_five_query = "
                        SELECT 
                            a.room_id,
                            a.room_type_id,
                            a.room_no,
                            a.status,
                            a.check_in_status,
                            a.check_out_status,
                            a.deleteStatus,
                            b.room_type_id,
                            b.room_type,
                            b.price,
                            b.max_person
                        FROM 
                            room a
                        LEFT JOIN 
                            room_type b ON b.room_type_id = a.room_type_id
                        WHERE 
                            deleteStatus = 0 AND room_id = 5";
						$rooms_five_result = mysqli_query($connection, $room_five_query); 
					?>
					<?php if (mysqli_num_rows($rooms_five_result) > 0) {
                        	while ($rooms_five = mysqli_fetch_assoc($rooms_five_result)) { 
					?>
					<?php 	if ($rooms_five['status']  == 0) {	

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-blue"></em>
												<a class="btn btn-primary btn-lg" href="index.php?reservation&amp;room_id=5&amp;room_type_id=1">Room 205</a>
											</div>
										</div>
									  </div>';
							}else{

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-red"></em>
												<button class="btn btn-danger btn-lg" id="extendBtnFive" data-id="" data-toggle="modal" data-target="#extendHrsFive" href="">Room 205</button>
											</div>
										</div>
									  </div>';
							}
					?>
					<?php } } ?>

					<?php
						$room_six_query = "
                        SELECT 
                            a.room_id,
                            a.room_type_id,
                            a.room_no,
                            a.status,
                            a.check_in_status,
                            a.check_out_status,
                            a.deleteStatus,
                            b.room_type_id,
                            b.room_type,
                            b.price,
                            b.max_person
                        FROM 
                            room a
                        LEFT JOIN 
                            room_type b ON b.room_type_id = a.room_type_id
                        WHERE 
                            deleteStatus = 0 AND room_id = 6";
						$rooms_six_result = mysqli_query($connection, $room_six_query); 
					?>
					<?php if (mysqli_num_rows($rooms_six_result) > 0) {
                        	while ($rooms_six = mysqli_fetch_assoc($rooms_six_result)) { 
					?>
					<?php 	if ($rooms_six['status']  == 0) {	

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-blue"></em>
												<a class="btn btn-primary btn-lg" href="index.php?reservation&amp;room_id=6&amp;room_type_id=1">Room 206</a>
											</div>
										</div>
									  </div>';
							}else{

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-red"></em>
												<button class="btn btn-danger btn-lg" id="extendBtnSix" data-id="" data-toggle="modal" data-target="#extendHrsSix" href="">Room 206</button>
											</div>
										</div>
									  </div>';
							}
					?>
					<?php } } ?>
			</div>
			<div class="row">
					<?php
						$room_seven_query = "
                        SELECT 
                            a.room_id,
                            a.room_type_id,
                            a.room_no,
                            a.status,
                            a.check_in_status,
                            a.check_out_status,
                            a.deleteStatus,
                            b.room_type_id,
                            b.room_type,
                            b.price,
                            b.max_person
                        FROM 
                            room a
                        LEFT JOIN 
                            room_type b ON b.room_type_id = a.room_type_id
                        WHERE 
                            deleteStatus = 0 AND room_id = 7";
						$rooms_seven_result = mysqli_query($connection, $room_seven_query); 
					?>
					<?php if (mysqli_num_rows($rooms_seven_result) > 0) {
                        	while ($rooms_seven = mysqli_fetch_assoc($rooms_seven_result)) { 
					?>
					<?php 	if ($rooms_seven['status']  == 0) {	

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-blue"></em>
												<a class="btn btn-primary btn-lg" href="index.php?reservation&amp;room_id=7&amp;room_type_id=1">Room 207</a>
											</div>
										</div>
									  </div>';
							}else{

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-red"></em>
												<button class="btn btn-danger btn-lg" id="extendBtnSeven" data-id="" data-toggle="modal" data-target="#extendHrsSeven" href="">Room 207</button>
											</div>
										</div>
									  </div>';
							}
					?>
					<?php } } ?>

					<?php
						$room_eight_query = "
                        SELECT 
                            a.room_id,
                            a.room_type_id,
                            a.room_no,
                            a.status,
                            a.check_in_status,
                            a.check_out_status,
                            a.deleteStatus,
                            b.room_type_id,
                            b.room_type,
                            b.price,
                            b.max_person
                        FROM 
                            room a
                        LEFT JOIN 
                            room_type b ON b.room_type_id = a.room_type_id
                        WHERE 
                            deleteStatus = 0 AND room_id = 8";
						$rooms_eight_result = mysqli_query($connection, $room_eight_query); 
					?>
					<?php if (mysqli_num_rows($rooms_seven_result) > 0) {
                        	while ($rooms_eight = mysqli_fetch_assoc($rooms_eight_result)) { 
					?>
					<?php 	if ($rooms_eight['status']  == 0) {	

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-blue"></em>
												<a class="btn btn-primary btn-lg" href="index.php?reservation&amp;room_id=8&amp;room_type_id=1">Room 208</a>
											</div>
										</div>
									  </div>';
							}else{

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-red"></em>
												<button class="btn btn-danger btn-lg" id="extendBtnEight" data-id="" data-toggle="modal" data-target="#extendHrsEight" href="">Room 208</button>
											</div>
										</div>
									  </div>';
							}
					?>
					<?php } } ?>

					<?php
						$room_nine_query = "
                        SELECT 
                            a.room_id,
                            a.room_type_id,
                            a.room_no,
                            a.status,
                            a.check_in_status,
                            a.check_out_status,
                            a.deleteStatus,
                            b.room_type_id,
                            b.room_type,
                            b.price,
                            b.max_person
                        FROM 
                            room a
                        LEFT JOIN 
                            room_type b ON b.room_type_id = a.room_type_id
                        WHERE 
                            deleteStatus = 0 AND room_id = 9";
						$rooms_nine_result = mysqli_query($connection, $room_nine_query); 
					?>
					<?php if (mysqli_num_rows($rooms_seven_result) > 0) {
                        	while ($rooms_nine = mysqli_fetch_assoc($rooms_nine_result)) { 
					?>
					<?php 	if ($rooms_nine['status']  == 0) {	

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-blue"></em>
												<a class="btn btn-primary btn-lg" href="index.php?reservation&amp;room_id=9&amp;room_type_id=1">Room 209</a>
											</div>
										</div>
									  </div>';
							}else{

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-red"></em>
												<button class="btn btn-danger btn-lg" id="extendBtnNine" data-id="" data-toggle="modal" data-target="#extendHrsNine" href="">Room 209</button>
											</div>
										</div>
									  </div>';
							}
					?>
					<?php } } ?>
			</div>
			<hr>

			<div class="row">
				<div class="col-xs-6 col-md-2 col-lg-2 no-padding">
					
				</div>

				<div class="col-xs-6 col-md-4 col-lg-4 no-padding">
					<div class="panel panel-red panel-widget border-right">
						<div class="row no-padding"><em class="fa fa-xl fa-money color-blue"></em>
							<div class="large">Php <?php include 'counters/count-firstfloor.php'?></div>
							<div class="text-muted">Total Earnings 2nd Floor</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>


<div id="extendHrs" class="modal fade" role="dialog">
        <div class="modal-dialog">
		<?php
						$sql = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person,
						c.check_out,
						c.payment_status,
						c.total_price,
						c.extend_hours,
						c.booking_date,
						c.booking_id
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					LEFT JOIN
						booking c ON c.room_id = a.room_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 1 AND c.payment_status = 0";
						$result = mysqli_query($connection, $sql);
						$fetch = mysqli_fetch_assoc($result);

					?>

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Extend</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h4 class="modal-dialog text-center">Room 201 is about to End, Do you want to Extend?</h4>
                            <form role="form" id="extendForm">
                                <div class="payment-response"></div>
                                <div class="form-group col-lg-12">
                                    <label>Extended Minutes</label>
                                    <input type="number" class="form-control" id="extend_hours"
                                           placeholder="Please Enter Minutes Here..">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Extend Price</label>
                                    <input type="number" class="form-control" id="extend_price"
                                           placeholder="Please Enter Amounts Here..">
                                </div>
                                <input type="hidden" id="getExtendId" value="<?php echo $fetch['booking_id'] ?>">
                                <button type="submit" class="btn btn-primary pull-right">Extend</button>
                                <!-- <button type="" class="btn btn-primary pull-right">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


	<div id="extendHrsTwo" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<?php
						$sqlTwo = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person,
						c.check_out,
						c.payment_status,
						c.total_price,
						c.extend_hours,
						c.booking_date,
						c.booking_id
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					LEFT JOIN
						booking c ON c.room_id = a.room_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 2 AND c.payment_status = 0";
						$resultTwo = mysqli_query($connection, $sqlTwo);
						$fetchTwo = mysqli_fetch_assoc($resultTwo);

					?>

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Extend</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h4 class="modal-dialog text-center">Room 202 is about to End, Do you want to Extend?</h4>
                            <form role="form" id="extendFormTwo">
                                <div class="payment-response"></div>
                                <div class="form-group col-lg-12">
                                    <label>Extended Minutes</label>
                                    <input type="number" class="form-control" id="extend_hours_two"
                                           placeholder="Please Enter Minutes Here..">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Extend Price</label>
                                    <input type="number" class="form-control" id="extend_price_two"
                                           placeholder="Please Enter Amounts Here..">
                                </div>
                                <input type="hidden" id="getExtendIdTwo" value="<?php echo $fetchTwo['booking_id'] ?>">
                                <button type="submit" class="btn btn-primary pull-right">Extend</button>
                                <!-- <button type="" class="btn btn-primary pull-right">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


	<div id="extendHrsThree" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<?php
						$sqlThree = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person,
						c.check_out,
						c.payment_status,
						c.total_price,
						c.extend_hours,
						c.booking_date,
						c.booking_id
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					LEFT JOIN
						booking c ON c.room_id = a.room_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 3 AND c.payment_status = 0";
						$resultThree = mysqli_query($connection, $sqlThree);
						$fetchThree = mysqli_fetch_assoc($resultThree);

					?>

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Extend</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h4 class="modal-dialog text-center">Room 203 is about to End, Do you want to Extend?</h4>
                            <form role="form" id="extendFormThree">
                                <div class="payment-response"></div>
                                <div class="form-group col-lg-12">
                                    <label>Extended Minutes</label>
                                    <input type="number" class="form-control" id="extend_hours_three"
                                           placeholder="Please Enter Minutes Here..">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Extend Price</label>
                                    <input type="number" class="form-control" id="extend_price_three"
                                           placeholder="Please Enter Amounts Here..">
                                </div>
                                <input type="hidden" id="getExtendIdThree" value="<?php echo $fetchThree['booking_id'] ?>">
                                <button type="submit" class="btn btn-primary pull-right">Extend</button>
                                <!-- <button type="" class="btn btn-primary pull-right">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


	<div id="extendHrsFour" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<?php
						$sqlFour = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person,
						c.check_out,
						c.payment_status,
						c.total_price,
						c.extend_hours,
						c.booking_date,
						c.booking_id
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					LEFT JOIN
						booking c ON c.room_id = a.room_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 4 AND c.payment_status = 0";
						$resultFour = mysqli_query($connection, $sqlFour);
						$fetchFour = mysqli_fetch_assoc($resultFour);

					?>

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Extend</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h4 class="modal-dialog text-center">Room 204 is about to End, Do you want to Extend?</h4>
                            <form role="form" id="extendFormFour">
                                <div class="payment-response"></div>
                                <div class="form-group col-lg-12">
                                    <label>Extended Minutes</label>
                                    <input type="number" class="form-control" id="extend_hours_four"
                                           placeholder="Please Enter Minutes Here..">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Extend Price</label>
                                    <input type="number" class="form-control" id="extend_price_four"
                                           placeholder="Please Enter Amounts Here..">
                                </div>
                                <input type="hidden" id="getExtendIdFour" value="<?php echo $fetchFour['booking_id'] ?>">
                                <button type="submit" class="btn btn-primary pull-right">Extend</button>
                                <!-- <button type="" class="btn btn-primary pull-right">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


	<div id="extendHrsFive" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<?php
						$sqlFive = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person,
						c.check_out,
						c.payment_status,
						c.total_price,
						c.extend_hours,
						c.booking_date,
						c.booking_id
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					LEFT JOIN
						booking c ON c.room_id = a.room_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 5 AND c.payment_status = 0";
						$resultFive = mysqli_query($connection, $sqlFive);
						$fetchFive = mysqli_fetch_assoc($resultFive);

					?>

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Extend</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h4 class="modal-dialog text-center">Room 205 is about to End, Do you want to Extend?</h4>
                            <form role="form" id="extendFormFive">
                                <div class="payment-response"></div>
                                <div class="form-group col-lg-12">
                                    <label>Extended Minutes</label>
                                    <input type="number" class="form-control" id="extend_hours_five"
                                           placeholder="Please Enter Minutes Here..">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Extend Price</label>
                                    <input type="number" class="form-control" id="extend_price_five"
                                           placeholder="Please Enter Amounts Here..">
                                </div>
                                <input type="hidden" id="getExtendIdFive" value="<?php echo $fetchFive['booking_id'] ?>">
                                <button type="submit" class="btn btn-primary pull-right">Extend</button>
                                <!-- <button type="" class="btn btn-primary pull-right">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


	<div id="extendHrsSix" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<?php
						$sqlSix = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person,
						c.check_out,
						c.payment_status,
						c.total_price,
						c.extend_hours,
						c.booking_date,
						c.booking_id
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					LEFT JOIN
						booking c ON c.room_id = a.room_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 6 AND c.payment_status = 0";
						$resultSix = mysqli_query($connection, $sqlSix);
						$fetchSix = mysqli_fetch_assoc($resultSix);

					?>

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Extend</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h4 class="modal-dialog text-center">Room 206 is about to End, Do you want to Extend?</h4>
                            <form role="form" id="extendFormSix">
                                <div class="payment-response"></div>
                                <div class="form-group col-lg-12">
                                    <label>Extended Minutes</label>
                                    <input type="number" class="form-control" id="extend_hours_six"
                                           placeholder="Please Enter Minutes Here..">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Extend Price</label>
                                    <input type="number" class="form-control" id="extend_price_six"
                                           placeholder="Please Enter Amounts Here..">
                                </div>
                                <input type="hidden" id="getExtendIdSix" value="<?php echo $fetchSix['booking_id'] ?>">
                                <button type="submit" class="btn btn-primary pull-right">Extend</button>
                                <!-- <button type="" class="btn btn-primary pull-right">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


	<div id="extendHrsSeven" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<?php
						$sqlSeven = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person,
						c.check_out,
						c.payment_status,
						c.total_price,
						c.extend_hours,
						c.booking_date,
						c.booking_id
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					LEFT JOIN
						booking c ON c.room_id = a.room_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 7 AND c.payment_status = 0";
						$resultSeven = mysqli_query($connection, $sqlSeven);
						$fetchSeven = mysqli_fetch_assoc($resultSeven);

					?>

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Extend</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h4 class="modal-dialog text-center">Room 207 is about to End, Do you want to Extend?</h4>
                            <form role="form" id="extendFormSeven">
                                <div class="payment-response"></div>
                                <div class="form-group col-lg-12">
                                    <label>Extended Minutes</label>
                                    <input type="number" class="form-control" id="extend_hours_seven"
                                           placeholder="Please Enter Minutes Here..">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Extend Price</label>
                                    <input type="number" class="form-control" id="extend_price_seven"
                                           placeholder="Please Enter Amounts Here..">
                                </div>
                                <input type="hidden" id="getExtendIdSeven" value="<?php echo $fetchSeven['booking_id'] ?>">
                                <button type="submit" class="btn btn-primary pull-right">Extend</button>
                                <!-- <button type="" class="btn btn-primary pull-right">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


	<div id="extendHrsEight" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<?php
						$sqlEight = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person,
						c.check_out,
						c.payment_status,
						c.total_price,
						c.extend_hours,
						c.booking_date,
						c.booking_id
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					LEFT JOIN
						booking c ON c.room_id = a.room_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 8 AND c.payment_status = 0";
						$resultEight = mysqli_query($connection, $sqlEight);
						$fetchEight = mysqli_fetch_assoc($resultEight);

					?>

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Extend</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h4 class="modal-dialog text-center">Room 208 is about to End, Do you want to Extend?</h4>
                            <form role="form" id="extendFormEight">
                                <div class="payment-response"></div>
                                <div class="form-group col-lg-12">
                                    <label>Extended Minutes</label>
                                    <input type="number" class="form-control" id="extend_hours_eight"
                                           placeholder="Please Enter Minutes Here..">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Extend Price</label>
                                    <input type="number" class="form-control" id="extend_price_eight"
                                           placeholder="Please Enter Amounts Here..">
                                </div>
                                <input type="hidden" id="getExtendIdEight" value="<?php echo $fetchEight['booking_id'] ?>">
                                <button type="submit" class="btn btn-primary pull-right">Extend</button>
                                <!-- <button type="" class="btn btn-primary pull-right">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


	<div id="extendHrsNine" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<?php
						$sqlNine = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person,
						c.check_out,
						c.payment_status,
						c.total_price,
						c.extend_hours,
						c.booking_date,
						c.booking_id
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					LEFT JOIN
						booking c ON c.room_id = a.room_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 9 AND c.payment_status = 0";
						$resultNine = mysqli_query($connection, $sqlNine);
						$fetchNine = mysqli_fetch_assoc($resultNine);

					?>

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Extend</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h4 class="modal-dialog text-center">Room 209 is about to End, Do you want to Extend?</h4>
                            <form role="form" id="extendFormNine">
                                <div class="payment-response"></div>
                                <div class="form-group col-lg-12">
                                    <label>Extended Minutes</label>
                                    <input type="number" class="form-control" id="extend_hours_nine"
                                           placeholder="Please Enter Minutes Here..">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Extend Price</label>
                                    <input type="number" class="form-control" id="extend_price_nine"
                                           placeholder="Please Enter Amounts Here..">
                                </div>
                                <input type="hidden" id="getExtendIdNine" value="<?php echo $fetchNine['booking_id'] ?>">
                                <button type="submit" class="btn btn-primary pull-right">Extend</button>
                                <!-- <button type="" class="btn btn-primary pull-right">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>