<?php
include_once 'db.php';
session_start();
date_default_timezone_set('Asia/Manila');

if (isset($_POST['login'])) {
    $email      = $_POST['email'];
    $password   = $_POST['password'];

    if (!$email && !$password) {
        header('Location:login.php?empty');
    } else {
        $password   = md5($password);
        $query      = "SELECT * FROM user WHERE username = '$email' AND password = '$password'";
        $result     = mysqli_query($connection, $query);

        if (mysqli_num_rows($result) == 1) {

            $user                       = mysqli_fetch_assoc($result);
            $_SESSION['username']       = $user['username'];
            $_SESSION['user_id']        = $user['id'];
            $_SESSION['name']           = $user['name'];
            $_SESSION['restrictions']   = $user['user_restrictions'];

            if($user['user_restrictions'] == 0){
                $employee_id    = $_SESSION['user_id'];
                $date           = date('Y-m-d');
                $query          = "SELECT * FROM attendance WHERE employee_id = '$employee_id' AND date = '$date'";
                $result         = mysqli_query($connection, $query);
                $check_rows     = mysqli_num_rows($result);
                if($check_rows>0){
                    echo "";
                }else{
                $empID      = $_SESSION['user_id'];
                $timeIn     = date('Y-m-d H:i');
                $attQuery   = "INSERT INTO attendance(employee_id,time_in,date) VALUES('$empID', '$timeIn','$date')";
                $res        = mysqli_query($connection, $attQuery);
                }

            }

            header('Location:index.php?dashboard');

        } else {

            header('Location:login.php?loginE');
        }

        
    }
}

if (isset($_POST['room_type'])) {

    $room_type_id   = $_POST['room_type_id'];
    $sql            = "SELECT * FROM room WHERE room_type_id = '$room_type_id' AND status IS NULL AND deleteStatus = '0'";
    $result         = mysqli_query($connection, $sql);

    if ($result) {

        echo "<option selected disabled>Select Room Type</option>";
        while ($room = mysqli_fetch_assoc($result)) {
            echo "<option value='" . $room['room_id'] . "'>" . $room['room_no'] . "</option>";
        }
    } else {
        echo "<option>No Available</option>";
    }
}


if (isset($_POST['add_room'])) {
    
    $room_type_id   = $_POST['room_type_id'];
    $room_no        = $_POST['room_no'];

    if ($room_no != '') {
        $sql = "SELECT * FROM room WHERE room_no = '$room_no'";
        if (mysqli_num_rows(mysqli_query($connection, $sql)) >= 1) {
            $response['done'] = false;
            $response['data'] = "Room No Already Exist";
        } else {
            $query = "INSERT INTO room (room_type_id,room_no) VALUES ('$room_type_id','$room_no')";
            $result = mysqli_query($connection, $query);

            if ($result) {
                $response['done'] = true;
                $response['data'] = 'Successfully Added Room';
            } else {
                $response['done'] = false;
                $response['data'] = "DataBase Error";
            }
        }
    } else {

        $response['done'] = false;
        $response['data'] = "Please Enter Room No";
    }

    echo json_encode($response);
}


// if (isset($_POST['time_IN'])) {

// }

if (isset($_POST['booking'])) {
    $room_id        = $_POST['room_id'];
    $additional     = $_POST['additional'];
    $room_price     = $_POST['room_price'];
    $add_price      = $_POST['add_price'];
    $check_in       = date('Y-m-d H:i', strtotime($_POST['check_in']));
    $check_out      = date('Y-m-d H:i', strtotime($_POST['check_out']));
    $total_price    = $_POST['total_price'];
    $name           = $_POST['name'];
    $contact_no     = $_POST['contact_no'];
    $email          = $_POST['email'];
    $address        = $_POST['address'];
    $date           = date('Y-m-d');
    $user_id        = $_SESSION['user_id'];

    $customer_sql       = "INSERT INTO customer (customer_name,contact_no,email,address) VALUES ('$name','$contact_no','$email','$address')";
    $customer_result    = mysqli_query($connection, $customer_sql);

    if ($customer_result) {
        $customer_id        = mysqli_insert_id($connection);
        $booking_sql        = "INSERT INTO booking (customer_id,room_id,additional,room_price,add_price,check_in,check_out,total_price,remaining_price,user_id,date) VALUES ('$customer_id','$room_id','$additional','$room_price','$add_price','$check_in','$check_out','$total_price','$total_price','$user_id','$date')";
        $booking_result     = mysqli_query($connection, $booking_sql);

        if ($booking_result) {
            $room_stats_sql = "UPDATE room SET status = '1' WHERE room_id = '$room_id'";

            if (mysqli_query($connection, $room_stats_sql)) {
                $response['done'] = true;
                $response['data'] = 'Successfully Booking';
            } else {
                $response['done'] = false;
                $response['data'] = "DataBase Error in status change";
            }
        } else {
            $response['done'] = false;
            $response['data'] = "DataBase Error booking";
        }
    } else {
        $response['done'] = false;
        $response['data'] = "DataBase Error add customer";
    }

    echo json_encode($response);
}


if (isset($_POST['cutomerDetails'])) {
    //$customer_result='';
    $room_id = $_POST['room_id'];

    if ($room_id != '') {

        $sql    = "SELECT * FROM room NATURAL JOIN room_type NATURAL JOIN booking NATURAL JOIN customer WHERE room_id = '$room_id' AND payment_status = '0'";
        $result = mysqli_query($connection, $sql);

        if ($result) {

            $customer_details               = mysqli_fetch_assoc($result);
            $response['done']               = true;
            $response['customer_id']        = $customer_details['customer_id'];
            $response['customer_name']      = $customer_details['customer_name'];
            $response['contact_no']         = $customer_details['contact_no'];
            $response['email']              = $customer_details['email'];
            $response['address']            = $customer_details['address'];
            $response['remaining_price']    = $customer_details['remaining_price'];
        } else {
            $response['done'] = false;
            $response['data'] = "DataBase Error";
        }

        echo json_encode($response);
    }
}

if (isset($_POST['editDetails'])) {
    //$customer_result='';
    $room_id = $_POST['room_id'];

    if ($room_id != '') {

        $sql    = "SELECT * FROM room NATURAL JOIN room_type NATURAL JOIN booking NATURAL JOIN customer WHERE room_id = '$room_id' AND payment_status = '0'";
        $result = mysqli_query($connection, $sql);

        if ($result) {

            $customer_details               = mysqli_fetch_assoc($result);
            $response['done']               = true;
            $response['customer_id']        = $customer_details['customer_id'];
            $response['customer_name']      = $customer_details['customer_name'];
            $response['check_in']           = $customer_details['check_in'];
            $response['check_out']          = $customer_details['check_out'];
            $response['additional']         = $customer_details['additional'];
            $response['add_price']          = $customer_details['add_price'];
            $response['room_price']          = $customer_details['room_price'];
            $response['total_price']          = $customer_details['total_price'];
            $response['booking_id']         = $customer_details['booking_id'];
        } else {
            $response['done'] = false;
            $response['data'] = "DataBase Error";
        }

        echo json_encode($response);
    }
}

if (isset($_POST['edit_info'])) {
    //$customer_result='';
    // $cus_name        = $_POST['cus_name'];
    $edit_adds_price = $_POST['edit_adds_price'];
    $edit_room_price = $_POST['edit_room_price'];
    $booking_edit_id = $_POST['booking_edit_id'];
    $total_price     = $_POST['total_price'];

    if ($booking_edit_id != '') {
        $date   = date('Y-m-d H:i:s');
        $sql    = "UPDATE booking SET add_price = '$edit_adds_price', room_price = '$edit_room_price', total_price = '$total_price', updated_date = '$date' WHERE booking_id = '$booking_edit_id'";
        $result = mysqli_query($connection, $sql);

        if ($result) {

            $response['done'] = true;
            $response['data'] = 'Successfully Update Room';
        } else {
            $response['done'] = false;
            $response['data'] = "DataBase Error";
        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error Booking ID";
    }

    echo json_encode($response);
}

if (isset($_POST['checkOutDetails'])) {
    //$customer_result='';
    $check_details = $_POST['check_details'];

    if ($check_details != '') {

        $sql    = "SELECT * FROM spa_sauna WHERE id = '$check_details'";
        $result = mysqli_query($connection, $sql);

        if ($result) {

            $customer_details               = mysqli_fetch_assoc($result);
            $response['done']               = true;
            // $response['customer_id']        = $customer_details['customer_id'];
            $response['customer_name']      = $customer_details['customer_name'];
            $response['additional']         = $customer_details['additional'];
            $response['sauna_price']        = $customer_details['sauna_price'];
            $response['add_price']          = $customer_details['add_price'];
            $response['check_in']           = $customer_details['check_in'];
            $response['check_out']          = $customer_details['check_out'];
            $response['total_price']        = $customer_details['total_price'];
        } else {
            $response['done'] = false;
            $response['data'] = "DataBase Error";
        }

        echo json_encode($response);
    }
}


if (isset($_POST['booked_room'])) {
    $room_id = $_POST['room_id'];

    $sql = "SELECT * FROM room NATURAL JOIN room_type NATURAL JOIN booking NATURAL JOIN customer WHERE room_id = '$room_id' AND payment_status = '0'";
    $result = mysqli_query($connection, $sql);
    if ($result) {

        $room                           = mysqli_fetch_assoc($result);
        $response['done']               = true;
        $response['booking_id']         = $room['booking_id'];
        $response['name']               = $room['customer_name'];
        $response['room_no']            = $room['room_no'];
        $response['room_type']          = $room['room_type'];
        // $response['check_in']           = date('F j, Y, g:i a', strtotime($room['check_in']));
        // $response['check_out']          = date('F j, Y, g:i a', strtotime($room['check_out']));
        $response['check_in']           = $room['check_in'];
        $response['check_out']          = $room['check_out'];
        $response['additional']         = $room['additional'];
        $response['room_price']         = $room['room_price'];
        $response['add_price']          = $room['add_price'];
        $response['total_price']        = $room['total_price'];
        $response['remaining_price']    = $room['remaining_price'];

    } else {

        $response['done'] = false;
        $response['data'] = "DataBase Error";
    }

    echo json_encode($response);
}


// if (isset($_POST['booked_sauna'])) {
//     $sauna_id = $_POST['sauna_id'];

//     $sql    = "SELECT * FROM spa_sauna WHERE id = $sauna_id";
//     $result = mysqli_query($connection, $sql);
//     if ($result) {

//         $spa                           = mysqli_fetch_assoc($result);
//         $response['done']               = true;
//         $response['id']                 = $spa['id'];
//         $response['name']               = $spa['customer_name'];
//         $response['additional']         = $spa['additional'];
//         $response['sauna_price']        = $spa['sauna_price'];
//         $response['add_price']          = $spa['add_price'];
//         // $response['check_in']           = date('F j, Y, g:i a', strtotime($room['check_in']));
//         // $response['check_out']          = date('F j, Y, g:i a', strtotime($room['check_out']));
//         $response['check_in']           = $spa['check_in'];
//         $response['check_out']          = $spa['check_out'];
//         $response['total_price']        = $spa['total_price'];

//     } else {

//         $response['done'] = false;
//         $response['data'] = "DataBase Error";
//     }

//     echo json_encode($response);
// }


if (isset($_POST['check_in_room'])) {

    $booking_id         = $_POST['booking_id'];
    $advance_payment    = $_POST['advance_payment'];

    if ($booking_id != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_id            = $booking_details['room_id'];
        $remaining_price    = $booking_details['total_price'] - $advance_payment;

        $updateBooking      = "UPDATE booking SET remaining_price = '$remaining_price' WHERE booking_id = '$booking_id'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

            $updateRoom     = "UPDATE room SET check_in_status = '1' WHERE room_id = '$room_id'";
            $updateResult   = mysqli_query($connection, $updateRoom);

            if ($updateResult) {

                $response['done'] = true;

            } else {

                $response['done'] = false;
                $response['data'] = "Problem in Update Room Check in status";
            }

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}


if (isset($_POST['check_out_room'])) {

    $booking_id         = $_POST['booking_id'];
    $remaining_amount   = $_POST['remaining_amount'];

    if ($booking_id != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_id            = $booking_details['room_id'];
        $remaining_price    = $booking_details['remaining_price'];

        if ($remaining_price == $remaining_amount) {

            $updateBooking  = "UPDATE booking SET remaining_price = '0',payment_status = '1' WHERE booking_id = '$booking_id'";
            $result         = mysqli_query($connection, $updateBooking);

            if ($result) {

                $updateRoom     = "UPDATE room SET status = NULL,check_in_status = '0',check_out_status = '1' WHERE room_id = '$room_id'";
                $updateResult   = mysqli_query($connection, $updateRoom);

                if ($updateResult) {

                    $response['done'] = true;

                } else {

                    $response['done'] = false;
                    $response['data'] = "Problem in Update Room Check in status";

                }

            } else {

                $response['done'] = false;
                $response['data'] = "Problem in payment";
            }

        } else {

            $response['done'] = false;
            $response['data'] = "Please Enter Full Payment";
        }
    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }
    
    echo json_encode($response);
}


if (isset($_POST['createComplaint'])) {
    $complainant_name   = $_POST['complainant_name'];
    $complaint_type     = $_POST['complaint_type'];
    $complaint          = $_POST['complaint'];
    $room_no            = $_POST['room_no'];

    $query      = "INSERT INTO complaint (complainant_name,complaint_type,complaint,room_no) VALUES ('$complainant_name','$complaint_type','$complaint', '$room_no')";
    $result     = mysqli_query($connection, $query);

    if ($result) {

        header("Location:index.php?complain&success");

    } else {

        header("Location:index.php?complain&error");
    }

}

if (isset($_POST['resolve_complaint'])) {
    $budget         = $_POST['budget'];
    $complaint_id   = $_POST['complaint_id'];
    $query          = "UPDATE complaint set budget = '$budget',resolve_status = '1' WHERE id='$complaint_id'";
    $result         = mysqli_query($connection, $query);

    if ($result) {

        header("Location:index.php?complain&resolveSuccess");

    } else {
        
        header("Location:index.php?complain&resolveError");
    }
}

if (isset($_POST['checkout_spa'])) {
    // $budget         = $_POST['budget'];
    $checkout_id   = $_POST['checkout_id'];
    $query          = "UPDATE spa_sauna set payment_status = 1 WHERE id = $checkout_id";
    $result         = mysqli_query($connection, $query);

    if ($result) {

        header("Location:index.php?spa_mang&successCheckout");

    } else {
        
        // header("Location:index.php?complain&resolveError");
    }
}




if (isset($_POST['extended_hours'])) {

    $booking_id         = $_POST['booking_id'];
    $extend_hours    	= $_POST['extend_hours'];
    $extend_price		= $_POST['extend_price'];

    if ($booking_id != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add = $extend_hours;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price;
        $total_price        = $total_amount + $extend_price;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}

if (isset($_POST['extend_spa'])) {
    $extend_minutes = $_POST['extend_minutes'];
    $spa_price      = $_POST['spa_price'];
    $extend_id      = $_POST['extend_id'];

    if ($extend_id != '') {
        $query          = "SELECT * FROM spa_sauna WHERE id = $extend_id";
        $result         = mysqli_query($connection, $query);
        $spa_details    = mysqli_fetch_assoc($result);
        $sauna_price    = $spa_details['sauna_price'];
        $spa_out        = $spa_details['check_out'];
        $total_spa      =$spa_details['total_price'];

        $minutes_to_add = $extend_minutes;
		$time = new DateTime($spa_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));
		$stamp = $time->format('Y-m-d H:i');

        $total 				= $sauna_price + $spa_price;
        $total_price        = $total_spa + $spa_price;
        // $remaining_price    = $booking_details['remaining_price'] + $extend_price_two;
        $updateBooking      = "UPDATE spa_sauna SET sauna_price = '$total', check_out = '$stamp', total_price = '$total_price' WHERE id = '$extend_id'";
        $result             = mysqli_query($connection, $updateBooking);

        header("Location:index.php?spa_mang&success");

    } else {
        header("Location:index.php?spa_mang&resolveError");
        // $response['done'] = false;
        // $response['data'] = "Error With Booking";
    }

    
    // echo json_encode($response);
}


if (isset($_POST['extended_hours_two'])) {

    $booking_id_two         = $_POST['booking_id_two'];
    $extend_hours_two    	= $_POST['extend_hours_two'];
    $extend_price_two		= $_POST['extend_price_two'];

    if ($booking_id_two != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id_two'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add = $extend_hours_two;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price_two;
        $total_price        = $total_amount + $extend_price_two;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price_two;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id_two'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}


if (isset($_POST['extended_hours_three'])) {

    $booking_id_three       = $_POST['booking_id_three'];
    $extend_hours_three    	= $_POST['extend_hours_three'];
    $extend_price_three		= $_POST['extend_price_three'];

    if ($booking_id_three != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id_three'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add = $extend_hours_three;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price_three;
        $total_price        = $total_amount + $extend_price_three;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price_three;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id_three'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}

if (isset($_POST['extended_hours_four'])) {

    $booking_id_four       = $_POST['booking_id_four'];
    $extend_hours_four    	= $_POST['extend_hours_four'];
    $extend_price_four		= $_POST['extend_price_four'];

    if ($booking_id_four != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id_four'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add     = $extend_hours_four;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price_four;
        $total_price        = $total_amount + $extend_price_four;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price_four;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id_four'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}

if (isset($_POST['extended_hours_five'])) {

    $booking_id_five       = $_POST['booking_id_five'];
    $extend_hours_five    	= $_POST['extend_hours_five'];
    $extend_price_five		= $_POST['extend_price_five'];

    if ($booking_id_five != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id_five'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add     = $extend_hours_five;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price_five;
        $total_price        = $total_amount + $extend_price_five;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price_five;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id_five'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}

if (isset($_POST['extended_hours_six'])) {

    $booking_id_six       = $_POST['booking_id_six'];
    $extend_hours_six    	= $_POST['extend_hours_six'];
    $extend_price_six		= $_POST['extend_price_six'];

    if ($booking_id_six != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id_six'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add     = $extend_hours_six;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price_six;
        $total_price        = $total_amount + $extend_price_six;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price_six;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id_six'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}

if (isset($_POST['extended_hours_seven'])) {

    $booking_id_seven       = $_POST['booking_id_seven'];
    $extend_hours_seven    	= $_POST['extend_hours_seven'];
    $extend_price_seven		= $_POST['extend_price_seven'];

    if ($booking_id_seven != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id_seven'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add     = $extend_hours_seven;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price_seven;
        $total_price        = $total_amount + $extend_price_seven;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price_seven;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id_seven'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}

if (isset($_POST['extended_hours_eight'])) {

    $booking_id_eight       = $_POST['booking_id_eight'];
    $extend_hours_eight    	= $_POST['extend_hours_eight'];
    $extend_price_eight		= $_POST['extend_price_eight'];

    if ($booking_id_eight != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id_eight'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add     = $extend_hours_eight;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price_eight;
        $total_price        = $total_amount + $extend_price_eight;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price_eight;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id_eight'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}

if (isset($_POST['extended_hours_nine'])) {

    $booking_id_nine        = $_POST['booking_id_nine'];
    $extend_hours_nine    	= $_POST['extend_hours_nine'];
    $extend_price_nine		= $_POST['extend_price_nine'];

    if ($booking_id_nine != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id_nine'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add     = $extend_hours_nine;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price_nine;
        $total_price        = $total_amount + $extend_price_nine;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price_nine;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id_nine'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}

if (isset($_POST['extended_hours_ten'])) {

    $booking_id_ten        = $_POST['booking_id_ten'];
    $extend_hours_ten    	= $_POST['extend_hours_ten'];
    $extend_price_ten		= $_POST['extend_price_ten'];

    if ($booking_id_ten != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id_ten'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add     = $extend_hours_ten;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price_ten;
        $total_price        = $total_amount + $extend_price_ten;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price_ten;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id_ten'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}

if (isset($_POST['extended_hours_eleven'])) {

    $booking_id_eleven        = $_POST['booking_id_eleven'];
    $extend_hours_eleven    	= $_POST['extend_hours_eleven'];
    $extend_price_eleven		= $_POST['extend_price_eleven'];

    if ($booking_id_eleven != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id_eleven'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add     = $extend_hours_eleven;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price_eleven;
        $total_price        = $total_amount + $extend_price_eleven;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price_eleven;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id_eleven'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}

if (isset($_POST['extended_hours_twelve'])) {

    $booking_id_twelve        = $_POST['booking_id_twelve'];
    $extend_hours_twelve    	= $_POST['extend_hours_twelve'];
    $extend_price_twelve		= $_POST['extend_price_twelve'];

    if ($booking_id_twelve != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id_twelve'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add     = $extend_hours_twelve;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price_twelve;
        $total_price        = $total_amount + $extend_price_twelve;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price_twelve;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id_twelve'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}

if (isset($_POST['extended_hours_thirteen'])) {

    $booking_id_thirteen        = $_POST['booking_id_thirteen'];
    $extend_hours_thirteen    	= $_POST['extend_hours_thirteen'];
    $extend_price_thirteen		= $_POST['extend_price_thirteen'];

    if ($booking_id_thirteen != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id_thirteen'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add     = $extend_hours_thirteen;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price_thirteen;
        $total_price        = $total_amount + $extend_price_thirteen;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price_thirteen;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id_thirteen'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}

if (isset($_POST['extended_hours_fourteen'])) {

    $booking_id_fourteen        = $_POST['booking_id_fourteen'];
    $extend_hours_fourteen    	= $_POST['extend_hours_fourteen'];
    $extend_price_fourteen		= $_POST['extend_price_fourteen'];

    if ($booking_id_fourteen != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id_fourteen'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add     = $extend_hours_fourteen;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price_fourteen;
        $total_price        = $total_amount + $extend_price_fourteen;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price_fourteen;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id_fourteen'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}

if (isset($_POST['extended_hours_fifteen'])) {

    $booking_id_fifteen        = $_POST['booking_id_fifteen'];
    $extend_hours_fifteen   	= $_POST['extend_hours_fifteen'];
    $extend_price_fifteen		= $_POST['extend_price_fifteen'];

    if ($booking_id_fifteen != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id_fifteen'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add     = $extend_hours_fifteen;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price_fifteen;
        $total_price        = $total_amount + $extend_price_fifteen;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price_fifteen;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id_fifteen'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}

if (isset($_POST['extended_hours_sixteen'])) {

    $booking_id_sixteen        = $_POST['booking_id_sixteen'];
    $extend_hours_sixteen   	= $_POST['extend_hours_sixteen'];
    $extend_price_sixteen		= $_POST['extend_price_sixteen'];

    if ($booking_id_sixteen != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id_sixteen'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add     = $extend_hours_sixteen;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price_sixteen;
        $total_price        = $total_amount + $extend_price_sixteen;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price_sixteen;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id_sixteen'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}

if (isset($_POST['extended_hours_seventeen'])) {

    $booking_id_seventeen        = $_POST['booking_id_seventeen'];
    $extend_hours_seventeen   	= $_POST['extend_hours_seventeen'];
    $extend_price_seventeen		= $_POST['extend_price_seventeen'];

    if ($booking_id_seventeen != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id_seventeen'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add     = $extend_hours_seventeen;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price_seventeen;
        $total_price        = $total_amount + $extend_price_seventeen;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price_seventeen;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id_seventeen'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}

if (isset($_POST['extended_hours_eighteen'])) {

    $booking_id_eighteen        = $_POST['booking_id_eighteen'];
    $extend_hours_eighteen   	= $_POST['extend_hours_eighteen'];
    $extend_price_eighteen	    = $_POST['extend_price_eighteen'];

    if ($booking_id_eighteen != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id_eighteen'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add     = $extend_hours_eighteen;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price_eighteen;
        $total_price        = $total_amount + $extend_price_eighteen;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price_eighteen;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id_eighteen'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}

if (isset($_POST['extended_hours_nineteen'])) {

    $booking_id_nineteen        = $_POST['booking_id_nineteen'];
    $extend_hours_nineteen   	= $_POST['extend_hours_nineteen'];
    $extend_price_nineteen	    = $_POST['extend_price_nineteen'];

    if ($booking_id_nineteen != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id_nineteen'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add     = $extend_hours_nineteen;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price_nineteen;
        $total_price        = $total_amount + $extend_price_nineteen;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price_nineteen;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id_nineteen'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}

if (isset($_POST['extended_hours_twenty'])) {

    $booking_id_twenty        = $_POST['booking_id_twenty'];
    $extend_hours_twenty   	= $_POST['extend_hours_twenty'];
    $extend_price_twenty	    = $_POST['extend_price_twenty'];

    if ($booking_id_twenty != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id_twenty'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add     = $extend_hours_twenty;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price_twenty;
        $total_price        = $total_amount + $extend_price_twenty;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price_twenty;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id_twenty'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}

if (isset($_POST['extended_hours_twentyone'])) {

    $booking_id_twentyone        = $_POST['booking_id_twentyone'];
    $extend_hours_twentyone   	= $_POST['extend_hours_twentyone'];
    $extend_price_twentyone	    = $_POST['extend_price_twentyone'];

    if ($booking_id_twentyone != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id_twentyone'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add     = $extend_hours_twentyone;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price_twentyone;
        $total_price        = $total_amount + $extend_price_twentyone;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price_twentyone;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id_twentyone'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}

if (isset($_POST['extended_hours_twentytwo'])) {

    $booking_id_twentytwo        = $_POST['booking_id_twentytwo'];
    $extend_hours_twentytwo   	= $_POST['extend_hours_twentytwo'];
    $extend_price_twentytwo	    = $_POST['extend_price_twentytwo'];

    if ($booking_id_twentytwo != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id_twentytwo'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add     = $extend_hours_twentytwo;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price_twentytwo;
        $total_price        = $total_amount + $extend_price_twentytwo;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price_twentytwo;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id_twentytwo'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}


if (isset($_POST['extended_hours_twentythree'])) {

    $booking_id_twentythree        = $_POST['booking_id_twentythree'];
    $extend_hours_twentythree   	= $_POST['extend_hours_twentythree'];
    $extend_price_twentythree	    = $_POST['extend_price_twentythree'];

    if ($booking_id_twentythree != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id_twentythree'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add     = $extend_hours_twentythree;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price_twentythree;
        $total_price        = $total_amount + $extend_price_twentythree;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price_twentythree;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id_twentythree'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}


if (isset($_POST['extended_hours_twentyfour'])) {

    $booking_id_twentyfour        = $_POST['booking_id_twentyfour'];
    $extend_hours_twentyfour   	= $_POST['extend_hours_twentyfour'];
    $extend_price_twentyfour	    = $_POST['extend_price_twentyfour'];

    if ($booking_id_twentyfour != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id_twentyfour'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add     = $extend_hours_twentyfour;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price_twentyfour;
        $total_price        = $total_amount + $extend_price_twentyfour;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price_twentyfour;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id_twentyfour'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}


if (isset($_POST['extended_hours_twentyfive'])) {

    $booking_id_twentyfive        = $_POST['booking_id_twentyfive'];
    $extend_hours_twentyfive   	= $_POST['extend_hours_twentyfive'];
    $extend_price_twentyfive	    = $_POST['extend_price_twentyfive'];

    if ($booking_id_twentyfive != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id_twentyfive'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add     = $extend_hours_twentyfive;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price_twentyfive;
        $total_price        = $total_amount + $extend_price_twentyfive;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price_twentyfive;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id_twentyfive'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}

if (isset($_POST['extended_hours_twentysix'])) {

    $booking_id_twentysix        = $_POST['booking_id_twentysix'];
    $extend_hours_twentysix   	= $_POST['extend_hours_twentysix'];
    $extend_price_twentysix	    = $_POST['extend_price_twentysix'];

    if ($booking_id_twentysix != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id_twentysix'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add     = $extend_hours_twentysix;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price_twentysix;
        $total_price        = $total_amount + $extend_price_twentysix;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price_twentysix;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id_twentysix'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}

if (isset($_POST['extended_hours_twentyeight'])) {

    $booking_id_twentyeight        = $_POST['booking_id_twentyeight'];
    $extend_hours_twentyeight   	= $_POST['extend_hours_twentyeight'];
    $extend_price_twentyeight	    = $_POST['extend_price_twentyeight'];

    if ($booking_id_twentyeight != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id_twentyeight'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add     = $extend_hours_twentyeight;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price_twentyeight;
        $total_price        = $total_amount + $extend_price_twentyeight;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price_twentyeight;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id_twentyeight'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}

if (isset($_POST['extended_hours_twentynine'])) {

    $booking_id_twentynine        = $_POST['booking_id_twentynine'];
    $extend_hours_twentynine   	= $_POST['extend_hours_twentynine'];
    $extend_price_twentynine	    = $_POST['extend_price_twentynine'];

    if ($booking_id_twentynine != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id_twentynine'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add     = $extend_hours_twentynine;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price_twentynine;
        $total_price        = $total_amount + $extend_price_twentynine;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price_twentynine;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id_twentynine'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}


if (isset($_POST['extended_hours_thirty'])) {

    $booking_id_thirty        = $_POST['booking_id_thirty'];
    $extend_hours_thirty   	= $_POST['extend_hours_thirty'];
    $extend_price_thirty	    = $_POST['extend_price_thirty'];

    if ($booking_id_thirty != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id_thirty'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add     = $extend_hours_thirty;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price_thirty;
        $total_price        = $total_amount + $extend_price_thirty;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price_thirty;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id_thirty'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}


if (isset($_POST['extended_hours_thirtyone'])) {

    $booking_id_thirtyone        = $_POST['booking_id_thirtyone'];
    $extend_hours_thirtyone   	= $_POST['extend_hours_thirtyone'];
    $extend_price_thirtyone	    = $_POST['extend_price_thirtyone'];

    if ($booking_id_thirtyone != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id_thirtyone'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add     = $extend_hours_thirtyone;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price_thirtyone;
        $total_price        = $total_amount + $extend_price_thirtyone;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price_thirtyone;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id_thirtyone'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}


if (isset($_POST['extended_hours_thirtytwo'])) {

    $booking_id_thirtytwo        = $_POST['booking_id_thirtytwo'];
    $extend_hours_thirtytwo   	= $_POST['extend_hours_thirtytwo'];
    $extend_price_thirtytwo	    = $_POST['extend_price_thirtytwo'];

    if ($booking_id_thirtytwo != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id_thirtytwo'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add     = $extend_hours_thirtytwo;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price_thirtytwo;
        $total_price        = $total_amount + $extend_price_thirtytwo;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price_thirtytwo;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id_thirtytwo'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}

if (isset($_POST['extended_hours_thirtythree'])) {

    $booking_id_thirtythree        = $_POST['booking_id_thirtythree'];
    $extend_hours_thirtythree   	= $_POST['extend_hours_thirtythree'];
    $extend_price_thirtythree	    = $_POST['extend_price_thirtythree'];

    if ($booking_id_thirtythree != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id_thirtythree'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add     = $extend_hours_thirtythree;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price_thirtythree;
        $total_price        = $total_amount + $extend_price_thirtythree;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price_thirtythree;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id_thirtythree'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}

if (isset($_POST['extended_hours_threeseven'])) {

    $booking_id_threeseven        = $_POST['booking_id_threeseven'];
    $extend_hours_threeseven   	= $_POST['extend_hours_threeseven'];
    $extend_price_threeseven	    = $_POST['extend_price_threeseven'];

    if ($booking_id_thirtythree != '') {

        $query              = "SELECT * FROM booking WHERE booking_id = '$booking_id_threeseven'";
        $result             = mysqli_query($connection, $query);
        $booking_details    = mysqli_fetch_assoc($result);
        $room_price         = $booking_details['room_price'];
        $rooms_out 			= $booking_details['check_out'];
        $total_amount       = $booking_details['total_price'];
        // $newnew = strtotime(sprintf("+%d hours", $extend_hours), strtotime($rooms_out));	
		$minutes_to_add     = $extend_hours_threeseven;

		$time = new DateTime($rooms_out);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

		$stamp = $time->format('Y-m-d H:i');
        $total 				= $room_price + $extend_price_threeseven;
        $total_price        = $total_amount + $extend_price_threeseven;
        $remaining_price    = $booking_details['remaining_price'] + $extend_price_threeseven;

        $updateBooking      = "UPDATE booking SET room_price = '$total', check_out = '$stamp', total_price = '$total_price', remaining_price = '$remaining_price' WHERE booking_id = '$booking_id_threeseven'";
        $result             = mysqli_query($connection, $updateBooking);

        if ($result) {

                $response['done'] = true;
                $response['data'] = "Problem in payment";

        } else {

            $response['done'] = false;
            $response['data'] = "Problem in payment";

        }

    } else {

        $response['done'] = false;
        $response['data'] = "Error With Booking";
    }

    
    echo json_encode($response);
}


if (isset($_POST['timeOut'])) {
    date_default_timezone_set('Asia/Manila');
    $time_Out = $_POST['time_Out'];
    $date = date('Y-m-d H:i');

    $query      = "UPDATE attendance SET time_out = '$date', present = 1 WHERE id = $time_Out";
    $result     = mysqli_query($connection, $query);

    if ($result) {
        
        $response['data'] == true;
        header("Refresh:0; index.php?attendance");
        // header("index.php?attendance");

    } else {
        $response['data'] == false;
        // header("Location:index.php?attendance&error");
    }

    echo json_encode($response);

}


// if (isset($_POST['add_sauna'])) {

//     $check_in_n     = $_POST['check_in_na'];
//     $check_out_n    = $_POST['check_out_na'];
//     $additional_n   = $_POST['additional_na'];
//     $add_price_n    = $_POST['add_price_na'];
//     $spa_price_n    = $_POST['spa_price_na'];
//     $full_name_n    = $_POST['full_name_na'];
//     $total_price_n  = $_POST['total_price_na'];

//     if ($check_in_n == '' && $check_out_n == '' && $full_name_n == ''){
//         $response['done'] = false;
//         $response['data'] = "Please Enter Carednalities";
//     }else{
//         $customer_sql = "INSERT INTO spa_sauna(customer_name,check_in,check_out,additional,add_price,sauna_price,total_price) VALUES('$full_name_n','$check_in_n','$check_out_n','$additional_n','$add_price_n','$spa_price_n','$total_price_n')";
//         $customer_result = mysqli_query($connection, $customer_sql);
//         if ($customer_result) {
//             $response['done'] = true;
//             $response['data'] = 'Successfully Booking';
//         } else {
//             $response['done'] = false;
//             $response['data'] = "DataBase Error in status change";
//         }
//     }
//     echo json_encode($response);
// }

if (isset($_POST['add_employee'])) {

    $check_in_n     = $_POST['check_in_na'];
    $check_out_n    = $_POST['check_out_na'];
    $additional_n   = $_POST['additional_na'];
    $add_price_n    = $_POST['add_price_na'];
    $spa_price_n    = $_POST['spa_price_na'];
    $full_name_n    = $_POST['full_name_na'];
    $total_price_n  = $_POST['total_price_na'];
    $userID         = $_SESSION['user_id'];
    $date           = date('Y-m-d');

    if ($check_in_n == '' && $check_out_n == '' && $full_name_n == ''){
        $response['done'] = false;
        $response['data'] = "Please Enter Cardinalities";
    }else{
        $customer_sql = "INSERT INTO spa_sauna (customer_name,check_in,check_out,additional,add_price,sauna_price,total_price,user_id,date) VALUES ('$full_name_n','$check_in_n','$check_out_n','$additional_n','$add_price_n','$spa_price_n','$total_price_n','$userID','$date')";
        $customer_result = mysqli_query($connection, $customer_sql);
        if ($customer_result) {
            $response['done'] = true;
            $response['data'] = 'Successfully Booking';
        } else {
            $response['done'] = false;
            $response['data'] = "DataBase Error in status change";
        }
    }
    echo json_encode($response);
}

// if (isset($_POST['alerting'])) {
//     $alert_id = $_POST['alert_id'];
    
//     $sql = "SELECT * FROM room NATURAL JOIN room_type NATURAL JOIN booking NATURAL JOIN customer WHERE room_id = '$alert_id' AND payment_status = '0'";
    
//     $result = mysqli_query($connection, $sql);
//    if(mysqli_num_rows($result) == 1) {

//     $res  = mysqli_fetch_assoc($result);
//     $response['done'] == true;
//     // $response['check_out'] = $res['check_out'];

//     }else {
//         $response['data'] == false;
//     }

   

//     echo json_encode($response);
// }