<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#">
                    <em class="fa fa-home"></em>
                </a></li>
            <li class="active">Add SPA / SAUNA</li>
        </ol>
    </div><!--/.row-->

    <!-- <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add Employee</h1>
        </div>
    </div> -->
    <!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">SPA / SAUNA Detail:</div>
                <div class="panel-body">
                    <div class="emp-response"></div>
                    <form role="form" id="addEmployee" data-toggle="validator">
                        <div class="row">
                                <div class="form-group col-lg-6">
                                    <label>Check In Date</label>
                                    <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="check_in_date_n" data-error="Select Check In Date" required>
                                    <div class="help-block with-errors"></div>
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>Check Out Date</label>
                                    <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="check_out_date_n" data-error="Select Check Out Date" required>
                                    <div class="help-block with-errors"></div>
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>Additional</label>
                                    <select id="additional_n" name="additional_n" class="form-control">
                                      <option value="default"></option>
                                      <option value="food">Food</option>
                                      <option value="beverages">Beverages</option>
                                      <option value="towel">Towel</option>
                                      <option value="bed">Bed</option>
                                    </select>
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>Price</label>
                                    <input type="text" class="form-control" placeholder="Input Price" id="add_price_n" data-error="Input Price" onkeyup="calc()" required>
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>SPA / SAUNA Price</label>
                                    <input type="text" class="form-control" placeholder="Input SPA / SAUNA Price" id="spa_price_n" data-error="Input Room Price" onkeyup="calc()" required>
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>Full Name</label>
                                    <input type="text" class="form-control" placeholder="Input Full Name" id="full_name_n" data-error="Input Full Name" required>
                                </div>
                                <div class="form-group col-lg-6 pull-right">
                                    </br>
                                    <div class=""><span style="font-size:20px;">SPA/SAUNA Price &#8213; Php 1500.00</span></div>
                                </div>
                                <div class="col-lg-12">
                                    <h4 style="font-weight: bold">Total Amount : <span id="total_price_n">0</span> /-</h4>
                                </div>
                        </div>

                        <button type="submit" class="btn btn-lg btn-success" style="border-radius:0%">Submit</button>
                        <button type="reset" class="btn btn-lg btn-danger" style="border-radius:0%">Reset</button>
                    </form>
                </div>
            </div>
        </div>


    </div>

    

</div>    <!--/.main-->




