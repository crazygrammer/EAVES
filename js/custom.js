!function ($) {
    $(document).on("click", "ul.nav li.parent > a ", function () {
        $(this).find('em').toggleClass("fa-minus");
    });
    $(".sidebar span.icon").find('em:first').addClass("fa-plus");
}

(window.jQuery);
$(window).on('resize', function () {
    if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
});
$(window).on('resize', function () {
    if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
});

function refreshPage() {
    $.ajax({
        type: 'POST',
        url: 'alerting.php',
        dataType: 'JSON',
        data: {
            sampli: 1,
            alert_id: ''
        },
        success: function(response) {
            if(response.done == true){
                playAudio();
                alert('Room 201 is about to END, there is only 10 mins left. Please proceed to the Check IN tab if you want to EXTEND!');
            }else{
                // NOTHING TO DO
            }
            
        },
        complete: function() {
            window.setTimeout(refreshPage, 30000);
        }
    });
}
window.setTimeout(refreshPage, 30000);

function refreshPageTwo() {
    $.ajax({
        type: 'POST',
        url: 'alerting.php',
        dataType: 'JSON',
        data: {
            sampli_two: 2,
            alert_id_two: ''
        },
        success: function(response) {
            if(response.done == true){
                playAudio();
                alert('Room 202 is about to END, there is only 10 mins left. Please proceed to the Check IN tab if you want to EXTEND!');
            }else{
                // NOTHING TO DO
            }
            
        },
        complete: function() {
            window.setTimeout(refreshPageTwo, 30000);
        }
    });
}
window.setTimeout(refreshPageTwo, 30000);

function refreshPageThree() {
    $.ajax({
        type: 'POST',
        url: 'alerting.php',
        dataType: 'JSON',
        data: {
            sampli_three: 3,
            alert_id_three: ''
        },
        success: function(response) {
            if(response.done == true){
                playAudio();
                alert('Room 203 is about to END, there is only 10 mins left. Please proceed to the Check IN tab if you want to EXTEND!');
            }else{
                // NOTHING TO DO
            }
            
        },
        complete: function() {
            window.setTimeout(refreshPageThree, 30000);
        }
    });
}
window.setTimeout(refreshPageThree, 30000);

function refreshPageFour() {
    $.ajax({
        type: 'POST',
        url: 'alerting.php',
        dataType: 'JSON',
        data: {
            sampli_four: 4,
            alert_id_four: ''
        },
        success: function(response) {
            if(response.done == true){
                playAudio();
                alert('Room 204 is about to END, there is only 10 mins left. Please proceed to the Check IN tab if you want to EXTEND!');
            }else{
                // NOTHING TO DO
            }
            
        },
        complete: function() {
            window.setTimeout(refreshPageFour, 30000);
        }
    });
}
window.setTimeout(refreshPageFour, 30000);

function refreshPageFive() {
    $.ajax({
        type: 'POST',
        url: 'alerting.php',
        dataType: 'JSON',
        data: {
            sampli_five: 5,
            alert_id_five: ''
        },
        success: function(response) {
            if(response.done == true){
                playAudio();
                alert('Room 205 is about to END, there is only 10 mins left. Please proceed to the Check IN tab if you want to EXTEND!');
            }else{
                // NOTHING TO DO
            }
            
        },
        complete: function() {
            window.setTimeout(refreshPageFive, 30000);
        }
    });
}
window.setTimeout(refreshPageFive, 30000);

function refreshPageSix() {
    $.ajax({
        type: 'POST',
        url: 'alerting.php',
        dataType: 'JSON',
        data: {
            sampli_six: 6,
            alert_id_six: ''
        },
        success: function(response) {
            if(response.done == true){
                playAudio();
                alert('Room 206 is about to END, there is only 10 mins left. Please proceed to the Check IN tab if you want to EXTEND!');
            }else{
                // NOTHING TO DO
            }
            
        },
        complete: function() {
            window.setTimeout(refreshPageSix, 30000);
        }
    });
}
window.setTimeout(refreshPageSix, 30000);

function refreshPageSeven() {
    $.ajax({
        type: 'POST',
        url: 'alerting.php',
        dataType: 'JSON',
        data: {
            sampli_seven: 7,
            alert_id_seven: ''
        },
        success: function(response) {
            if(response.done == true){
                playAudio();
                alert('Room 207 is about to END, there is only 10 mins left. Please proceed to the Check IN tab if you want to EXTEND!');
            }else{
                // NOTHING TO DO
            }
            
        },
        complete: function() {
            window.setTimeout(refreshPageSeven, 30000);
        }
    });
}
window.setTimeout(refreshPageSeven, 30000);

function refreshPageEight() {
    $.ajax({
        type: 'POST',
        url: 'alerting.php',
        dataType: 'JSON',
        data: {
            sampli_eight: 8,
            alert_id_eight: ''
        },
        success: function(response) {
            if(response.done == true){
                playAudio();
                alert('Room 208 is about to END, there is only 10 mins left. Please proceed to the Check IN tab if you want to EXTEND!');
            }else{
                // NOTHING TO DO
            }
            
        },
        complete: function() {
            window.setTimeout(refreshPageEight, 30000);
        }
    });
}
window.setTimeout(refreshPageEight, 30000);

function refreshPageNine() {
    $.ajax({
        type: 'POST',
        url: 'alerting.php',
        dataType: 'JSON',
        data: {
            sampli_nine: 9,
            alert_id_nine: ''
        },
        success: function(response) {
            if(response.done == true){
                playAudio();
                alert('Room 209 is about to END, there is only 10 mins left. Please proceed to the Check IN tab if you want to EXTEND!');
            }else{
                // NOTHING TO DO
            }
            
        },
        complete: function() {
            window.setTimeout(refreshPageNine, 30000);
        }
    });
}
window.setTimeout(refreshPageNine, 30000);

function refreshPageTen() {
    $.ajax({
        type: 'POST',
        url: 'alerting.php',
        dataType: 'JSON',
        data: {
            sampli_ten: 10,
            alert_id_ten: ''
        },
        success: function(response) {
            if(response.done == true){
                playAudio();
                alert('Room 301 is about to END, there is only 10 mins left. Please proceed to the Check IN tab if you want to EXTEND!');
            }else{
                // NOTHING TO DO
            }
            
        },
        complete: function() {
            window.setTimeout(refreshPageTen, 30000);
        }
    });
}
window.setTimeout(refreshPageTen, 30000);

function refreshPageEleven() {
    $.ajax({
        type: 'POST',
        url: 'alerting.php',
        dataType: 'JSON',
        data: {
            sampli_eleven: 11,
            alert_id_eleven: ''
        },
        success: function(response) {
            if(response.done == true){
                playAudio();
                alert('Room 302 is about to END, there is only 10 mins left. Please proceed to the Check IN tab if you want to EXTEND!');
            }else{
                // NOTHING TO DO
            }
            
        },
        complete: function() {
            window.setTimeout(refreshPageEleven, 30000);
        }
    });
}
window.setTimeout(refreshPageEleven, 30000);

function refreshPageTwelve() {
    $.ajax({
        type: 'POST',
        url: 'alerting.php',
        dataType: 'JSON',
        data: {
            sampli_twelve: 12,
            alert_id_twelve: ''
        },
        success: function(response) {
            if(response.done == true){
                playAudio();
                alert('Room 303 is about to END, there is only 10 mins left. Please proceed to the Check IN tab if you want to EXTEND!');
            }else{
                // NOTHING TO DO
            }
            
        },
        complete: function() {
            window.setTimeout(refreshPageTwelve, 30000);
        }
    });
}
window.setTimeout(refreshPageTwelve, 30000);

function refreshPageThirteen() {
    $.ajax({
        type: 'POST',
        url: 'alerting.php',
        dataType: 'JSON',
        data: {
            sampli_thirteen: 13,
            alert_id_thirteen: ''
        },
        success: function(response) {
            if(response.done == true){
                playAudio();
                alert('Room 304 is about to END, there is only 10 mins left. Please proceed to the Check IN tab if you want to EXTEND!');
            }else{
                // NOTHING TO DO
            }
            
        },
        complete: function() {
            window.setTimeout(refreshPageThirteen, 30000);
        }
    });
}
window.setTimeout(refreshPageThirteen, 30000);

function refreshPageFourteen() {
    $.ajax({
        type: 'POST',
        url: 'alerting.php',
        dataType: 'JSON',
        data: {
            sampli_fourteen: 14,
            alert_id_fourteen: ''
        },
        success: function(response) {
            if(response.done == true){
                playAudio();
                alert('Room 305 is about to END, there is only 10 mins left. Please proceed to the Check IN tab if you want to EXTEND!');
            }else{
                // NOTHING TO DO
            }
            
        },
        complete: function() {
            window.setTimeout(refreshPageFourteen, 30000);
        }
    });
}
window.setTimeout(refreshPageFourteen, 30000);

function refreshPageFifteen() {
    $.ajax({
        type: 'POST',
        url: 'alerting.php',
        dataType: 'JSON',
        data: {
            sampli_fifteen: 15,
            alert_id_fifteen: ''
        },
        success: function(response) {
            if(response.done == true){
                playAudio();
                alert('Room 306 is about to END, there is only 10 mins left. Please proceed to the Check IN tab if you want to EXTEND!');
            }else{
                // NOTHING TO DO
            }
            
        },
        complete: function() {
            window.setTimeout(refreshPageFifteen, 30000);
        }
    });
}
window.setTimeout(refreshPageFifteen, 30000);

function refreshPageSixteen() {
    $.ajax({
        type: 'POST',
        url: 'alerting.php',
        dataType: 'JSON',
        data: {
            sampli_sixteen: 16,
            alert_id_sixteen: ''
        },
        success: function(response) {
            if(response.done == true){
                playAudio();
                alert('Room 307 is about to END, there is only 10 mins left. Please proceed to the Check IN tab if you want to EXTEND!');
            }else{
                // NOTHING TO DO
            }
            
        },
        complete: function() {
            window.setTimeout(refreshPageSixteen, 30000);
        }
    });
}
window.setTimeout(refreshPageSixteen, 30000);

function refreshPageSeventeen() {
    $.ajax({
        type: 'POST',
        url: 'alerting.php',
        dataType: 'JSON',
        data: {
            sampli_seventeen: 17,
            alert_id_seventeen: ''
        },
        success: function(response) {
            if(response.done == true){
                playAudio();
                alert('Room 308 is about to END, there is only 10 mins left. Please proceed to the Check IN tab if you want to EXTEND!');
            }else{
                // NOTHING TO DO
            }
            
        },
        complete: function() {
            window.setTimeout(refreshPageSeventeen, 30000);
        }
    });
}
window.setTimeout(refreshPageSeventeen, 30000);

function refreshPageEighteen() {
    $.ajax({
        type: 'POST',
        url: 'alerting.php',
        dataType: 'JSON',
        data: {
            sampli_eighteen: 18,
            alert_id_eighteen: ''
        },
        success: function(response) {
            if(response.done == true){
                playAudio();
                alert('Room 309 is about to END, there is only 10 mins left. Please proceed to the Check IN tab if you want to EXTEND!');
            }else{
                // NOTHING TO DO
            }
            
        },
        complete: function() {
            window.setTimeout(refreshPageEighteen, 30000);
        }
    });
}
window.setTimeout(refreshPageEighteen, 30000);

function refreshPageNineteen() {
    $.ajax({
        type: 'POST',
        url: 'alerting.php',
        dataType: 'JSON',
        data: {
            sampli_nineteen: 19,
            alert_id_nineteen: ''
        },
        success: function(response) {
            if(response.done == true){
                playAudio();
                alert('Room 401 is about to END, there is only 10 mins left. Please proceed to the Check IN tab if you want to EXTEND!');
            }else{
                // NOTHING TO DO
            }
            
        },
        complete: function() {
            window.setTimeout(refreshPageNineteen, 30000);
        }
    });
}
window.setTimeout(refreshPageNineteen, 30000);

function refreshPageTwenty() {
    $.ajax({
        type: 'POST',
        url: 'alerting.php',
        dataType: 'JSON',
        data: {
            sampli_twenty: 20,
            alert_id_twenty: ''
        },
        success: function(response) {
            if(response.done == true){
                playAudio();
                alert('Room 402 is about to END, there is only 10 mins left. Please proceed to the Check IN tab if you want to EXTEND!');
            }else{
                // NOTHING TO DO
            }
            
        },
        complete: function() {
            window.setTimeout(refreshPageTwenty, 30000);
        }
    });
}
window.setTimeout(refreshPageTwenty, 30000);

function refreshPageTwentyone() {
    $.ajax({
        type: 'POST',
        url: 'alerting.php',
        dataType: 'JSON',
        data: {
            sampli_twentyone: 21,
            alert_id_twentyone: ''
        },
        success: function(response) {
            if(response.done == true){
                playAudio();
                alert('Room 403 is about to END, there is only 10 mins left. Please proceed to the Check IN tab if you want to EXTEND!');
            }else{
                // NOTHING TO DO
            }
            
        },
        complete: function() {
            window.setTimeout(refreshPageTwentyone, 30000);
        }
    });
}
window.setTimeout(refreshPageTwentyone, 30000);

function refreshPageTwentytwo() {
    $.ajax({
        type: 'POST',
        url: 'alerting.php',
        dataType: 'JSON',
        data: {
            sampli_twentytwo: 22,
            alert_id_twentytwo: ''
        },
        success: function(response) {
            if(response.done == true){
                playAudio();
                alert('Room 404 is about to END, there is only 10 mins left. Please proceed to the Check IN tab if you want to EXTEND!');
            }else{
                // NOTHING TO DO
            }
            
        },
        complete: function() {
            window.setTimeout(refreshPageTwentytwo, 30000);
        }
    });
}
window.setTimeout(refreshPageTwentytwo, 30000);

function refreshPageTwentythree() {
    $.ajax({
        type: 'POST',
        url: 'alerting.php',
        dataType: 'JSON',
        data: {
            sampli_twentythree: 23,
            alert_id_twentythree: ''
        },
        success: function(response) {
            if(response.done == true){
                playAudio();
                alert('Room 405 is about to END, there is only 10 mins left. Please proceed to the Check IN tab if you want to EXTEND!');
            }else{
                // NOTHING TO DO
            }
            
        },
        complete: function() {
            window.setTimeout(refreshPageTwentythree, 30000);
        }
    });
}
window.setTimeout(refreshPageTwentythree, 30000);

function refreshPageTwentyfour() {
    $.ajax({
        type: 'POST',
        url: 'alerting.php',
        dataType: 'JSON',
        data: {
            sampli_twentyfour: 24,
            alert_id_twentyfour: ''
        },
        success: function(response) {
            if(response.done == true){
                playAudio();
                alert('Room 406 is about to END, there is only 10 mins left. Please proceed to the Check IN tab if you want to EXTEND!');
            }else{
                // NOTHING TO DO
            }
            
        },
        complete: function() {
            window.setTimeout(refreshPageTwentyfour, 30000);
        }
    });
}
window.setTimeout(refreshPageTwentyfour, 30000);

function refreshPageTwentyfive() {
    $.ajax({
        type: 'POST',
        url: 'alerting.php',
        dataType: 'JSON',
        data: {
            sampli_twentyfive: 25,
            alert_id_twentyfive: ''
        },
        success: function(response) {
            if(response.done == true){
                playAudio();
                alert('Room 407 is about to END, there is only 10 mins left. Please proceed to the Check IN tab if you want to EXTEND!');
            }else{
                // NOTHING TO DO
            }
            
        },
        complete: function() {
            window.setTimeout(refreshPageTwentyfive, 30000);
        }
    });
}
window.setTimeout(refreshPageTwentyfive, 30000);

function refreshPageTwentysix() {
    $.ajax({
        type: 'POST',
        url: 'alerting.php',
        dataType: 'JSON',
        data: {
            sampli_twentysix: 26,
            alert_id_twentysix: ''
        },
        success: function(response) {
            if(response.done == true){
                playAudio();
                alert('Room 408 is about to END, there is only 10 mins left. Please proceed to the Check IN tab if you want to EXTEND!');
            }else{
                // NOTHING TO DO
            }
            
        },
        complete: function() {
            window.setTimeout(refreshPageTwentysix, 30000);
        }
    });
}
window.setTimeout(refreshPageTwentysix, 30000);

function refreshPageTwentyeight() {
    $.ajax({
        type: 'POST',
        url: 'alerting.php',
        dataType: 'JSON',
        data: {
            sampli_twentyeight: 28,
            alert_id_twentyeight: ''
        },
        success: function(response) {
            if(response.done == true){
                playAudio();
                alert('Room 501 is about to END, there is only 10 mins left. Please proceed to the Check IN tab if you want to EXTEND!');
            }else{
                // NOTHING TO DO
            }
            
        },
        complete: function() {
            window.setTimeout(refreshPageTwentyeight, 30000);
        }
    });
}
window.setTimeout(refreshPageTwentyeight, 30000);

function refreshPageTwentynine() {
    $.ajax({
        type: 'POST',
        url: 'alerting.php',
        dataType: 'JSON',
        data: {
            sampli_twentynine: 29,
            alert_id_twentynine: ''
        },
        success: function(response) {
            if(response.done == true){
                playAudio();
                alert('Room 502 is about to END, there is only 10 mins left. Please proceed to the Check IN tab if you want to EXTEND!');
            }else{
                // NOTHING TO DO
            }
            
        },
        complete: function() {
            window.setTimeout(refreshPageTwentynine, 30000);
        }
    });
}
window.setTimeout(refreshPageTwentynine, 30000);

function refreshPageThirty() {
    $.ajax({
        type: 'POST',
        url: 'alerting.php',
        dataType: 'JSON',
        data: {
            sampli_thirty: 30,
            alert_id_thirty: ''
        },
        success: function(response) {
            if(response.done == true){
                playAudio();
                alert('Room 503 is about to END, there is only 10 mins left. Please proceed to the Check IN tab if you want to EXTEND!');
            }else{
                // NOTHING TO DO
            }
            
        },
        complete: function() {
            window.setTimeout(refreshPageThirty, 30000);
        }
    });
}
window.setTimeout(refreshPageThirty, 30000);

function refreshPageThirtyone() {
    $.ajax({
        type: 'POST',
        url: 'alerting.php',
        dataType: 'JSON',
        data: {
            sampli_thirtyone: 31,
            alert_id_thirtyone: ''
        },
        success: function(response) {
            if(response.done == true){
                playAudio();
                alert('Room 504 is about to END, there is only 10 mins left. Please proceed to the Check IN tab if you want to EXTEND!');
            }else{
                // NOTHING TO DO
            }
            
        },
        complete: function() {
            window.setTimeout(refreshPageThirtyone, 30000);
        }
    });
}
window.setTimeout(refreshPageThirtyone, 30000);

function refreshPageThirtytwo() {
    $.ajax({
        type: 'POST',
        url: 'alerting.php',
        dataType: 'JSON',
        data: {
            sampli_thirtytwo: 32,
            alert_id_thirtytwo: ''
        },
        success: function(response) {
            if(response.done == true){
                playAudio();
                alert('Room 505 is about to END, there is only 10 mins left. Please proceed to the Check IN tab if you want to EXTEND!');
            }else{
                // NOTHING TO DO
            }
            
        },
        complete: function() {
            window.setTimeout(refreshPageThirtytwo, 30000);
        }
    });
}
window.setTimeout(refreshPageThirtytwo, 30000);

function refreshPageThirtythree() {
    $.ajax({
        type: 'POST',
        url: 'alerting.php',
        dataType: 'JSON',
        data: {
            sampli_thirtythree: 33,
            alert_id_thirtythree: ''
        },
        success: function(response) {
            if(response.done == true){
                playAudio();
                alert('Room 506 is about to END, there is only 10 mins left. Please proceed to the Check IN tab if you want to EXTEND!');
            }else{
                // NOTHING TO DO
            }
            
        },
        complete: function() {
            window.setTimeout(refreshPageThirtythree, 30000);
        }
    });
}
window.setTimeout(refreshPageThirtythree, 30000);




function playAudio() {
    document.getElementById("audio").play();
}

$(document).ready(function () {
    $('#rooms').DataTable();
    jQuery('#check_in_date').datetimepicker();
    jQuery('#check_out_date').datetimepicker();
    jQuery('#check_in_date_n').datetimepicker();
    jQuery('#check_out_date_n').datetimepicker();

});

function calculate() {
    var add_price   = $("#add_price").val();
    var room_price  = $("#room_price").val();
    var total_price = parseInt(add_price) + parseInt(room_price);
    
    if(!isNaN(total_price)) {
        document.getElementById('total_price').innerHTML = total_price;
    }
}

function calc_edit() {
    var add_price   = $("#edit_adds_price").val();
    var room_price  = $("#edit_room_price").val();
    var total_price = parseInt(add_price) + parseInt(room_price);
    
    if(!isNaN(total_price)) {
        document.getElementById('edit_total_price').innerHTML = total_price;
        // $("#room_price").val() = total_price;
    }
}


function calc() {
    var add_price   = $("#add_price_n").val();
    var spa_price   = $("#spa_price_n").val();
    var total_price = parseInt(add_price) + parseInt(spa_price);
    
    if(!isNaN(total_price)) {
        document.getElementById('total_price_n').innerHTML = total_price;
    }
}
document.getElementById("btn_print").onclick = function () {
    printElement(document.getElementById("printThis"));
}

// $(document).ready( function () {
//     var alert_id = 1;
//     $.ajax({
//         url: "alerting.php",
//         type: 'POST',
//         data: {
//             alert_id: alert_id,
//             alerting: ''
//         },
//         success: function(response) {
//             // alert(response);
//             if(response == '"2022-03-22 14:14"'){
//                 alert('done');
//             }else{
//                 alert('none');
//             }

//         }
//     });
//  });





function printElement(elem) {
    var domClone = elem.cloneNode(true);
    
    var $printSection = document.getElementById("printSection");
    
    if (!$printSection) {
        var $printSection = document.createElement("div");
        $printSection.id = "printSection";
        document.body.appendChild($printSection);
    }
    
    $printSection.innerHTML = "";
    $printSection.appendChild(domClone);
    window.print();
}


function display_c(){
    var refresh=1000; // Refresh rate in milli seconds
    mytime=setTimeout('display_ct()',refresh)
    }


    function display_ct() {
        var strcount
        var x 
        var x1
        var currentTime = new Date()
        var month = currentTime.getMonth() + 1
        var day = currentTime.getDate()
        var year = currentTime.getFullYear()
        var hours = currentTime.getHours()
        var minutes = currentTime.getMinutes()
        var seconds= currentTime.getSeconds()
         x = month + "/" + day + "/" + year
        if (minutes < 10){
        minutes = "0" + minutes
        }
        x=x+"  "+hours + ":" + minutes +":"+seconds
        x1=hours + ":" + minutes + ":"+seconds
        
        
        document.getElementById('ct').innerHTML = x;
        tt=display_c();
    }



