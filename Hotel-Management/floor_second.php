<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">3rd Floor</li>
				<div class="pull-right"> <span id='ct' ></span></div>
			</ol>
		</div><!--/.row-->

		<div class="panel panel-container">
			<div class="row">
					<?php
						// $room_one_query = "SELECT * FROM room NATURAL JOIN room_type WHERE deleteStatus = 0 AND room_id =10";
                        $room_ten_query = "
							SELECT 
							a.room_id,
							a.room_type_id,
							a.room_no,
							a.status,
							a.check_in_status,
							a.check_out_status,
							a.deleteStatus,
							b.room_type_id,
							b.room_type,
							b.price,
							b.max_person
						FROM 
							room a
						LEFT JOIN 
							room_type b ON b.room_type_id = a.room_type_id
						
						WHERE 
							deleteStatus = 0 AND a.room_id = 10";
						$rooms_ten_result = mysqli_query($connection, $room_ten_query); 
					?>
					<?php if (mysqli_num_rows($rooms_ten_result) > 0) {
                        	while ($rooms_ten = mysqli_fetch_assoc($rooms_ten_result)) { 
					?>
					<?php 	if ($rooms_ten['status']  == 0) {	

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-blue"></em>
												<a class="btn btn-primary btn-lg" href="index.php?reservation&amp;room_id=10&amp;room_type_id=2">Room 301</a>
											</div>
										</div>
									  </div>';
							}else{

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-red"></em>
												<button class="btn btn-danger btn-lg" id="extendBtnTen" data-id="" data-toggle="modal" data-target="#extendHrsTen" href="">Room 301</button>
											</div>
										</div>
									  </div>';
							}
					?>
					<?php } } ?>

					<?php
						$room_eleven_query = "
							SELECT 
							a.room_id,
							a.room_type_id,
							a.room_no,
							a.status,
							a.check_in_status,
							a.check_out_status,
							a.deleteStatus,
							b.room_type_id,
							b.room_type,
							b.price,
							b.max_person
						FROM 
							room a
						LEFT JOIN 
							room_type b ON b.room_type_id = a.room_type_id
						
						WHERE 
							deleteStatus = 0 AND a.room_id = 11";
						$rooms_eleven_result = mysqli_query($connection, $room_eleven_query); 
					?>
					<?php if (mysqli_num_rows($rooms_eleven_result) > 0) {
                        	while ($rooms_eleven = mysqli_fetch_assoc($rooms_eleven_result)) { 
					?>
					<?php 	if ($rooms_eleven['status']  == 0) {	

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-blue"></em>
												<a class="btn btn-primary btn-lg" href="index.php?reservation&amp;room_id=11&amp;room_type_id=2">Room 302</a>
											</div>
										</div>
									  </div>';
							}else{

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-red"></em>
												<button class="btn btn-danger btn-lg" id="extendBtnEleven" data-id="" data-toggle="modal" data-target="#extendHrsEleven" href="">Room 302</button>
											</div>
										</div>
									  </div>';
							}
					?>
					<?php } } ?>

					<?php
						$room_twelve_query = "
						SELECT 
							a.room_id,
							a.room_type_id,
							a.room_no,
							a.status,
							a.check_in_status,
							a.check_out_status,
							a.deleteStatus,
							b.room_type_id,
							b.room_type,
							b.price,
							b.max_person
						FROM 
							room a
						LEFT JOIN 
							room_type b ON b.room_type_id = a.room_type_id
						
						WHERE 
							deleteStatus = 0 AND a.room_id = 12";
						$rooms_twelve_result = mysqli_query($connection, $room_twelve_query); 
					?>
					<?php if (mysqli_num_rows($rooms_twelve_result) > 0) {
                        	while ($rooms_twelve = mysqli_fetch_assoc($rooms_twelve_result)) { 
					?>
					<?php 	if ($rooms_twelve['status']  == 0) {	

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-blue"></em>
												<a class="btn btn-primary btn-lg" href="index.php?reservation&amp;room_id=12&amp;room_type_id=2">Room 303</a>
											</div>
										</div>
									  </div>';
							}else{

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-red"></em>
												<button class="btn btn-danger btn-lg" id="extendBtnTwelve" data-id="" data-toggle="modal" data-target="#extendHrsTwelve" href="">Room 303</button>
											</div>
										</div>
									  </div>';
							}
					?>
					<?php } } ?>
			</div>
			<div class="row">
					<?php
						$room_thirteen_query = "
						SELECT 
							a.room_id,
							a.room_type_id,
							a.room_no,
							a.status,
							a.check_in_status,
							a.check_out_status,
							a.deleteStatus,
							b.room_type_id,
							b.room_type,
							b.price,
							b.max_person
						FROM 
							room a
						LEFT JOIN 
							room_type b ON b.room_type_id = a.room_type_id
						WHERE 
							deleteStatus = 0 AND a.room_id = 13";
						$rooms_thirteen_result = mysqli_query($connection, $room_thirteen_query); 
					?>
					<?php if (mysqli_num_rows($rooms_thirteen_result) > 0) {
                        	while ($rooms_thirteen = mysqli_fetch_assoc($rooms_thirteen_result)) { 
					?>
					<?php 	if ($rooms_thirteen['status']  == 0) {	

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-blue"></em>
												<a class="btn btn-primary btn-lg" href="index.php?reservation&amp;room_id=13&amp;room_type_id=2">Room 304</a>
											</div>
										</div>
									  </div>';
							}else{

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-red"></em>
												<button class="btn btn-danger btn-lg" id="extendBtnThirteen" data-id="" data-toggle="modal" data-target="#extendHrsThirteen" href="">Room 304</button>
											</div>
										</div>
									  </div>';
							}
					?>
					<?php } } ?>

					<?php
						$room_fourteen_query = "
						SELECT 
							a.room_id,
							a.room_type_id,
							a.room_no,
							a.status,
							a.check_in_status,
							a.check_out_status,
							a.deleteStatus,
							b.room_type_id,
							b.room_type,
							b.price,
							b.max_person
						FROM 
							room a
						LEFT JOIN 
							room_type b ON b.room_type_id = a.room_type_id
						WHERE 
							deleteStatus = 0 AND a.room_id = 14";
						$rooms_fourteen_result = mysqli_query($connection, $room_fourteen_query); 
					?>
					<?php if (mysqli_num_rows($rooms_fourteen_result) > 0) {
                        	while ($rooms_fourteen = mysqli_fetch_assoc($rooms_fourteen_result)) { 
					?>
					<?php 	if ($rooms_fourteen['status']  == 0) {	

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-blue"></em>
												<a class="btn btn-primary btn-lg" href="index.php?reservation&amp;room_id=14&amp;room_type_id=2">Room 305</a>
											</div>
										</div>
									  </div>';
							}else{

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-red"></em>
												<button class="btn btn-danger btn-lg" id="extendBtnFourteen" data-id="" data-toggle="modal" data-target="#extendHrsFourteen" href="">Room 305</button>
											</div>
										</div>
									  </div>';
							}
					?>
					<?php } } ?>

					<?php
						$room_fifteen_query = "
						SELECT 
							a.room_id,
							a.room_type_id,
							a.room_no,
							a.status,
							a.check_in_status,
							a.check_out_status,
							a.deleteStatus,
							b.room_type_id,
							b.room_type,
							b.price,
							b.max_person
						FROM 
							room a
						LEFT JOIN 
							room_type b ON b.room_type_id = a.room_type_id
						WHERE 
							deleteStatus = 0 AND a.room_id = 15";
						$rooms_fifteen_result = mysqli_query($connection, $room_fifteen_query); 
					?>
					<?php if (mysqli_num_rows($rooms_fifteen_result) > 0) {
                        	while ($rooms_fifteen = mysqli_fetch_assoc($rooms_fifteen_result)) { 
					?>
					<?php 	if ($rooms_fifteen['status']  == 0) {	

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-blue"></em>
												<a class="btn btn-primary btn-lg" href="index.php?reservation&amp;room_id=15&amp;room_type_id=2">Room 306</a>
											</div>
										</div>
									  </div>';
							}else{

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-red"></em>
												<button class="btn btn-danger btn-lg" id="extendBtnFifteen" data-id="" data-toggle="modal" data-target="#extendHrsFifteen" href="">Room 306</button>
											</div>
										</div>
									  </div>';
							}
					?>
					<?php } } ?>
			</div>
			<div class="row">
					<?php
						$room_sixteen_query = "
						SELECT 
							a.room_id,
							a.room_type_id,
							a.room_no,
							a.status,
							a.check_in_status,
							a.check_out_status,
							a.deleteStatus,
							b.room_type_id,
							b.room_type,
							b.price,
							b.max_person
						FROM 
							room a
						LEFT JOIN 
							room_type b ON b.room_type_id = a.room_type_id
						WHERE 
							deleteStatus = 0 AND a.room_id = 16";
						$rooms_sixteen_result = mysqli_query($connection, $room_sixteen_query); 
					?>
					<?php if (mysqli_num_rows($rooms_sixteen_result) > 0) {
                        	while ($rooms_sixteen = mysqli_fetch_assoc($rooms_sixteen_result)) { 
					?>
					<?php 	if ($rooms_sixteen['status']  == 0) {	

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-blue"></em>
												<a class="btn btn-primary btn-lg" href="index.php?reservation&amp;room_id=16&amp;room_type_id=2">Room 307</a>
											</div>
										</div>
									  </div>';
							}else{

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-red"></em>
												<button class="btn btn-danger btn-lg" id="extendBtnSixteen" data-id="" data-toggle="modal" data-target="#extendHrsSixteen" href="">Room 307</button>
											</div>
										</div>
									  </div>';
							}
					?>
					<?php } } ?>

					<?php
						$room_seventeen_query = "
						SELECT 
							a.room_id,
							a.room_type_id,
							a.room_no,
							a.status,
							a.check_in_status,
							a.check_out_status,
							a.deleteStatus,
							b.room_type_id,
							b.room_type,
							b.price,
							b.max_person
						FROM 
							room a
						LEFT JOIN 
							room_type b ON b.room_type_id = a.room_type_id
						WHERE 
							deleteStatus = 0 AND a.room_id = 17";
						$rooms_seventeen_result = mysqli_query($connection, $room_seventeen_query); 
					?>
					<?php if (mysqli_num_rows($rooms_seventeen_result) > 0) {
                        	while ($rooms_seventeen = mysqli_fetch_assoc($rooms_seventeen_result)) { 
					?>
					<?php 	if ($rooms_seventeen['status']  == 0) {	

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-blue"></em>
												<a class="btn btn-primary btn-lg" href="index.php?reservation&amp;room_id=17&amp;room_type_id=2">Room 308</a>
											</div>
										</div>
									  </div>';
							}else{

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-red"></em>
												<button class="btn btn-danger btn-lg" id="extendBtnSeventeen" data-id="" data-toggle="modal" data-target="#extendHrsSeventeen" href="">Room 308</button>
											</div>
										</div>
									  </div>';
							}
					?>
					<?php } } ?>

					<?php
						$room_eighteen_query = "
						SELECT 
							a.room_id,
							a.room_type_id,
							a.room_no,
							a.status,
							a.check_in_status,
							a.check_out_status,
							a.deleteStatus,
							b.room_type_id,
							b.room_type,
							b.price,
							b.max_person
						FROM 
							room a
						LEFT JOIN 
							room_type b ON b.room_type_id = a.room_type_id
						WHERE 
							deleteStatus = 0 AND a.room_id = 18";
						$rooms_eighteen_result = mysqli_query($connection, $room_eighteen_query); 
					?>
					<?php if (mysqli_num_rows($rooms_eighteen_result) > 0) {
                        	while ($rooms_eighteen = mysqli_fetch_assoc($rooms_eighteen_result)) { 
					?>
					<?php 	if ($rooms_eighteen['status']  == 0) {	

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-blue"></em>
												<a class="btn btn-primary btn-lg" href="index.php?reservation&amp;room_id=18&amp;room_type_id=2">Room 309</a>
											</div>
										</div>
									  </div>';
							}else{

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-red"></em>
												<button class="btn btn-danger btn-lg" id="extendBtnEighteen" data-id="" data-toggle="modal" data-target="#extendHrsEighteen" href="">Room 309</button>
											</div>
										</div>
									  </div>';
							}
					?>
					<?php } } ?>
			</div>
			<hr>

			<div class="row">
				<div class="col-xs-6 col-md-2 col-lg-2 no-padding">
					
				</div>

				<div class="col-xs-6 col-md-4 col-lg-4 no-padding">
					<div class="panel panel-red panel-widget border-right">
						<div class="row no-padding"><em class="fa fa-xl fa-money color-blue"></em>
							<div class="large">Php <?php include 'counters/count-secondfloor.php'?></div>
							<div class="text-muted">Total Earnings 3rd Floor</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>


<div id="extendHrsTen" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<?php
						$sqlTen = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person,
						c.check_out,
						c.payment_status,
						c.total_price,
						c.extend_hours,
						c.booking_date,
						c.booking_id
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					LEFT JOIN
						booking c ON c.room_id = a.room_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 10 AND c.payment_status = 0";
						$resultTen = mysqli_query($connection, $sqlTen);
						$fetchTen = mysqli_fetch_assoc($resultTen);

					?>

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Extend</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h4 class="modal-dialog text-center">Room 301 is about to End, Do you want to Extend?</h4>
                            <form role="form" id="extendFormTen">
                                <div class="payment-response"></div>
                                <div class="form-group col-lg-12">
                                    <label>Extended Minutes</label>
                                    <input type="number" class="form-control" id="extend_hours_ten"
                                           placeholder="Please Enter Minutes Here..">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Extend Price</label>
                                    <input type="number" class="form-control" id="extend_price_ten"
                                           placeholder="Please Enter Amounts Here..">
                                </div>
                                <input type="hidden" id="getExtendIdTen" value="<?php echo $fetchTen['booking_id'] ?>">
                                <button type="submit" class="btn btn-primary pull-right">Extend</button>
                                <!-- <button type="" class="btn btn-primary pull-right">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

	<div id="extendHrsEleven" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<?php
						$sqlEleven = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person,
						c.check_out,
						c.payment_status,
						c.total_price,
						c.extend_hours,
						c.booking_date,
						c.booking_id
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					LEFT JOIN
						booking c ON c.room_id = a.room_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 11 AND c.payment_status = 0";
						$resultEleven = mysqli_query($connection, $sqlEleven);
						$fetchEleven = mysqli_fetch_assoc($resultEleven);

					?>

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Extend</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h4 class="modal-dialog text-center">Room 302 is about to End, Do you want to Extend?</h4>
                            <form role="form" id="extendFormEleven">
                                <div class="payment-response"></div>
                                <div class="form-group col-lg-12">
                                    <label>Extended Minutes</label>
                                    <input type="number" class="form-control" id="extend_hours_eleven"
                                           placeholder="Please Enter Minutes Here..">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Extend Price</label>
                                    <input type="number" class="form-control" id="extend_price_eleven"
                                           placeholder="Please Enter Amounts Here..">
                                </div>
                                <input type="hidden" id="getExtendIdEleven" value="<?php echo $fetchEleven['booking_id'] ?>">
                                <button type="submit" class="btn btn-primary pull-right">Extend</button>
                                <!-- <button type="" class="btn btn-primary pull-right">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


	<div id="extendHrsTwelve" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<?php
						$sqlTwelve = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person,
						c.check_out,
						c.payment_status,
						c.total_price,
						c.extend_hours,
						c.booking_date,
						c.booking_id
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					LEFT JOIN
						booking c ON c.room_id = a.room_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 12 AND c.payment_status = 0";
						$resultTwelve = mysqli_query($connection, $sqlTwelve);
						$fetchTwelve = mysqli_fetch_assoc($resultTwelve);

					?>

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Extend</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h4 class="modal-dialog text-center">Room 303 is about to End, Do you want to Extend?</h4>
                            <form role="form" id="extendFormTwelve">
                                <div class="payment-response"></div>
                                <div class="form-group col-lg-12">
                                    <label>Extended Minutes</label>
                                    <input type="number" class="form-control" id="extend_hours_twelve"
                                           placeholder="Please Enter Minutes Here..">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Extend Price</label>
                                    <input type="number" class="form-control" id="extend_price_twelve"
                                           placeholder="Please Enter Amounts Here..">
                                </div>
                                <input type="hidden" id="getExtendIdTwelve" value="<?php echo $fetchTwelve['booking_id'] ?>">
                                <button type="submit" class="btn btn-primary pull-right">Extend</button>
                                <!-- <button type="" class="btn btn-primary pull-right">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


	<div id="extendHrsThirteen" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<?php
						$sqlThirteen = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person,
						c.check_out,
						c.payment_status,
						c.total_price,
						c.extend_hours,
						c.booking_date,
						c.booking_id
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					LEFT JOIN
						booking c ON c.room_id = a.room_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 13 AND c.payment_status = 0";
						$resultThirteen = mysqli_query($connection, $sqlThirteen);
						$fetchThirteen = mysqli_fetch_assoc($resultThirteen);

					?>

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Extend</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h4 class="modal-dialog text-center">Room 304 is about to End, Do you want to Extend?</h4>
                            <form role="form" id="extendFormThirteen">
                                <div class="payment-response"></div>
                                <div class="form-group col-lg-12">
                                    <label>Extended Minutes</label>
                                    <input type="number" class="form-control" id="extend_hours_thirteen"
                                           placeholder="Please Enter Minutes Here..">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Extend Price</label>
                                    <input type="number" class="form-control" id="extend_price_thirteen"
                                           placeholder="Please Enter Amounts Here..">
                                </div>
                                <input type="hidden" id="getExtendIdThirteen" value="<?php echo $fetchThirteen['booking_id'] ?>">
                                <button type="submit" class="btn btn-primary pull-right">Extend</button>
                                <!-- <button type="" class="btn btn-primary pull-right">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


	<div id="extendHrsFourteen" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<?php
						$sqlFourteen = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person,
						c.check_out,
						c.payment_status,
						c.total_price,
						c.extend_hours,
						c.booking_date,
						c.booking_id
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					LEFT JOIN
						booking c ON c.room_id = a.room_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 14 AND c.payment_status = 0";
						$resultFourteen = mysqli_query($connection, $sqlFourteen);
						$fetchFourteen = mysqli_fetch_assoc($resultFourteen);

					?>

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Extend</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h4 class="modal-dialog text-center">Room 305 is about to End, Do you want to Extend?</h4>
                            <form role="form" id="extendFormFourteen">
                                <div class="payment-response"></div>
                                <div class="form-group col-lg-12">
                                    <label>Extended Minutes</label>
                                    <input type="number" class="form-control" id="extend_hours_fourteen"
                                           placeholder="Please Enter Minutes Here..">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Extend Price</label>
                                    <input type="number" class="form-control" id="extend_price_fourteen"
                                           placeholder="Please Enter Amounts Here..">
                                </div>
                                <input type="hidden" id="getExtendIdFourteen" value="<?php echo $fetchFourteen['booking_id'] ?>">
                                <button type="submit" class="btn btn-primary pull-right">Extend</button>
                                <!-- <button type="" class="btn btn-primary pull-right">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


	<div id="extendHrsFifteen" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<?php
						$sqlFifteen = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person,
						c.check_out,
						c.payment_status,
						c.total_price,
						c.extend_hours,
						c.booking_date,
						c.booking_id
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					LEFT JOIN
						booking c ON c.room_id = a.room_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 15 AND c.payment_status = 0";
						$resultFifteen = mysqli_query($connection, $sqlFifteen);
						$fetchFifteen = mysqli_fetch_assoc($resultFifteen);

					?>

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Extend</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h4 class="modal-dialog text-center">Room 306 is about to End, Do you want to Extend?</h4>
                            <form role="form" id="extendFormFifteen">
                                <div class="payment-response"></div>
                                <div class="form-group col-lg-12">
                                    <label>Extended Minutes</label>
                                    <input type="number" class="form-control" id="extend_hours_fifteen"
                                           placeholder="Please Enter Minutes Here..">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Extend Price</label>
                                    <input type="number" class="form-control" id="extend_price_fifteen"
                                           placeholder="Please Enter Amounts Here..">
                                </div>
                                <input type="hidden" id="getExtendIdFifteen" value="<?php echo $fetchFifteen['booking_id'] ?>">
                                <button type="submit" class="btn btn-primary pull-right">Extend</button>
                                <!-- <button type="" class="btn btn-primary pull-right">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


	<div id="extendHrsSixteen" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<?php
						$sqlSixteen = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person,
						c.check_out,
						c.payment_status,
						c.total_price,
						c.extend_hours,
						c.booking_date,
						c.booking_id
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					LEFT JOIN
						booking c ON c.room_id = a.room_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 16 AND c.payment_status = 0";
						$resultSixteen = mysqli_query($connection, $sqlSixteen);
						$fetchSixteen = mysqli_fetch_assoc($resultSixteen);

					?>

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Extend</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h4 class="modal-dialog text-center">Room 307 is about to End, Do you want to Extend?</h4>
                            <form role="form" id="extendFormSixteen">
                                <div class="payment-response"></div>
                                <div class="form-group col-lg-12">
                                    <label>Extended Minutes</label>
                                    <input type="number" class="form-control" id="extend_hours_sixteen"
                                           placeholder="Please Enter Minutes Here..">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Extend Price</label>
                                    <input type="number" class="form-control" id="extend_price_sixteen"
                                           placeholder="Please Enter Amounts Here..">
                                </div>
                                <input type="hidden" id="getExtendIdSixteen" value="<?php echo $fetchSixteen['booking_id'] ?>">
                                <button type="submit" class="btn btn-primary pull-right">Extend</button>
                                <!-- <button type="" class="btn btn-primary pull-right">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


	<div id="extendHrsSeventeen" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<?php
						$sqlSeventeen = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person,
						c.check_out,
						c.payment_status,
						c.total_price,
						c.extend_hours,
						c.booking_date,
						c.booking_id
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					LEFT JOIN
						booking c ON c.room_id = a.room_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 17 AND c.payment_status = 0";
						$resultSeventeen = mysqli_query($connection, $sqlSeventeen);
						$fetchSeventeen = mysqli_fetch_assoc($resultSeventeen);

					?>

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Extend</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h4 class="modal-dialog text-center">Room 308 is about to End, Do you want to Extend?</h4>
                            <form role="form" id="extendFormSeventeen">
                                <div class="payment-response"></div>
                                <div class="form-group col-lg-12">
                                    <label>Extended Minutes</label>
                                    <input type="number" class="form-control" id="extend_hours_seventeen"
                                           placeholder="Please Enter Minutes Here..">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Extend Price</label>
                                    <input type="number" class="form-control" id="extend_price_seventeen"
                                           placeholder="Please Enter Amounts Here..">
                                </div>
                                <input type="hidden" id="getExtendIdSeventeen" value="<?php echo $fetchSeventeen['booking_id'] ?>">
                                <button type="submit" class="btn btn-primary pull-right">Extend</button>
                                <!-- <button type="" class="btn btn-primary pull-right">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


	<div id="extendHrsEighteen" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<?php
						$sqlEighteen = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person,
						c.check_out,
						c.payment_status,
						c.total_price,
						c.extend_hours,
						c.booking_date,
						c.booking_id
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					LEFT JOIN
						booking c ON c.room_id = a.room_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 18 AND c.payment_status = 0";
						$resultEighteen = mysqli_query($connection, $sqlEighteen);
						$fetchEighteen = mysqli_fetch_assoc($resultEighteen);

					?>

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Extend</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h4 class="modal-dialog text-center">Room 309 is about to End, Do you want to Extend?</h4>
                            <form role="form" id="extendFormEighteen">
                                <div class="payment-response"></div>
                                <div class="form-group col-lg-12">
                                    <label>Extended Minutes</label>
                                    <input type="number" class="form-control" id="extend_hours_eighteen"
                                           placeholder="Please Enter Minutes Here..">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Extend Price</label>
                                    <input type="number" class="form-control" id="extend_price_eighteen"
                                           placeholder="Please Enter Amounts Here..">
                                </div>
                                <input type="hidden" id="getExtendIdEighteen" value="<?php echo $fetchEighteen['booking_id'] ?>">
                                <button type="submit" class="btn btn-primary pull-right">Extend</button>
                                <!-- <button type="" class="btn btn-primary pull-right">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>