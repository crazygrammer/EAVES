<?php
include_once "db.php";
session_start();
if (isset($_SESSION['user_id'])){
    $user_id    = $_SESSION['user_id'];
    $userQuery  = "SELECT * FROM user WHERE id = '$user_id'";
    $result     = mysqli_query($connection, $userQuery);
    $user       = mysqli_fetch_assoc($result);
}else{
    header('Location:login.php');
}
include_once "header.php";
include_once "sidebar.php";

if (isset($_GET['dashboard'])){
    include_once "dashboard.php";
}
elseif (isset($_GET['reservation'])){
    include_once "reservation.php";
}
elseif (isset($_GET['first_floor'])){
    include_once "floor_first.php";
}
elseif (isset($_GET['second_floor'])){
    include_once "floor_second.php";
}
elseif (isset($_GET['third_floor'])){
    include_once "floor_third.php";
}
elseif (isset($_GET['fourth_floor'])){
    include_once "floor_fourth.php";
}
elseif (isset($_GET['room_mang'])){
    include_once "room_mang.php";
}
elseif (isset($_GET['complain'])){
    include_once "complain.php";
}
elseif (isset($_GET['spa_mang'])){
    include_once "spa_mang.php";
}
elseif (isset($_GET['spa_sauna'])){
    include_once "spa_sauna.php";
}
elseif (isset($_GET['attendance'])){
    include_once "attendance.php";
}
elseif (isset($_GET['sauna_history'])){
    include_once "sauna_history.php";
}
elseif($_SESSION['user_id']){
    if(isset($_GET['history'])){
        include_once "history.php";
    }
}

include_once "footer.php";