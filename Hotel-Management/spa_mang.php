<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#">
                    <em class="fa fa-home"></em>
                </a></li>
            <li class="active">SPA / SAUNA</li>
        </ol>
    </div><!--/.row-->

   

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">SPA / SAUNA Details:
                    <a href="index.php?spa_sauna" class="btn btn-secondary pull-right" style="border-radius:0%">Add SPA/SAUNA</a>
                </div>
                <div class="panel-body">
                    <?php
                    if (isset($_GET['error'])) {
                        echo "<div class='alert alert-danger'>
                                <span class='glyphicon glyphicon-info-sign'></span> &nbsp; Error on Extend Change !
                            </div>";
                    }
                    if (isset($_GET['success'])) {
                        echo "<div class='alert alert-success'>
                                <span class='glyphicon glyphicon-info-sign'></span> &nbsp; Extend Successfully Changed!
                            </div>";
                    }
                    if (isset($_GET['successCheckout'])) {
                        echo "<div class='alert alert-success'>
                                <span class='glyphicon glyphicon-info-sign'></span> &nbsp; Successfully Check out!
                            </div>";
                    }
                    ?>
                    <table class="table table-striped table-bordered table-responsive" cellspacing="0" width="100%"
                           id="rooms">
                        <thead>
                        <tr>
                            <th>SPA ID</th>
                            <th>Customer Name</th>
                            <th>Check In</th>
                            <th>Check Out</th>
                            <th>Total Amount</th>
                            <!-- <th>Add Ons Price</th>
                            <th>SPA PRICE</th> -->
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if($_SESSION['restrictions'] == 0){
                        $date = date('Y-m-d');
                        $staff_query = "SELECT * FROM spa_sauna WHERE date = '$date'";
                        $staff_result = mysqli_query($connection, $staff_query);

                        if (mysqli_num_rows($staff_result) > 0) {
                            while ($staff = mysqli_fetch_assoc($staff_result)) { ?>
                                <tr>

                                    <td><?php echo $staff['id']; ?></td>
                                    <td><?php echo $staff['customer_name']; ?></td>
                                    <td><?php echo date('F j, Y, g:i a', strtotime($staff['check_in'])); ?></td>
                                    <td><?php echo date('F j, Y, g:i a', strtotime($staff['check_out'])); ?></td>
                                    <td><?php echo $staff['total_price']; ?></td>
                                    <!-- <td><?php// echo $staff['additional']; ?></td>
                                    <td><?php// echo $staff['add_price']; ?></td>
                                    <td><?php //echo $staff['sauna_price']; ?></td> -->
                                    <?php if($staff['payment_status'] == 0) {?>
                                    <td>
                                    <button title="Check Out Information" data-toggle="modal" data-target="#checkOutModal" data-id="<?= $staff['id'] ?>" id="checkOutDetails" class="btn btn-warning" style="border-radius:0%;">Check Out</button>
                                    <!-- <button class="btn btn-danger" data-toggle="modal" style="border-radius:0%" data-target="#checkOutSpa" data-id="<?= $staff['id'] ?>" id="checkoutSpa">Check Out</button> -->
                                    <!-- <button class="btn btn-warning" id="checkOutSauna"  data-id="<?// $staff['id'] ?>" data-toggle="modal" style="border-radius:0%" data-target="#checkOutSpa">Check Out<?// $staff['id'] ?></button> -->
                                    <!-- <button class="btn btn-warning" id="extendSauna"  data-id="<?// $staff['id'] ?>" data-toggle="modal" style="border-radius:0%" data-target="#extendSpa">Extend<? //$staff['id'] ?></button> -->
                                    <button class="btn btn-info" data-toggle="modal" style="border-radius:0%" data-target="#extendSpa" data-id="<?= $staff['id'] ?>" id="extend">Extend</button>
                                    </td>
                                    <?php } else { ?>
                                        <td>CHECK OUT</td>
                                    <?php } ?>
                                </tr>
                                <?php
                            }
                        }
                    }else{
                        $staff_query = "SELECT * FROM spa_sauna";
                        $staff_result = mysqli_query($connection, $staff_query);

                        if (mysqli_num_rows($staff_result) > 0) {
                            while ($staff = mysqli_fetch_assoc($staff_result)) { ?>
                                <tr>

                                    <td><?php echo $staff['id']; ?></td>
                                    <td><?php echo $staff['customer_name']; ?></td>
                                    <td><?php echo date('F j, Y, g:i a', strtotime($staff['check_in'])); ?></td>
                                    <td><?php echo date('F j, Y, g:i a', strtotime($staff['check_out'])); ?></td>
                                    <td><?php echo $staff['total_price']; ?></td>
                                    <!-- <td><?php// echo $staff['additional']; ?></td>
                                    <td><?php// echo $staff['add_price']; ?></td>
                                    <td><?php //echo $staff['sauna_price']; ?></td> -->
                                    <?php if($staff['payment_status'] == 0) {?>
                                    <td>
                                    <button title="Check Out Information" data-toggle="modal" data-target="#checkOutModal" data-id="<?= $staff['id'] ?>" id="checkOutDetails" class="btn btn-warning" style="border-radius:0%;">Check Out</button>
                                    <!-- <button class="btn btn-danger" data-toggle="modal" style="border-radius:0%" data-target="#checkOutSpa" data-id="<?= $staff['id'] ?>" id="checkoutSpa">Check Out</button> -->
                                    <!-- <button class="btn btn-warning" id="checkOutSauna"  data-id="<?// $staff['id'] ?>" data-toggle="modal" style="border-radius:0%" data-target="#checkOutSpa">Check Out<?// $staff['id'] ?></button> -->
                                    <!-- <button class="btn btn-warning" id="extendSauna"  data-id="<?// $staff['id'] ?>" data-toggle="modal" style="border-radius:0%" data-target="#extendSpa">Extend<? //$staff['id'] ?></button> -->
                                    <button class="btn btn-info" data-toggle="modal" style="border-radius:0%" data-target="#extendSpa" data-id="<?= $staff['id'] ?>" id="extend">Extend</button>
                                    </td>
                                    <?php } else { ?>
                                        <td>CHECK OUT</td>
                                    <?php } ?>
                                </tr>
                                <?php
                            }
                        }
                    }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <div id="extendSpa" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Extend SPA / SAUNA</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form data-toggle="validator" role="form" method="post" action="ajax.php">
                                <div class="form-group">
                                    <label>Extended Minutes</label>
                                    <input class="form-control" placeholder="Enter Extended Minutes" name="extend_minutes" data-error="Enter Minutes to Extend" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <label>SPA / SAUNA Price</label>
                                    <input class="form-control" placeholder="Enter the Amount" name="spa_price" data-error="Enter the Price" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <input type="hidden" id="extend_id" name="extend_id" value="">
                                <button class="btn btn-primary pull-right" name="extend_spa">Extend</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div id="printThis">
    <div id="checkOutModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Check Out SPA / SAUNA</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <table class="table table-responsive table-bordered">

                                <tbody>
                                <tr>
                                    <td><b>Customer Name</b></td>
                                    <td id="customer_name"></td>
                                </tr>
                                <tr>
                                    <td><b>Additionals</b></td>
                                    <td id="additional"></td>
                                </tr>
                                <tr>
                                    <td><b>Sauna / SPA Price</b></td>
                                    <td id="sauna_price"></td>
                                </tr>
                                <tr>
                                    <td><b>Add Ons Price</b></td>
                                    <td id="add_price"></td>
                                </tr>
                                <tr>
                                    <td><b>Check In</b></td>
                                    <td id="check_in"></td>
                                </tr>
                                <tr>
                                    <td><b>Check Out</b></td>
                                    <td id="check_out"></td>
                                </tr>
                                <tr>
                                    <td><b>Total Price</b></td>
                                    <td id="total_price"></td>
                                </tr>
                                </tbody>
                                <a href="" class="btn btn-primary" id ="btn_print">Print</a>
                            </table>

                            <form data-toggle="validator" role="form" method="post" action="ajax.php">
                                
                                <input type="hidden" id="checkout_id" name="checkout_id" value="">
                                <button class="btn btn-primary pull-right" name="checkout_spa">Check Out</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
 

    <div class="row">
        <div class="col-sm-12">
        
        </div>
    </div>

</div>    <!--/.main-->
