function fetch_room(val) {
    $.ajax({
        type: 'post',
        url: 'ajax.php',
        data: {
            room_type_id: val,
            room_type: ''
        },
        success: function (response) {
            $('#room_no').html(response);

        }
    });
}

$('#addRoom').submit(function () {

    var room_type_id    = $('#room_type_id').val();
    var room_no         = $('#room_no').val();

    $.ajax({
        type: 'post',
        url: 'ajax.php',
        dataType: 'JSON',
        data: {
            room_type_id: room_type_id,
            room_no: room_no,
            add_room: ''
        },
        success: function (response) {

            if (response.done == true) {

                $('#addRoom').modal('hide');
                window.location.href = 'index.php?room_mang';

            } else {
                
                $('.response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
            }
        }
    });

    return false;
});

$('#booking').submit(function () {
    var room_type_id    = $('#room_type').val();
    var room_type       = $("#room_type :selected").text();
    var room_id         = $('#room_no').val();
    var room_no         = $("#room_no :selected").text();
    var additional      = $("#additional").val();
    var room_price      = $("#room_price").val();
    var add_price       = $("#add_price").val();
    var check_in_date   = $('#check_in_date').val();
    var check_out_date  = $('#check_out_date').val();
    var first_name      = $('#first_name').val();
    var last_name       = $('#last_name').val();
    var contact_no      = $('#contact_no').val();
    var email           = $('#email').val();
    var address         = $('#address').val();
    var total_price     = document.getElementById('total_price').innerHTML;

    if(!check_in_date && !check_out_date && !room_price && !first_name && !last_name && !contact_no && !email && !address){
        $('.response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>Please Fill Cardinality</div>');
    }else{
        $.ajax({
            type: 'post',
            url: 'ajax.php',
            dataType: 'JSON',
            data: {
                room_id:room_id,
                additional:additional,
                room_price:room_price,
                add_price:add_price,
                check_in:check_in_date,
                check_out:check_out_date,
                total_price:total_price,
                name:first_name+' '+last_name,
                contact_no:contact_no,
                email:email,
                address:address,
                booking:''
            },
            success: function (response) {
                if (response.done == true) {
                    $('#getCustomerName').html(first_name+' '+last_name);
                    $('#getRoomType').html(room_type);
                    $('#getRoomNo').html(room_no);
                    $('#getCheckIn').html(check_in_date);
                    $('#getCheckOut').html(check_out_date);
                    $('#getAdditional').html(additional);
                    $('#getRoomPrice').html(room_price);
                    $('#getAddPrice').html(add_price);
                    $('#getTotalPrice').html(total_price);
                    $('#getPaymentStaus').html("Unpaid");
                    $('#bookingConfirm').modal('show');
                    document.getElementById("booking").reset();
                } else {
                    $('.response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
                }
            }
        });
    }

    return false;
});


$(document).on('click', '#checkOutDetails', function (e) {
    e.preventDefault();

    var check_details = $(this).data('id');
    console.log(check_details);

    $.ajax({
        type: 'post',
        url: 'ajax.php',
        dataType: 'JSON',
        data: {
            check_details: check_details,
            checkOutDetails: ''
        },
        success: function (response) {


            if (response.done == true) {

                $('#customer_name').html(response.customer_name);
                $('#additional').html(response.additional);
                $('#sauna_price').html(response.sauna_price);
                $('#add_price').html(response.add_price);
                $('#check_in').html(response.check_in);
                $('#check_out').html(response.check_out);
                $('#total_price').html(response.total_price);
            } else {


                $('.edit_response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
            }
        }
    });

});



$(document).on('click', '#cutomerDetails', function (e) {
    e.preventDefault();

    var room_id = $(this).data('id');
    // alert(room_id);

    $.ajax({
        type: 'post',
        url: 'ajax.php',
        dataType: 'JSON',
        data: {
            room_id: room_id,
            cutomerDetails: ''
        },
        success: function (response) {


            if (response.done == true) {

                $('#customer_name').html(response.customer_name);
                $('#customer_contact_no').html(response.contact_no);
                $('#customer_email').html(response.email);
                $('#customer_address').html(response.address);
                $('#remaining_price').html(response.remaining_price);
            } else {


                $('.edit_response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
            }
        }
    });

});

$(document).on('click', '#editDetails', function (e) {
    e.preventDefault();

    var room_id = $(this).data('id');
    // alert(room_id);

    $.ajax({
        type: 'post',
        url: 'ajax.php',
        dataType: 'JSON',
        data: {
            room_id: room_id,
            editDetails: ''
        },
        success: function (response) {


            if (response.done == true) {

                $('#cus_name').val(response.customer_name);
                $('#edit_checkin').val(response.check_in);
                $('#edit_checkout').val(response.check_out);
                $('#edit_adds').val(response.additional);
                $('#edit_adds_price').val(response.add_price);
                $('#edit_room_price').val(response.room_price);
                $('#getEditID').val(response.booking_id);
                $('#edit_total_price').html(response.total_price);
            } else {


                $('.edit_response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
            }
        }
    });

});

$('#editInfoDetails').submit(function () {
    // var cus_name            = $('#cus_name').val();
    var edit_adds_price     = $('#edit_adds_price').val();
    var edit_room_price     = $('#edit_room_price').val();
    var booking_edit_id     = $('#getEditID').val();
    var total_price         = document.getElementById('edit_total_price').innerHTML;

    $.ajax({
        type: 'post',
        url: 'ajax.php',
        dataType: 'JSON',
        data: {
            edit_adds_price: edit_adds_price,
            edit_room_price: edit_room_price,
            booking_edit_id: booking_edit_id,
            total_price:total_price,
            edit_info: ''
        },
        success: function (response) {
            if (response.done == true) {
                $('#editDetailsModal').modal('hide');
                window.location.href = 'index.php?room_mang';
            } else {
                $('.response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
            }
        }
    });

    return false;
});


$(document).on('click', '#checkInRoom', function (e) {
    e.preventDefault();

    var room_id = $(this).data('id');
    // alert(room_id);

    $.ajax({
        type: 'post',
        url: 'ajax.php',
        dataType: 'JSON',
        data: {
            room_id: room_id,
            booked_room: ''
        },
        success: function (response) {
            if (response.done == true) {
                $('#room_id').val(room_id);
                $('#getCustomerName').html(response.name);
                $('#getRoomType').html(response.room_type);
                $('#getRoomNo').html(response.room_no);
                $('#getCheckIn').html(response.check_in);
                $('#getCheckOut').html(response.check_out);
                $('#getAdditional').html(response.additional);
                $('#getRoomPrice').html(response.room_price);
                $('#getAddPrice').html(response.add_price);
                $('#getTotalPrice').html(response.total_price + '/-');
                $('#getBookingID').val(response.booking_id);
                $('#checkIn').modal('show');
            } else {
                alert(response.data);
            }
        }
    });

});


$('#advancePayment').submit(function () {

    var booking_id          = $('#getBookingID').val();
    var advance_payment     = $('#advance_payment').val();

    $.ajax({
        type: 'post',
        url: 'ajax.php',
        dataType: 'JSON',
        data: {
            booking_id: booking_id,
            advance_payment: advance_payment,
            check_in_room:''
        },
        success: function (response) {
            if (response.done == true) {
                $('#checkIn').modal('hide');
                window.location.href = 'index.php?room_mang';
            } else {
                $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
            }
        }
    });

    return false;
});


$(document).on('click', '#checkOutRoom', function (e) {
    e.preventDefault();

    var room_id = $(this).data('id');

    $.ajax({
        type: 'post',
        url: 'ajax.php',
        dataType: 'JSON',
        data: {
            room_id: room_id,
            booked_room: ''
        },
        success: function (response) {
            if (response.done == true) {
                // alert(response.check_out);
                $('#getCustomerName_n').html(response.name);
                $('#getRoomType_n').html(response.room_type);
                $('#getRoomNo_n').html(response.room_no);
                $('#getCheckIn_n').html(response.check_in);
                $('#getCheckOut_n').html(response.check_out);
                $('#getAdditional_n').html(response.additional);
                $('#getRoomPrice_n').html(response.room_price);
                $('#getAddPrice_n').html(response.add_price);
                $('#getTotalPrice_n').html(response.total_price + '/-');
                $('#getRemainingPrice_n').html(response.remaining_price + '/-');
                $('#getBookingId_n').val(response.booking_id);
                $('#checkOut').modal('show');
            } else {
                alert(response.data);
            }
        }
    });

});


// $(document).on('click', '#checkOutSpa', function (e) {
//     e.preventDefault();

//     var sauna_id = $(this).data('id');
//     // alert(sauna_id);
//     $.ajax({
//         type: 'post',
//         url: 'ajax.php',
//         dataType: 'JSON',
//         data: {
//             sauna_id: sauna_id,
//             booked_sauna: ''
//         },
//         success: function (response) {
//             if (response.done == true) {
//                 $('#getName').html(response.name);
//                 $('#getAddOns').html(response.additional);
//                 $('#getSaunaPrice').html(response.sauna_price);
//                 $('#getAddOnsPrice').html(response.add_price);
//                 $('#getCheckIN').html(response.check_in);
//                 $('#getCheckOUT').html(response.check_out);
//                 $('#getTotalPrice').html(response.total_price);
//                 $('#checkOutSauna').modal('show');
//             } else {
//                 alert(response.data);
//             }
//         }
//     });

// });


$('#checkOutRoom_n').submit(function () {
    var booking_id = $('#getBookingId_n').val();
    var remaining_amount = $('#remaining_amount').val();

    console.log(booking_id);

    $.ajax({
        type: 'post',
        url: 'ajax.php',
        dataType: 'JSON',
        data: {
            booking_id: booking_id,
            remaining_amount: remaining_amount,
            check_out_room:''
        },
        success: function (response) {
            if (response.done == true) {
                $('#checkIn').modal('hide');
                window.location.href = 'index.php?room_mang';
            } else {
                $('.checkout-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
            }
        }
    });

    return false;

});


$(document).on('click', '#complaint', function (e) {
    e.preventDefault();

    var complaint_id = $(this).data('id');
    console.log(complaint_id);
    $('#complaint_id').val(complaint_id);

});

$(document).on('click', '#extend', function (e) {
    e.preventDefault();

    var extend_id = $(this).data('id');
    // console.log(complaint_id);
    $('#extend_id').val(extend_id);

});

$(document).on('click', '#checkOutDetails', function (e) {
    e.preventDefault();

    var check_sauna_id = $(this).data('id');
    // console.log(complaint_id);
    $('#checkout_id').val(check_sauna_id);

});



$('#extendForm').submit(function () {
    var extend_hours 		= $('#extend_hours').val();
    var extend_price		= $('#extend_price').val();
    var booking_id          = $('#getExtendId').val();
console.log(booking_id);
   $.ajax({
       type: 'post',
       url: 'ajax.php',
       dataType: 'JSON',
       data: {
           extend_hours: extend_hours,
           extend_price: extend_price,
           booking_id: booking_id,
           extended_hours:''
       },
       success: function (response) {
           if (response.done == true) {
               $('#extendHrs').modal('hide');
               window.location.href = 'index.php?room_mang';
           } else {
               $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
           }
       }
   });

   return false;
});


$('#extendFormTwo').submit(function () {
    var extend_hours_two 		= $('#extend_hours_two').val();
    var extend_price_two		= $('#extend_price_two').val();
    var booking_id_two          = $('#getExtendIdTwo').val();
console.log(booking_id_two);
   $.ajax({
       type: 'post',
       url: 'ajax.php',
       dataType: 'JSON',
       data: {
           extend_hours_two: extend_hours_two,
           extend_price_two: extend_price_two,
           booking_id_two: booking_id_two,
           extended_hours_two:''
       },
       success: function (response) {
           if (response.done == true) {
                $('#extendHrsTwo').modal('hide');
               window.location.href = 'index.php?room_mang';
           } else {
               $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
           }
       }
   });

   return false;
});


$('#extendFormThree').submit(function () {
    var extend_hours_three 		= $('#extend_hours_three').val();
    var extend_price_three		= $('#extend_price_three').val();
    var booking_id_three          = $('#getExtendIdThree').val();
// console.log(booking_id_two);
   $.ajax({
       type: 'post',
       url: 'ajax.php',
       dataType: 'JSON',
       data: {
           extend_hours_three: extend_hours_three,
           extend_price_three: extend_price_three,
           booking_id_three: booking_id_three,
           extended_hours_three:''
       },
       success: function (response) {
           if (response.done == true) {
                $('#extendHrsThree').modal('hide');
               window.location.href = 'index.php?room_mang';
           } else {
               $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
           }
       }
   });

   return false;
});

$('#extendFormFour').submit(function () {
    var extend_hours_four 		= $('#extend_hours_four').val();
    var extend_price_four		= $('#extend_price_four').val();
    var booking_id_four          = $('#getExtendIdFour').val();
// console.log(booking_id_two);
   $.ajax({
       type: 'post',
       url: 'ajax.php',
       dataType: 'JSON',
       data: {
           extend_hours_four: extend_hours_four,
           extend_price_four: extend_price_four,
           booking_id_four: booking_id_four,
           extended_hours_four:''
       },
       success: function (response) {
           if (response.done == true) {
                $('#extendHrsFour').modal('hide');
               window.location.href = 'index.php?room_mang';
           } else {
               $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
           }
       }
   });

   return false;
});

$('#extendFormFive').submit(function () {
    var extend_hours_five 		= $('#extend_hours_five').val();
    var extend_price_five		= $('#extend_price_five').val();
    var booking_id_five          = $('#getExtendIdFive').val();
// console.log(booking_id_two);
   $.ajax({
       type: 'post',
       url: 'ajax.php',
       dataType: 'JSON',
       data: {
           extend_hours_five: extend_hours_five,
           extend_price_five: extend_price_five,
           booking_id_five: booking_id_five,
           extended_hours_five:''
       },
       success: function (response) {
           if (response.done == true) {
                $('#extendHrsFive').modal('hide');
               window.location.href = 'index.php?room_mang';
           } else {
               $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
           }
       }
   });

   return false;
});

$('#extendFormSix').submit(function () {
    var extend_hours_six 		= $('#extend_hours_six').val();
    var extend_price_six		= $('#extend_price_six').val();
    var booking_id_six          = $('#getExtendIdSix').val();
// console.log(booking_id_two);
   $.ajax({
       type: 'post',
       url: 'ajax.php',
       dataType: 'JSON',
       data: {
           extend_hours_six: extend_hours_six,
           extend_price_six: extend_price_six,
           booking_id_six: booking_id_six,
           extended_hours_six:''
       },
       success: function (response) {
           if (response.done == true) {
                $('#extendHrsSix').modal('hide');
               window.location.href = 'index.php?room_mang';
           } else {
               $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
           }
       }
   });

   return false;
});

$('#extendFormSeven').submit(function () {
    var extend_hours_seven 		= $('#extend_hours_seven').val();
    var extend_price_seven		= $('#extend_price_seven').val();
    var booking_id_seven          = $('#getExtendIdSeven').val();
// console.log(booking_id_two);
   $.ajax({
       type: 'post',
       url: 'ajax.php',
       dataType: 'JSON',
       data: {
           extend_hours_seven: extend_hours_seven,
           extend_price_seven: extend_price_seven,
           booking_id_seven: booking_id_seven,
           extended_hours_seven:''
       },
       success: function (response) {
           if (response.done == true) {
                $('#extendHrsSeven').modal('hide');
               window.location.href = 'index.php?room_mang';
           } else {
               $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
           }
       }
   });

   return false;
});

$('#extendFormEight').submit(function () {
    var extend_hours_eight 		= $('#extend_hours_eight').val();
    var extend_price_eight		= $('#extend_price_eight').val();
    var booking_id_eight          = $('#getExtendIdEight').val();
// console.log(booking_id_two);
   $.ajax({
       type: 'post',
       url: 'ajax.php',
       dataType: 'JSON',
       data: {
           extend_hours_eight: extend_hours_eight,
           extend_price_eight: extend_price_eight,
           booking_id_eight: booking_id_eight,
           extended_hours_eight:''
       },
       success: function (response) {
           if (response.done == true) {
                $('#extendHrsEight').modal('hide');
               window.location.href = 'index.php?room_mang';
           } else {
               $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
           }
       }
   });

   return false;
});

$('#extendFormNine').submit(function () {
    var extend_hours_nine 		= $('#extend_hours_nine').val();
    var extend_price_nine		= $('#extend_price_nine').val();
    var booking_id_nine          = $('#getExtendIdNine').val();
    // console.log(booking_id_two);
   $.ajax({
       type: 'post',
       url: 'ajax.php',
       dataType: 'JSON',
       data: {
           extend_hours_nine: extend_hours_nine,
           extend_price_nine: extend_price_nine,
           booking_id_nine: booking_id_nine,
           extended_hours_nine:''
       },
       success: function (response) {
           if (response.done == true) {
                $('#extendHrsNine').modal('hide');
               window.location.href = 'index.php?room_mang';
           } else {
               $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
           }
       }
   });

   return false;
});

$('#extendFormTen').submit(function () {
    var extend_hours_ten 		= $('#extend_hours_ten').val();
    var extend_price_ten		= $('#extend_price_ten').val();
    var booking_id_ten          = $('#getExtendIdTen').val();
    // console.log(booking_id_two);
   $.ajax({
       type: 'post',
       url: 'ajax.php',
       dataType: 'JSON',
       data: {
           extend_hours_ten: extend_hours_ten,
           extend_price_ten: extend_price_ten,
           booking_id_ten: booking_id_ten,
           extended_hours_ten:''
       },
       success: function (response) {
           if (response.done == true) {
                $('#extendHrsTen').modal('hide');
               window.location.href = 'index.php?room_mang';
           } else {
               $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
           }
       }
   });

   return false;
});

$('#extendFormEleven').submit(function () {
    var extend_hours_eleven 		= $('#extend_hours_eleven').val();
    var extend_price_eleven		= $('#extend_price_eleven').val();
    var booking_id_eleven          = $('#getExtendIdEleven').val();
    // console.log(booking_id_two);
   $.ajax({
       type: 'post',
       url: 'ajax.php',
       dataType: 'JSON',
       data: {
            extend_hours_eleven: extend_hours_eleven,
            extend_price_eleven: extend_price_eleven,
            booking_id_eleven: booking_id_eleven,
           extended_hours_eleven:''
       },
       success: function (response) {
           if (response.done == true) {
                $('#extendHrsEleven').modal('hide');
               window.location.href = 'index.php?room_mang';
           } else {
               $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
           }
       }
   });

   return false;
});

$('#extendFormTwelve').submit(function () {
    var extend_hours_twelve 		= $('#extend_hours_twelve').val();
    var extend_price_twelve		= $('#extend_price_twelve').val();
    var booking_id_twelve          = $('#getExtendIdTwelve').val();
    // console.log(booking_id_two);
   $.ajax({
       type: 'post',
       url: 'ajax.php',
       dataType: 'JSON',
       data: {
            extend_hours_twelve: extend_hours_twelve,
            extend_price_twelve: extend_price_twelve,
            booking_id_twelve: booking_id_twelve,
           extended_hours_twelve:''
       },
       success: function (response) {
           if (response.done == true) {
                $('#extendHrsTwelve').modal('hide');
               window.location.href = 'index.php?room_mang';
           } else {
               $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
           }
       }
   });

   return false;
});

$('#extendFormThirteen').submit(function () {
    var extend_hours_thirteen 		= $('#extend_hours_thirteen').val();
    var extend_price_thirteen		= $('#extend_price_thirteen').val();
    var booking_id_thirteen          = $('#getExtendIdThirteen').val();
    // console.log(booking_id_two);
   $.ajax({
       type: 'post',
       url: 'ajax.php',
       dataType: 'JSON',
       data: {
            extend_hours_thirteen: extend_hours_thirteen,
            extend_price_thirteen: extend_price_thirteen,
            booking_id_thirteen: booking_id_thirteen,
           extended_hours_thirteen:''
       },
       success: function (response) {
           if (response.done == true) {
                $('#extendHrsThirteen').modal('hide');
               window.location.href = 'index.php?room_mang';
           } else {
               $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
           }
       }
   });

   return false;
});


$('#extendFormFourteen').submit(function () {
    var extend_hours_fourteen 		= $('#extend_hours_fourteen').val();
    var extend_price_fourteen		= $('#extend_price_fourteen').val();
    var booking_id_fourteen          = $('#getExtendIdFourteen').val();
    // console.log(booking_id_two);
   $.ajax({
       type: 'post',
       url: 'ajax.php',
       dataType: 'JSON',
       data: {
            extend_hours_fourteen: extend_hours_fourteen,
            extend_price_fourteen: extend_price_fourteen,
            booking_id_fourteen: booking_id_fourteen,
           extended_hours_fourteen:''
       },
       success: function (response) {
           if (response.done == true) {
                $('#extendHrsFourteen').modal('hide');
               window.location.href = 'index.php?room_mang';
           } else {
               $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
           }
       }
   });

   return false;
});


$('#extendFormFifteen').submit(function () {
    var extend_hours_fifteen 		= $('#extend_hours_fifteen').val();
    var extend_price_fifteen		= $('#extend_price_fifteen').val();
    var booking_id_fifteen          = $('#getExtendIdFifteen').val();
    // console.log(booking_id_two);
   $.ajax({
       type: 'post',
       url: 'ajax.php',
       dataType: 'JSON',
       data: {
            extend_hours_fifteen: extend_hours_fifteen,
            extend_price_fifteen: extend_price_fifteen,
            booking_id_fifteen: booking_id_fifteen,
           extended_hours_fifteen:''
       },
       success: function (response) {
           if (response.done == true) {
                $('#extendHrsFifteen').modal('hide');
               window.location.href = 'index.php?room_mang';
           } else {
               $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
           }
       }
   });

   return false;
});

$('#extendFormSixteen').submit(function () {
    var extend_hours_sixteen 		= $('#extend_hours_sixteen').val();
    var extend_price_sixteen		= $('#extend_price_sixteen').val();
    var booking_id_sixteen          = $('#getExtendIdSixteen').val();
    // console.log(booking_id_two);
   $.ajax({
       type: 'post',
       url: 'ajax.php',
       dataType: 'JSON',
       data: {
            extend_hours_sixteen: extend_hours_sixteen,
            extend_price_sixteen: extend_price_sixteen,
            booking_id_sixteen: booking_id_sixteen,
           extended_hours_sixteen:''
       },
       success: function (response) {
           if (response.done == true) {
                $('#extendHrsSixteen').modal('hide');
               window.location.href = 'index.php?room_mang';
           } else {
               $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
           }
       }
   });

   return false;
});

$('#extendFormSeventeen').submit(function () {
    var extend_hours_seventeen 		= $('#extend_hours_seventeen').val();
    var extend_price_seventeen		= $('#extend_price_seventeen').val();
    var booking_id_seventeen          = $('#getExtendIdSeventeen').val();
    // console.log(booking_id_two);
   $.ajax({
       type: 'post',
       url: 'ajax.php',
       dataType: 'JSON',
       data: {
            extend_hours_seventeen: extend_hours_seventeen,
            extend_price_seventeen: extend_price_seventeen,
            booking_id_seventeen: booking_id_seventeen,
           extended_hours_seventeen:''
       },
       success: function (response) {
           if (response.done == true) {
                $('#extendHrsSeventeen').modal('hide');
               window.location.href = 'index.php?room_mang';
           } else {
               $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
           }
       }
   });

   return false;
});

$('#extendFormEighteen').submit(function () {
    var extend_hours_eighteen 		= $('#extend_hours_eighteen').val();
    var extend_price_eighteen		= $('#extend_price_eighteen').val();
    var booking_id_eighteen          = $('#getExtendIdEighteen').val();
    // console.log(booking_id_two);
   $.ajax({
       type: 'post',
       url: 'ajax.php',
       dataType: 'JSON',
       data: {
            extend_hours_eighteen: extend_hours_eighteen,
            extend_price_eighteen: extend_price_eighteen,
            booking_id_eighteen: booking_id_eighteen,
           extended_hours_eighteen:''
       },
       success: function (response) {
           if (response.done == true) {
                $('#extendHrsEighteen').modal('hide');
               window.location.href = 'index.php?room_mang';
           } else {
               $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
           }
       }
   });

   return false;
});

$('#extendFormNineteen').submit(function () {
    var extend_hours_nineteen 		= $('#extend_hours_nineteen').val();
    var extend_price_nineteen		= $('#extend_price_nineteen').val();
    var booking_id_nineteen          = $('#getExtendIdNineteen').val();
    // console.log(booking_id_two);
   $.ajax({
       type: 'post',
       url: 'ajax.php',
       dataType: 'JSON',
       data: {
            extend_hours_nineteen: extend_hours_nineteen,
            extend_price_nineteen: extend_price_nineteen,
            booking_id_nineteen: booking_id_nineteen,
           extended_hours_nineteen:''
       },
       success: function (response) {
           if (response.done == true) {
                $('#extendHrsNineteen').modal('hide');
               window.location.href = 'index.php?room_mang';
           } else {
               $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
           }
       }
   });

   return false;
});

$('#extendFormTwenty').submit(function () {
    var extend_hours_twenty 		= $('#extend_hours_twenty').val();
    var extend_price_twenty 		= $('#extend_price_twenty').val();
    var booking_id_twenty           = $('#getExtendIdTwenty').val();
    // console.log(booking_id_two);
   $.ajax({
       type: 'post',
       url: 'ajax.php',
       dataType: 'JSON',
       data: {
            extend_hours_twenty: extend_hours_twenty,
            extend_price_twenty: extend_price_twenty,
            booking_id_twenty: booking_id_twenty,
           extended_hours_twenty:''
       },
       success: function (response) {
           if (response.done == true) {
                $('#extendHrsTwenty').modal('hide');
               window.location.href = 'index.php?room_mang';
           } else {
               $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
           }
       }
   });

   return false;
});

$('#extendFormTwentyone').submit(function () {
    var extend_hours_twentyone 		= $('#extend_hours_twentyone').val();
    var extend_price_twentyone 		= $('#extend_price_twentyone').val();
    var booking_id_twentyone           = $('#getExtendIdTwentyone').val();
    // console.log(booking_id_two);
   $.ajax({
       type: 'post',
       url: 'ajax.php',
       dataType: 'JSON',
       data: {
            extend_hours_twentyone: extend_hours_twentyone,
            extend_price_twentyone: extend_price_twentyone,
            booking_id_twentyone: booking_id_twentyone,
           extended_hours_twentyone:''
       },
       success: function (response) {
           if (response.done == true) {
                $('#extendHrsTwentyone').modal('hide');
               window.location.href = 'index.php?room_mang';
           } else {
               $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
           }
       }
   });

   return false;
});

$('#extendFormTwentytwo').submit(function () {
    var extend_hours_twentytwo 		= $('#extend_hours_twentytwo').val();
    var extend_price_twentytwo 		= $('#extend_price_twentytwo').val();
    var booking_id_twentytwo           = $('#getExtendIdTwentytwo').val();
    // console.log(booking_id_two);
   $.ajax({
       type: 'post',
       url: 'ajax.php',
       dataType: 'JSON',
       data: {
            extend_hours_twentytwo: extend_hours_twentytwo,
            extend_price_twentytwo: extend_price_twentytwo,
            booking_id_twentytwo: booking_id_twentytwo,
           extended_hours_twentytwo:''
       },
       success: function (response) {
           if (response.done == true) {
                $('#extendHrsTwentytwo').modal('hide');
               window.location.href = 'index.php?room_mang';
           } else {
               $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
           }
       }
   });

   return false;
});

$('#extendFormTwentythree').submit(function () {
    var extend_hours_twentythree 		= $('#extend_hours_twentythree').val();
    var extend_price_twentythree 		= $('#extend_price_twentythree').val();
    var booking_id_twentythree           = $('#getExtendIdTwentythree').val();
    // console.log(booking_id_two);
   $.ajax({
       type: 'post',
       url: 'ajax.php',
       dataType: 'JSON',
       data: {
            extend_hours_twentythree: extend_hours_twentythree,
            extend_price_twentythree: extend_price_twentythree,
            booking_id_twentythree: booking_id_twentythree,
           extended_hours_twentythree:''
       },
       success: function (response) {
           if (response.done == true) {
                $('#extendHrsTwentythree').modal('hide');
               window.location.href = 'index.php?room_mang';
           } else {
               $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
           }
       }
   });

   return false;
});

$('#extendFormTwentyfour').submit(function () {
    var extend_hours_twentyfour 		= $('#extend_hours_twentyfour').val();
    var extend_price_twentyfour 		= $('#extend_price_twentyfour').val();
    var booking_id_twentyfour           = $('#getExtendIdTwentyfour').val();
    // console.log(booking_id_two);
   $.ajax({
       type: 'post',
       url: 'ajax.php',
       dataType: 'JSON',
       data: {
            extend_hours_twentyfour: extend_hours_twentyfour,
            extend_price_twentyfour: extend_price_twentyfour,
            booking_id_twentyfour: booking_id_twentyfour,
           extended_hours_twentyfour:''
       },
       success: function (response) {
           if (response.done == true) {
                $('#extendHrsTwentyfour').modal('hide');
               window.location.href = 'index.php?room_mang';
           } else {
               $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
           }
       }
   });

   return false;
});

$('#extendFormTwentyfive').submit(function () {
    var extend_hours_twentyfive 		= $('#extend_hours_twentyfive').val();
    var extend_price_twentyfive 		= $('#extend_price_twentyfive').val();
    var booking_id_twentyfive           = $('#getExtendIdTwentyfive').val();
    // console.log(booking_id_two);
   $.ajax({
       type: 'post',
       url: 'ajax.php',
       dataType: 'JSON',
       data: {
            extend_hours_twentyfive: extend_hours_twentyfive,
            extend_price_twentyfive: extend_price_twentyfive,
            booking_id_twentyfive: booking_id_twentyfive,
           extended_hours_twentyfive:''
       },
       success: function (response) {
           if (response.done == true) {
                $('#extendHrsTwentyfive').modal('hide');
               window.location.href = 'index.php?room_mang';
           } else {
               $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
           }
       }
   });

   return false;
});

$('#extendFormTwentysix').submit(function () {
    var extend_hours_twentysix 		= $('#extend_hours_twentysix').val();
    var extend_price_twentysix 		= $('#extend_price_twentysix').val();
    var booking_id_twentysix           = $('#getExtendIdTwentysix').val();
    // console.log(booking_id_two);
   $.ajax({
       type: 'post',
       url: 'ajax.php',
       dataType: 'JSON',
       data: {
            extend_hours_twentysix: extend_hours_twentysix,
            extend_price_twentysix: extend_price_twentysix,
            booking_id_twentysix: booking_id_twentysix,
           extended_hours_twentysix:''
       },
       success: function (response) {
           if (response.done == true) {
                $('#extendHrsTwentysix').modal('hide');
               window.location.href = 'index.php?room_mang';
           } else {
               $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
           }
       }
   });

   return false;
});


$('#extendFormTwentyeight').submit(function () {
    var extend_hours_twentyeight 		= $('#extend_hours_twentyeight').val();
    var extend_price_twentyeight  		= $('#extend_price_twentyeight').val();
    var booking_id_twentyeight           = $('#getExtendIdTwentyeight').val();
    // console.log(booking_id_two);
   $.ajax({
       type: 'post',
       url: 'ajax.php',
       dataType: 'JSON',
       data: {
            extend_hours_twentyeight: extend_hours_twentyeight,
            extend_price_twentyeight: extend_price_twentyeight,
            booking_id_twentyeight: booking_id_twentyeight,
           extended_hours_twentyeight:''
       },
       success: function (response) {
           if (response.done == true) {
                $('#extendHrsTwentyeight').modal('hide');
               window.location.href = 'index.php?room_mang';
           } else {
               $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
           }
       }
   });

   return false;
});


$('#extendFormTwentynine').submit(function () {
    var extend_hours_twentynine 		= $('#extend_hours_twentynine').val();
    var extend_price_twentynine  		= $('#extend_price_twentynine').val();
    var booking_id_twentynine           = $('#getExtendIdTwentynine').val();
    // console.log(booking_id_two);
   $.ajax({
       type: 'post',
       url: 'ajax.php',
       dataType: 'JSON',
       data: {
            extend_hours_twentynine: extend_hours_twentynine,
            extend_price_twentynine: extend_price_twentynine,
            booking_id_twentynine: booking_id_twentynine,
           extended_hours_twentynine:''
       },
       success: function (response) {
           if (response.done == true) {
                $('#extendHrsTwentynine').modal('hide');
               window.location.href = 'index.php?room_mang';
           } else {
               $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
           }
       }
   });

   return false;
});


$('#extendFormThirty').submit(function () {
    var extend_hours_thirty 		= $('#extend_hours_thirty').val();
    var extend_price_thirty  		= $('#extend_price_thirty').val();
    var booking_id_thirty           = $('#getExtendIdThirty').val();
    // console.log(booking_id_two);
   $.ajax({
       type: 'post',
       url: 'ajax.php',
       dataType: 'JSON',
       data: {
            extend_hours_thirty: extend_hours_thirty,
            extend_price_thirty: extend_price_thirty,
            booking_id_thirty: booking_id_thirty,
           extended_hours_thirty:''
       },
       success: function (response) {
           if (response.done == true) {
                $('#extendHrsThirty').modal('hide');
               window.location.href = 'index.php?room_mang';
           } else {
               $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
           }
       }
   });

   return false;
});


$('#extendFormThirtyone').submit(function () {
    var extend_hours_thirtyone 		= $('#extend_hours_thirtyone').val();
    var extend_price_thirtyone  		= $('#extend_price_thirtyone').val();
    var booking_id_thirtyone           = $('#getExtendIdThirtyone').val();
    // console.log(booking_id_two);
   $.ajax({
       type: 'post',
       url: 'ajax.php',
       dataType: 'JSON',
       data: {
            extend_hours_thirtyone: extend_hours_thirtyone,
            extend_price_thirtyone: extend_price_thirtyone,
            booking_id_thirtyone: booking_id_thirtyone,
           extended_hours_thirtyone:''
       },
       success: function (response) {
           if (response.done == true) {
                $('#extendHrsThirtyone').modal('hide');
               window.location.href = 'index.php?room_mang';
           } else {
               $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
           }
       }
   });

   return false;
});


$('#extendFormThirtytwo').submit(function () {
    var extend_hours_thirtytwo 		= $('#extend_hours_thirtytwo').val();
    var extend_price_thirtytwo  		= $('#extend_price_thirtytwo').val();
    var booking_id_thirtytwo           = $('#getExtendIdThirtytwo').val();
    // console.log(booking_id_two);
   $.ajax({
       type: 'post',
       url: 'ajax.php',
       dataType: 'JSON',
       data: {
            extend_hours_thirtytwo: extend_hours_thirtytwo,
            extend_price_thirtytwo: extend_price_thirtytwo,
            booking_id_thirtytwo: booking_id_thirtytwo,
           extended_hours_thirtytwo:''
       },
       success: function (response) {
           if (response.done == true) {
                $('#extendHrsThirtytwo').modal('hide');
               window.location.href = 'index.php?room_mang';
           } else {
               $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
           }
       }
   });

   return false;
});

$('#extendFormThirtythree').submit(function () {
    var extend_hours_thirtythree 		= $('#extend_hours_thirtyhree').val();
    var extend_price_thirtythree  		= $('#extend_price_thirtyhree').val();
    var booking_id_thirtythree           = $('#getExtendIdThirtythree').val();
    // console.log(booking_id_two);
   $.ajax({
       type: 'post',
       url: 'ajax.php',
       dataType: 'JSON',
       data: {
            extend_hours_thirtythree: extend_hours_thirtythree,
            extend_price_thirtythree: extend_price_thirtythree,
            booking_id_thirtythree: booking_id_thirtythree,
           extended_hours_thirtythree:''
       },
       success: function (response) {
           if (response.done == true) {
                $('#extendHrsThirtythree').modal('hide');
               window.location.href = 'index.php?room_mang';
           } else {
               $('.payment-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
           }
       }
   });

   return false;
});



$(document).on('click', '#time_Out', function (e) {
    e.preventDefault();

    var time_Out = $(this).data('id');
    $.ajax({
        type: 'post',
        url: 'ajax.php',
        dataType: 'JSON',
        data: {
            time_Out: time_Out,
            timeOut: ''
        },
        success: function (response) {
            
            if(response.data == true) {
                window.location.reload();
                return false;
            }
        }
    });

});


// $('#addSauna').submit(function () {

//     var check_in_n      = $("#check_in_date_n").val();
//     var check_out_n     = $("#check_out_date_n").val();
//     var additional_n    = $("#additional_n").val();
//     var add_price_n     = $("#add_price_n").val();
//     var spa_price_n     = $("#spa_price_n").val();
//     var full_name_n     = $("#full_name_n").val();
//     var total_price_n   = document.getElementById('#total_price_n').innerHTML;

//     alert(check_in_n);

//     $.ajax({
//         type: 'post',
//         url: 'ajax.php',
//         dataType: 'JSON',
//         data: {
//             check_in_na:check_in_n,
//             check_out_na:check_out_n,
//             additional_na:additional_n,
//             add_price_na:add_price_n,
//             spa_price_na:spa_price_n,
//             full_name_na:full_name_n,
//             total_price_na:total_price_n,
//             add_sauna:''
//         },
//         success: function (response) {
//             if (response.done == true){
//                 document.getElementById("#addSauna").reset();
//                 $('.emp-response').html('<div class="alert bg-success alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>SPA / SAUNA Successfully Added</div>');
//             }else{
//                 $('.emp-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
//             }
//         }
//     });

//     return false;
// });

$('#addEmployee').submit(function () {

    var check_in_n   = $("#check_in_date_n").val();
    var check_out_n   = $("#check_out_date_n").val();
    var additional_n   = $("#additional_n").val();
    var add_price_n   = $("#add_price_n").val();
    var spa_price_n    = $("#spa_price_n").val();
    var full_name_n     = $("#full_name_n").val();
    var total_price_n   = document.getElementById('total_price_n').innerHTML;
    // alert(total_price_n);

    $.ajax({
        type: 'post',
        url: 'ajax.php',
        dataType: 'JSON',
        data: {
            check_in_na:check_in_n,
            check_out_na:check_out_n,
            additional_na:additional_n,
            add_price_na:add_price_n,
            spa_price_na:spa_price_n,
            full_name_na:full_name_n,
            total_price_na:total_price_n,
            add_employee:''
        },
        success: function (response) {
            if (response.done == true){
                document.getElementById("addEmployee").reset();
                $('.emp-response').html('<div class="alert bg-success alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>SPA / SAUNA Successfully Added</div>');
            }else{
                $('.emp-response').html('<div class="alert bg-danger alert-dismissable" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em>' + response.data + '</div>');
            }
        }
    });

    return false;
});