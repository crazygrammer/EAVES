<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#">
                    <em class="fa fa-home"></em>
                </a></li>
            <li class="active">SPA / SAUNA</li>
        </ol>
    </div><!--/.row-->

   

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">SPA / SAUNA Details:
                    <a href="index.php?add_emp" class="btn btn-secondary pull-right" style="border-radius:0%">Add SPA/SAUNA</a>
                </div>
                <div class="panel-body">
                    <?php
                    if (isset($_GET['error'])) {
                        echo "<div class='alert alert-danger'>
                                <span class='glyphicon glyphicon-info-sign'></span> &nbsp; Error on SAUNA Change !
                            </div>";
                    }
                    if (isset($_GET['success'])) {
                        echo "<div class='alert alert-success'>
                                <span class='glyphicon glyphicon-info-sign'></span> &nbsp; SAUNA Successfully Changed!
                            </div>";
                    }
                    ?>
                    <table class="table table-striped table-bordered table-responsive" cellspacing="0" width="100%"
                           id="rooms">
                        <thead>
                        <tr>
                            <th>SPA ID</th>
                            <th>Customer Name</th>
                            <th>Check In</th>
                            <th>Check Out</th>
                            <th>Additional</th>
                            <th>Add Ons Price</th>
                            <th>SPA PRICE</th>
                            <!-- <th>Total Price</th> -->
                            <!-- <th>Action</th> -->
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $staff_query = "SELECT * FROM spa_sauna ";
                        $staff_result = mysqli_query($connection, $staff_query);

                        if (mysqli_num_rows($staff_result) > 0) {
                            while ($staff = mysqli_fetch_assoc($staff_result)) { ?>
                                <tr>

                                    <td><?php echo $staff['spa_id']; ?></td>
                                    <td><?php echo $staff['customer_name']; ?></td>
                                    <td><?php echo date('F j, Y, g:i a', strtotime($staff['check_in'])); ?></td>
                                    <td><?php echo date('F j, Y, g:i a', strtotime($staff['checkout'])); ?></td>
                                    <td><?php echo $staff['additional']; ?></td>
                                    <td><?php echo $staff['add_on_price']; ?></td>
                                    <td><?php echo $staff['spa_price']; ?></td>
                                    
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- <div class="row">
        <div class="col-sm-12">
        <p class="back-link">Developed By allen_sdev</p>
        </div>
    </div> -->

</div>    <!--/.main-->