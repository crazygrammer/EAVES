<?php
if (isset($_GET['room_id'])){
    $get_room_id        = $_GET['room_id'];
    // $get_room_sql       = "SELECT * FROM room NATURAL JOIN room_type WHERE room_id = '$get_room_id'";
    $get_room_sql       = "
                            SELECT 
                                a.room_id,
                                a.room_type_id,
                                a.room_no,
                                a.status,
                                a.check_in_status,
                                a.check_out_status,
                                a.deleteStatus,
                                b.room_type_id,
                                b.room_type,
                                b.price,
                                b.max_person
                            FROM 
                                room a
                            LEFT JOIN 
                                room_type b ON b.room_type_id = a.room_type_id
                            WHERE 
                                a.room_id = '$get_room_id'";
    $get_room_result    = mysqli_query($connection,$get_room_sql);
    $get_room           = mysqli_fetch_assoc($get_room_result);

    $get_room_type_id   = $get_room['room_type_id'];
    $get_room_type      = $get_room['room_type'];
    $get_room_no        = $get_room['room_no'];
    $get_room_price     = $get_room['price'];
}

?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#">
                    <em class="fa fa-home"></em>
                </a></li>
            <li class="active">Reservation</li>
        </ol>
    </div><!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <form role="form" id="booking" method="POST" data-toggle="validator">
                <div class="response"></div>
                <div class="col-lg-12">
                    <?php
                    if (isset($_GET['room_id'])){?>

                        <div class="panel panel-default">
                            <div class="panel-heading">Room Information:
                                
                            </div>
                            <div class="panel-body">
                                <div class="form-group col-lg-6">
                                    <label>Room Type</label>
                                    <select class="form-control" id="room_type">
                                        <option selected disabled>Select Room Type</option>
                                        <option selected value="<?= $get_room_type_id; ?>"><?= $get_room_type; ?></option>
                                    </select>
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>Room No</label>
                                    <select class="form-control" id="room_no"s>
                                        <option selected disabled>Select Room No</option>
                                        <option selected value="<?= $get_room_id; ?>"><?= $get_room_no; ?></option>
                                    </select>
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>Check In Date</label>
                                    <input type="text" class="form-control" placeholder="mm-dd-yyyy hh:mm" id="check_in_date" data-error="Select Check In Date" required>
                                    <div class="help-block with-errors"></div>
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>Check Out Date</label>
                                    <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="check_out_date" data-error="Select Check Out Date" required>
                                    <div class="help-block with-errors"></div>
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>Additional</label>
                                    <select id="additional" class="form-control">
                                      <option value=""></option>
                                      <option value="food">Food</option>
                                      <option value="beverages">Beverages</option>
                                      <option value="towel">Towel</option>
                                      <option value="bed">Bed</option>
                                      <option value="others">Others</option>
                                    </select>
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>Price</label>
                                    <input type="text" class="form-control" placeholder="Input Price" id="add_price" onkeyup="calculate()">
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>Room Price</label>
                                    <input type="text" class="form-control" placeholder="Input Room Price" id="room_price" onkeyup="calculate()" data-error="Input Room Price" required>
                                    <div class="help-block with-errors"></div>
                                </div>


                                    <div class="form-group col-lg-6">
                                        <div class="panel panel-orange panel-widget border-right">
                                            <div class="row no-padding"><em class="fa fa-xl  color-magg">Price List</em>
                                            <hr>
                                            <?php
                                                $query = "SELECT * FROM room_type";
                                                $result = mysqli_query($connection, $query);
                                                if (mysqli_num_rows($result) > 0) {
                                                    while ($res= mysqli_fetch_assoc($result)) {
                                                echo'<div class=""><span style="font-size:20px;">'.$res['room_type'].' &#8213; Php '.$res['price'].'.00</span></div>';
                                                    }
                                                }
                                            ?>
                                                <!-- <div class="text-muted">Room Rates</div> -->
                                            </div>
                                        </div>
                                    </div>


                                <div class="col-lg-12">
                                    <h4 style="font-weight: bold">Total Amount : <span id="total_price">0</span> /-</h4>
                                </div>
                            </div>
                        </div>
                    <?php } else{?>
                        <div class="panel panel-default">
                            <div class="panel-heading">Room Information:
                                
                            </div>
                            <div class="panel-body">
                                <div class="form-group col-lg-6">
                                    <label>Room Type</label>
                                    <select class="form-control" id="room_type" onchange="fetch_room(this.value);" required data-error="Select Room Type">
                                        <option selected disabled>Select Room Type</option>
                                        <?php
                                        $query  = "SELECT * FROM room_type";
                                        $result = mysqli_query($connection,$query);
                                        if (mysqli_num_rows($result) > 0){
                                            while ($room_type = mysqli_fetch_assoc($result)){
                                                echo '<option value="'.$room_type['room_type_id'].'">'.$room_type['room_type'].'</option>';
                                            }}
                                        ?>
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>Room No</label>
                                    <select class="form-control" id="room_no">

                                    </select>
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>Check In Date</label>
                                    <input type="text" class="form-control" placeholder="mm-dd-yyyy" id="check_in_date" data-error="Select Check In Date" required>
                                    <div class="help-block with-errors"></div>
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>Check Out Date</label>
                                    <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="check_out_date" data-error="Select Check Out Date" required>
                                    <div class="help-block with-errors"></div>
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>Additional</label>
                                    <select id="additional" class="form-control">
                                      <option value=""></option>
                                      <option value="food">Food</option>
                                      <option value="beverages">Beverages</option>
                                      <option value="towel">Towel</option>
                                      <option value="bed">Bed</option>
                                      <option value="others">Others</option>
                                    </select>
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>Price</label>
                                    <input type="text" class="form-control" placeholder="Input Price" id="add_price" onkeyup="calculate()">
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>Room Price</label>
                                    <input type="text" class="form-control" placeholder="Input Room Price" id="room_price" onkeyup="calculate()" data-error="Input Room Price" required>
                                    <div class="help-block with-errors"></div>
                                </div>

                                <div class="col-lg-12">
                                    <h4 style="font-weight: bold">Total Amount : <span id="total_price">0</span> /-</h4>
                                </div>
                            </div>
                        </div>
                    <?php }
                    ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">Customer Detail:</div>
                        <div class="panel-body">
                            <div class="form-group col-lg-6">
                                <label>First Name</label>
                                <input class="form-control" placeholder="First Name" id="first_name" data-error="Enter First Name" required>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group col-lg-6">
                                <label>Last Name</label>
                                <input class="form-control" placeholder="Last Name" id="last_name" data-error="Enter Last Name" required>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group col-lg-6">
                                <label>Contact Number</label>
                                <input type="number" class="form-control" data-error="Enter Min 10 Digit" data-minlength="10" placeholder="Contact No" id="contact_no" required>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group col-lg-6">
                                <label>Email Address</label>
                                <input type="email" class="form-control" placeholder="Email Address" id="email" data-error="Enter Valid Email Address" required>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group col-lg-12">
                                <label>Residential Address</label>
                                <input type="text" class="form-control" placeholder="Full Address" id="address" data-error="Enter Residential Address" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-lg btn-success pull-right" style="border-radius:0%">Submit</button>
                </div>
            </form>
        </div>
    </div>

</div>    <!--/.main-->


<!-- Booking Confirmation-->
<div id="bookingConfirm" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><b>Room Booking</b></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert bg-success alert-dismissable" role="alert"><em class="fa fa-lg fa-check-circle">&nbsp;</em>Room Successfully Booked</div>
                        <table class="table table-striped table-bordered table-responsive">

                            <tbody>
                            <tr>
                                <td><b>Customer Name</b></td>
                                <td id="getCustomerName"></td>
                            </tr>
                            <tr>
                                <td><b>Room Type</b></td>
                                <td id="getRoomType"></td>
                            </tr>
                            <tr>
                                <td><b>Room No</b></td>
                                <td id="getRoomNo"></td>
                            </tr>
                            <tr>
                                <td><b>Check In</b></td>
                                <td id="getCheckIn"></td>
                            </tr>
                            <tr>
                                <td><b>Check Out</b></td>
                                <td id="getCheckOut"></td>
                            </tr>
                            <tr>
                                <td><b>Additional</b></td>
                                <td id="getAdditional"></td>
                            </tr>
                            <tr>
                                <td><b>Room Price</b></td>
                                <td id="getRoomPrice"></td>
                            </tr>
                            <tr>
                                <td><b>Add Ons Price</b></td>
                                <td id="getAddPrice"></td>
                            </tr>
                            <tr>
                                <td><b>Total Amount</b></td>
                                <td id="getTotalPrice"></td>
                            </tr>
                            <tr>
                                <td><b>Payment Status</b></td>
                                <td id="getPaymentStaus"></td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                
                <a class="btn btn-primary" style="border-radius:60px;" href="index.php?room_mang"><i class="fa fa-check-circle"></i></a>
            
            </div>
        </div>

    </div>
</div>


