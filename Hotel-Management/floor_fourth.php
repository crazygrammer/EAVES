<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">5th Floor</li>
			</ol>
		</div><!--/.row-->

		<div class="panel panel-container">
			<div class="row">
					<?php
						$room_twentyeight_query = "
							SELECT 
							a.room_id,
							a.room_type_id,
							a.room_no,
							a.status,
							a.check_in_status,
							a.check_out_status,
							a.deleteStatus,
							b.room_type_id,
							b.room_type,
							b.price,
							b.max_person
						FROM 
							room a
						LEFT JOIN 
							room_type b ON b.room_type_id = a.room_type_id
						
						WHERE 
							deleteStatus = 0 AND a.room_id = 28";
						$rooms_twentyeight_result = mysqli_query($connection, $room_twentyeight_query); 
					?>
					<?php if (mysqli_num_rows($rooms_twentyeight_result) > 0) {
                        	while ($rooms_twentyeight = mysqli_fetch_assoc($rooms_twentyeight_result)) { 
					?>
					<?php 	if ($rooms_twentyeight['status']  == 0) {	

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-blue"></em>
												<a class="btn btn-primary btn-lg" href="index.php?reservation&amp;room_id=28&amp;room_type_id=4">Room 501</a>
											</div>
										</div>
									  </div>';
							}else{

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-red"></em>
												<button class="btn btn-danger btn-lg" id="extendBtnTwentyeight" data-id="" data-toggle="modal" data-target="#extendHrsTwentyeight" href="">Room 501</button>
											</div>
										</div>
									  </div>';
							}
					?>
					<?php } } ?>

					<?php
						$room_twentynine_query = "
							SELECT 
							a.room_id,
							a.room_type_id,
							a.room_no,
							a.status,
							a.check_in_status,
							a.check_out_status,
							a.deleteStatus,
							b.room_type_id,
							b.room_type,
							b.price,
							b.max_person
						FROM 
							room a
						LEFT JOIN 
							room_type b ON b.room_type_id = a.room_type_id
						
						WHERE 
							deleteStatus = 0 AND a.room_id = 29";
						$rooms_twentynine_result = mysqli_query($connection, $room_twentynine_query); 
					?>
					<?php if (mysqli_num_rows($rooms_twentynine_result) > 0) {
                        	while ($rooms_twentynine = mysqli_fetch_assoc($rooms_twentynine_result)) { 
					?>
					<?php 	if ($rooms_twentynine['status']  == 0) {	

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-blue"></em>
												<a class="btn btn-primary btn-lg" href="index.php?reservation&amp;room_id=29&amp;room_type_id=4">Room 502</a>
											</div>
										</div>
									  </div>';
							}else{

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-red"></em>
												<button class="btn btn-danger btn-lg" id="extendBtnTwentynine" data-id="" data-toggle="modal" data-target="#extendHrsTwentynine" href="">Room 502</button>
											</div>
										</div>
									  </div>';
							}
					?>
					<?php } } ?>

					<?php
						$room_thirty_query = "
							SELECT 
							a.room_id,
							a.room_type_id,
							a.room_no,
							a.status,
							a.check_in_status,
							a.check_out_status,
							a.deleteStatus,
							b.room_type_id,
							b.room_type,
							b.price,
							b.max_person
						FROM 
							room a
						LEFT JOIN 
							room_type b ON b.room_type_id = a.room_type_id
						
						WHERE 
							deleteStatus = 0 AND a.room_id = 30";
						$rooms_thirty_result = mysqli_query($connection, $room_thirty_query); 
					?>
					<?php if (mysqli_num_rows($rooms_thirty_result) > 0) {
                        	while ($rooms_thirty = mysqli_fetch_assoc($rooms_thirty_result)) { 
					?>
					<?php 	if ($rooms_thirty['status']  == 0) {	

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-blue"></em>
												<a class="btn btn-primary btn-lg" href="index.php?reservation&amp;room_id=30&amp;room_type_id=4">Room 503</a>
											</div>
										</div>
									  </div>';
							}else{

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-red"></em>
												<button class="btn btn-danger btn-lg" id="extendBtnThirty" data-id="" data-toggle="modal" data-target="#extendHrsThirty" href="">Room 503</button>
											</div>
										</div>
									  </div>';
							}
					?>
					<?php } } ?>
			</div>
			<div class="row">
					<?php
						$room_thirtyone_query = "
							SELECT 
							a.room_id,
							a.room_type_id,
							a.room_no,
							a.status,
							a.check_in_status,
							a.check_out_status,
							a.deleteStatus,
							b.room_type_id,
							b.room_type,
							b.price,
							b.max_person
						FROM 
							room a
						LEFT JOIN 
							room_type b ON b.room_type_id = a.room_type_id
						
						WHERE 
							deleteStatus = 0 AND a.room_id = 31";
						$rooms_thirtyone_result = mysqli_query($connection, $room_thirtyone_query); 
					?>
					<?php if (mysqli_num_rows($rooms_thirtyone_result) > 0) {
                        	while ($rooms_thirtyone = mysqli_fetch_assoc($rooms_thirtyone_result)) { 
					?>
					<?php 	if ($rooms_thirtyone['status']  == 0) {	

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-blue"></em>
												<a class="btn btn-primary btn-lg" href="index.php?reservation&amp;room_id=31&amp;room_type_id=4">Room 504</a>
											</div>
										</div>
									  </div>';
							}else{

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-red"></em>
												<button class="btn btn-danger btn-lg" id="extendBtnThirtyone" data-id="" data-toggle="modal" data-target="#extendHrsThirtyone" href="">Room 504</button>
											</div>
										</div>
									  </div>';
							}
					?>
					<?php } } ?>

					<?php
						$room_thirtytwo_query = "
							SELECT 
							a.room_id,
							a.room_type_id,
							a.room_no,
							a.status,
							a.check_in_status,
							a.check_out_status,
							a.deleteStatus,
							b.room_type_id,
							b.room_type,
							b.price,
							b.max_person
						FROM 
							room a
						LEFT JOIN 
							room_type b ON b.room_type_id = a.room_type_id
						
						WHERE 
							deleteStatus = 0 AND a.room_id = 32";
						$rooms_thirtytwo_result = mysqli_query($connection, $room_thirtytwo_query); 
					?>
					<?php if (mysqli_num_rows($rooms_thirtytwo_result) > 0) {
                        	while ($rooms_thirtytwo = mysqli_fetch_assoc($rooms_thirtytwo_result)) { 
					?>
					<?php 	if ($rooms_thirtytwo['status']  == 0) {	

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-blue"></em>
												<a class="btn btn-primary btn-lg" href="index.php?reservation&amp;room_id=32&amp;room_type_id=4">Room 505</a>
											</div>
										</div>
									  </div>';
							}else{

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-red"></em>
												<button class="btn btn-danger btn-lg" id="extendBtnThirtytwo" data-id="" data-toggle="modal" data-target="#extendHrsThirtytwo" href="">Room 505</button>
											</div>
										</div>
									  </div>';
							}
					?>
					<?php } } ?>

					<?php
						$room_thirtythree_query = "
							SELECT 
							a.room_id,
							a.room_type_id,
							a.room_no,
							a.status,
							a.check_in_status,
							a.check_out_status,
							a.deleteStatus,
							b.room_type_id,
							b.room_type,
							b.price,
							b.max_person
						FROM 
							room a
						LEFT JOIN 
							room_type b ON b.room_type_id = a.room_type_id
						
						WHERE 
							deleteStatus = 0 AND a.room_id = 33";
						$rooms_thirtythree_result = mysqli_query($connection, $room_thirtythree_query); 
					?>
					<?php if (mysqli_num_rows($rooms_thirtythree_result) > 0) {
                        	while ($rooms_thirtythree = mysqli_fetch_assoc($rooms_thirtythree_result)) { 
					?>
					<?php 	if ($rooms_thirtythree['status']  == 0) {	

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-blue"></em>
												<a class="btn btn-primary btn-lg" href="index.php?reservation&amp;room_id=33&amp;room_type_id=4">Room 506</a>
											</div>
										</div>
									  </div>';
							}else{

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-red"></em>
												<button class="btn btn-danger btn-lg" id="extendBtnThirtythree" data-id="" data-toggle="modal" data-target="#extendHrsThirtythree" href="">Room 506</button>
											</div>
										</div>
									  </div>';
							}
					?>
					<?php } } ?>
			</div>
			<hr>

			<div class="row">
				<div class="col-xs-6 col-md-2 col-lg-2 no-padding">
					
				</div>

				<div class="col-xs-6 col-md-4 col-lg-4 no-padding">
					<div class="panel panel-red panel-widget border-right">
						<div class="row no-padding"><em class="fa fa-xl fa-money color-blue"></em>
							<div class="large">Php <?php include 'counters/count-fourthfloor.php'?></div>
							<div class="text-muted">Total Earnings 5th Floor</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>



<div id="extendHrsTwentyeight" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<?php
						$sqlTwentyeight = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person,
						c.check_out,
						c.payment_status,
						c.total_price,
						c.extend_hours,
						c.booking_date,
						c.booking_id
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					LEFT JOIN
						booking c ON c.room_id = a.room_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 28 AND c.payment_status = 0";
						$resultTwentyeight = mysqli_query($connection, $sqlTwentyeight);
						$fetchTwentyeight = mysqli_fetch_assoc($resultTwentyeight);

					?>

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Extend</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h4 class="modal-dialog text-center">Room 501 is about to End, Do you want to Extend?</h4>
                            <form role="form" id="extendFormTwentyeight">
                                <div class="payment-response"></div>
                                <div class="form-group col-lg-12">
                                    <label>Extended Minutes</label>
                                    <input type="number" class="form-control" id="extend_hours_twentyeight"
                                           placeholder="Please Enter Minutes Here..">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Extend Price</label>
                                    <input type="number" class="form-control" id="extend_price_twentyeight"
                                           placeholder="Please Enter Amounts Here..">
                                </div>
                                <input type="hidden" id="getExtendIdTwentyeight" value="<?php echo $fetchTwentyeight['booking_id'] ?>">
                                <button type="submit" class="btn btn-primary pull-right">Extend</button>
                                <!-- <button type="" class="btn btn-primary pull-right">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


	<div id="extendHrsTwentynine" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<?php
						$sqlTwentynine = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person,
						c.check_out,
						c.payment_status,
						c.total_price,
						c.extend_hours,
						c.booking_date,
						c.booking_id
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					LEFT JOIN
						booking c ON c.room_id = a.room_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 29 AND c.payment_status = 0";
						$resultTwentynine = mysqli_query($connection, $sqlTwentynine);
						$fetchTwentynine = mysqli_fetch_assoc($resultTwentynine);

					?>

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Extend</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h4 class="modal-dialog text-center">Room 502 is about to End, Do you want to Extend?</h4>
                            <form role="form" id="extendFormTwentynine">
                                <div class="payment-response"></div>
                                <div class="form-group col-lg-12">
                                    <label>Extended Minutes</label>
                                    <input type="number" class="form-control" id="extend_hours_twentynine"
                                           placeholder="Please Enter Minutes Here..">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Extend Price</label>
                                    <input type="number" class="form-control" id="extend_price_twentynine"
                                           placeholder="Please Enter Amounts Here..">
                                </div>
                                <input type="hidden" id="getExtendIdTwentynine" value="<?php echo $fetchTwentynine['booking_id'] ?>">
                                <button type="submit" class="btn btn-primary pull-right">Extend</button>
                                <!-- <button type="" class="btn btn-primary pull-right">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


	<div id="extendHrsThirty" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<?php
						$sqlThirty = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person,
						c.check_out,
						c.payment_status,
						c.total_price,
						c.extend_hours,
						c.booking_date,
						c.booking_id
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					LEFT JOIN
						booking c ON c.room_id = a.room_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 30 AND c.payment_status = 0";
						$resultThirty = mysqli_query($connection, $sqlThirty);
						$fetchThirty = mysqli_fetch_assoc($resultThirty);

					?>

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Extend</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h4 class="modal-dialog text-center">Room 503 is about to End, Do you want to Extend?</h4>
                            <form role="form" id="extendFormThirty">
                                <div class="payment-response"></div>
                                <div class="form-group col-lg-12">
                                    <label>Extended Minutes</label>
                                    <input type="number" class="form-control" id="extend_hours_thirty"
                                           placeholder="Please Enter Minutes Here..">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Extend Price</label>
                                    <input type="number" class="form-control" id="extend_price_thirty"
                                           placeholder="Please Enter Amounts Here..">
                                </div>
                                <input type="hidden" id="getExtendIdThirty" value="<?php echo $fetchThirty['booking_id'] ?>">
                                <button type="submit" class="btn btn-primary pull-right">Extend</button>
                                <!-- <button type="" class="btn btn-primary pull-right">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


	<div id="extendHrsThirtyone" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<?php
						$sqlThirtyone = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person,
						c.check_out,
						c.payment_status,
						c.total_price,
						c.extend_hours,
						c.booking_date,
						c.booking_id
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					LEFT JOIN
						booking c ON c.room_id = a.room_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 31 AND c.payment_status = 0";
						$resultThirtyone = mysqli_query($connection, $sqlThirtyone);
						$fetchThirtyone = mysqli_fetch_assoc($resultThirtyone);

					?>

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Extend</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h4 class="modal-dialog text-center">Room 504 is about to End, Do you want to Extend?</h4>
                            <form role="form" id="extendFormThirtyone">
                                <div class="payment-response"></div>
                                <div class="form-group col-lg-12">
                                    <label>Extended Minutes</label>
                                    <input type="number" class="form-control" id="extend_hours_thirtyone"
                                           placeholder="Please Enter Minutes Here..">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Extend Price</label>
                                    <input type="number" class="form-control" id="extend_price_thirtyone"
                                           placeholder="Please Enter Amounts Here..">
                                </div>
                                <input type="hidden" id="getExtendIdThirtyone" value="<?php echo $fetchThirtyone['booking_id'] ?>">
                                <button type="submit" class="btn btn-primary pull-right">Extend</button>
                                <!-- <button type="" class="btn btn-primary pull-right">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


	<div id="extendHrsThirtytwo" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<?php
						$sqlThirtytwo = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person,
						c.check_out,
						c.payment_status,
						c.total_price,
						c.extend_hours,
						c.booking_date,
						c.booking_id
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					LEFT JOIN
						booking c ON c.room_id = a.room_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 32 AND c.payment_status = 0";
						$resultThirtytwo = mysqli_query($connection, $sqlThirtytwo);
						$fetchThirtytwo = mysqli_fetch_assoc($resultThirtytwo);

					?>

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Extend</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h4 class="modal-dialog text-center">Room 505 is about to End, Do you want to Extend?</h4>
                            <form role="form" id="extendFormThirtytwo">
                                <div class="payment-response"></div>
                                <div class="form-group col-lg-12">
                                    <label>Extended Minutes</label>
                                    <input type="number" class="form-control" id="extend_hours_thirtytwo"
                                           placeholder="Please Enter Minutes Here..">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Extend Price</label>
                                    <input type="number" class="form-control" id="extend_price_thirtytwo"
                                           placeholder="Please Enter Amounts Here..">
                                </div>
                                <input type="hidden" id="getExtendIdThirtytwo" value="<?php echo $fetchThirtytwo['booking_id'] ?>">
                                <button type="submit" class="btn btn-primary pull-right">Extend</button>
                                <!-- <button type="" class="btn btn-primary pull-right">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


	<div id="extendHrsThirtythree" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<?php
						$sqlThirtythree = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person,
						c.check_out,
						c.payment_status,
						c.total_price,
						c.extend_hours,
						c.booking_date,
						c.booking_id
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					LEFT JOIN
						booking c ON c.room_id = a.room_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 33 AND c.payment_status = 0";
						$resultThirtythree = mysqli_query($connection, $sqlThirtythree);
						$fetchThirtythree = mysqli_fetch_assoc($resultThirtythree);

					?>

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Extend</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h4 class="modal-dialog text-center">Room 506 is about to End, Do you want to Extend?</h4>
                            <form role="form" id="extendFormThirtythree">
                                <div class="payment-response"></div>
                                <div class="form-group col-lg-12">
                                    <label>Extended Minutes</label>
                                    <input type="number" class="form-control" id="extend_hours_thirtythree"
                                           placeholder="Please Enter Minutes Here..">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Extend Price</label>
                                    <input type="number" class="form-control" id="extend_price_thirtythree"
                                           placeholder="Please Enter Amounts Here..">
                                </div>
                                <input type="hidden" id="getExtendIdThirtythree" value="<?php echo $fetchThirtythree['booking_id'] ?>">
                                <button type="submit" class="btn btn-primary pull-right">Extend</button>
                                <!-- <button type="" class="btn btn-primary pull-right">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>