
<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
    <div class="profile-sidebar">
        <div class="profile-userpic">
            <img src="img/user.png" class="img-responsive" alt="">
        </div>
        <div class="profile-usertitle">
            <div class="profile-usertitle-name"><?php echo $user['name'];?></div>
            <div class="profile-usertitle-status">
            <?php if($_SESSION['restrictions'] == 0){ ?>
                <span class="indicator label-success"></span>Reservations Agent
                <?php }else{ ?>
                    <span class="indicator label-success"></span>Administrator
                <?php } ?>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="divider"></div>
    <ul class="nav menu">
    <?php 
        if (isset($_GET['dashboard'])){ ?>
            <li class="active">
                <a href="index.php?dashboard"><em class="fa fa-dashboard">&nbsp;</em>
                    Dashboard
                </a>
            </li>
        <?php } else{?>
            <li>
                <a href="index.php?dashboard"><em class="fa fa-dashboard">&nbsp;</em>
                    Dashboard
                </a>
            </li>
        <?php }
        if (isset($_GET['reservation'])){ ?>
            <li class="active">
                <a class="" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample"><em class="fa fa-calendar">&nbsp;</em>
                Check-IN Room
                </a>
            </li>
            <div class="collapse" id="collapseExample">
                <div class="card card-body">
                <ul>
                    <li><a href="index.php?first_floor">2nd Floor Deluxe Rooms</a></li>
                    <li><a href="index.php?second_floor">3rd Floor View Room</a></li>
                    <li><a href="index.php?third_floor">4th Floor Superior Room</a></li>
                    <li><a href="index.php?fourth_floor">5th Floor Family Room</a></li>
                </ul>
                </div>
            </div>
        <?php } else{?>
            <li>
            <a class="" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample"><em class="fa fa-calendar">&nbsp;</em>
                Check-IN Room
                </a>
            </li>
            <div class="collapse" id="collapseExample">
                <div class="card card-body">
                <ul>
                    <li><a href="index.php?first_floor">2nd Floor Deluxe Room</a></li>
                    <li><a href="index.php?second_floor">3rd Floor View Room</a></li>
                    <li><a href="index.php?third_floor">4th Floor Superior Room</a></li>
                    <li><a href="index.php?fourth_floor">5th Floor Family Room</a></li>
                </ul>
                </div>
            </div>
        <?php }


        if (isset($_GET['room_mang'])){ ?>
            <li class="active">
                <a href="index.php?room_mang"><em class="fa fa-bed">&nbsp;</em>
                    Manage Rooms
                </a>
            </li>
        <?php } else{?>
            <li>
            <a href="index.php?room_mang"><em class="fa fa-bed">&nbsp;</em>
                    Manage Rooms
                </a>
            </li>
        <?php }


        if (isset($_GET['spa_mang'])){ ?>
            <li class="active">
                <a href="index.php?spa_mang"><em class="fa fa-shower">&nbsp;</em>
                    SPA / Sauna Room
                </a>
            </li>
        <?php } else{?>
            <li>
                <a href="index.php?spa_mang"><em class="fa fa-shower">&nbsp;</em>
                    SPA / Sauna Room
                </a>
            </li>
        <?php }


        if (isset($_GET['complain'])){ ?>
            <li class="active">
                <a href="index.php?complain"><em class="fa fa-comments">&nbsp;</em>
                    Manage Complaints
                </a>
            </li>
        <?php } else{?>
            <li>
                <a href="index.php?complain"><em class="fa fa-comments">&nbsp;</em>
                    Manage Complaints
                </a>
            </li>
        <?php }


        if (isset($_GET['attendance'])){ ?>
            <li class="active">
                <a href="index.php?attendance"><em class="fa fa-clock-o">&nbsp;</em>
                    Attendance
                </a>
            </li>
        <?php } else{?>
            <li>
                <a href="index.php?attendance"><em class="fa fa-clock-o">&nbsp;</em>
                    Attendance
                </a>
            </li>
        <?php }


        if (isset($_GET['history'])){ ?>
            <li class="active">
                <a class="" data-toggle="collapse" href="#collapseExample2" role="button" aria-expanded="false" aria-controls="collapseExample2"><em class="fa fa-history">&nbsp;</em>
                History
                </a>
            </li>
            <div class="collapse" id="collapseExample2">
                <div class="card card-body">
                <ul>
                    <li><a href="index.php?history">Booking History</a></li>
                    <li><a href="index.php?sauna_history">SPA / SAUNA History</a></li>
                </ul>
                </div>
            </div>
        <?php } else{?>
            <li>
            <a class="" data-toggle="collapse" href="#collapseExample2" role="button" aria-expanded="false" aria-controls="collapseExample2"><em class="fa fa-history">&nbsp;</em>
                History
                </a>
            </li>
            <div class="collapse" id="collapseExample2">
                <div class="card card-body">
                <ul>
                    <li><a href="index.php?history">Booking History</a></li>
                    <li><a href="index.php?sauna_history">SPA / SAUNA History</a></li>
                </ul>
                </div>
            </div>
        <?php }        
        
        ?>   

        
    </ul>
</div><!--/.sidebar-->
