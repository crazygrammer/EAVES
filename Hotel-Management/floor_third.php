<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">4th Floor</li>
				<div class="pull-right"> <span id='ct' ></span></div>
			</ol>
		</div><!--/.row-->

		<div class="panel panel-container">
			<div class="row">
					<?php
						$room_nineteen_query = "
						SELECT 
							a.room_id,
							a.room_type_id,
							a.room_no,
							a.status,
							a.check_in_status,
							a.check_out_status,
							a.deleteStatus,
							b.room_type_id,
							b.room_type,
							b.price,
							b.max_person
						FROM 
							room a
						LEFT JOIN 
							room_type b ON b.room_type_id = a.room_type_id
						WHERE 
							deleteStatus = 0 AND a.room_id = 19";
						$rooms_nineteen_result = mysqli_query($connection, $room_nineteen_query); 
					?>
					<?php if (mysqli_num_rows($rooms_nineteen_result) > 0) {
                        	while ($rooms_nineteen = mysqli_fetch_assoc($rooms_nineteen_result)) { 
					?>
					<?php 	if ($rooms_nineteen['status']  == 0) {	

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-blue"></em>
												<a class="btn btn-primary btn-lg" href="index.php?reservation&amp;room_id=19&amp;room_type_id=3">Room 401</a>
											</div>
										</div>
									  </div>';
							}else{

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-red"></em>
												<button class="btn btn-danger btn-lg" id="extendBtnNineteen" data-id="" data-toggle="modal" data-target="#extendHrsNineteen" href="">Room 401</button>
											</div>
										</div>
									  </div>';
							}
					?>
					<?php } } ?>

					<?php
						$room_twenty_query = "
						SELECT 
							a.room_id,
							a.room_type_id,
							a.room_no,
							a.status,
							a.check_in_status,
							a.check_out_status,
							a.deleteStatus,
							b.room_type_id,
							b.room_type,
							b.price,
							b.max_person
						FROM 
							room a
						LEFT JOIN 
							room_type b ON b.room_type_id = a.room_type_id
						WHERE 
							deleteStatus = 0 AND a.room_id = 20";
						$rooms_twenty_result = mysqli_query($connection, $room_twenty_query); 
					?>
					<?php if (mysqli_num_rows($rooms_twenty_result) > 0) {
                        	while ($rooms_twenty = mysqli_fetch_assoc($rooms_twenty_result)) { 
					?>
					<?php 	if ($rooms_twenty['status']  == 0) {	

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-blue"></em>
												<a class="btn btn-primary btn-lg" href="index.php?reservation&amp;room_id=20&amp;room_type_id=3">Room 402</a>
											</div>
										</div>
									  </div>';
							}else{

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-red"></em>
												<button class="btn btn-danger btn-lg" id="extendBtnTwenty" data-id="" data-toggle="modal" data-target="#extendHrsTwenty" href="">Room 402</button>
											</div>
										</div>
									  </div>';
							}
					?>
					<?php } } ?>

					<?php
						$room_twentyone_query = "
							SELECT 
							a.room_id,
							a.room_type_id,
							a.room_no,
							a.status,
							a.check_in_status,
							a.check_out_status,
							a.deleteStatus,
							b.room_type_id,
							b.room_type,
							b.price,
							b.max_person
						FROM 
							room a
						LEFT JOIN 
							room_type b ON b.room_type_id = a.room_type_id
						WHERE 
							deleteStatus = 0 AND a.room_id = 21";
						$rooms_twentyone_result = mysqli_query($connection, $room_twentyone_query); 
					?>
					<?php if (mysqli_num_rows($rooms_twentyone_result) > 0) {
                        	while ($rooms_twentyone = mysqli_fetch_assoc($rooms_twentyone_result)) { 
					?>
					<?php 	if ($rooms_twentyone['status']  == 0) {	

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-blue"></em>
												<a class="btn btn-primary btn-lg" href="index.php?reservation&amp;room_id=21&amp;room_type_id=3">Room 403</a>
											</div>
										</div>
									  </div>';
							}else{

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-red"></em>
												<button class="btn btn-danger btn-lg" id="extendBtnTwentyone" data-id="" data-toggle="modal" data-target="#extendHrsTwentyone" href="">Room 403</button>
											</div>
										</div>
									  </div>';
							}
					?>
					<?php } } ?>
			</div>
			<div class="row">
					<?php
						$room_twentytwo_query = "
							SELECT 
							a.room_id,
							a.room_type_id,
							a.room_no,
							a.status,
							a.check_in_status,
							a.check_out_status,
							a.deleteStatus,
							b.room_type_id,
							b.room_type,
							b.price,
							b.max_person
						FROM 
							room a
						LEFT JOIN 
							room_type b ON b.room_type_id = a.room_type_id
						WHERE 
							deleteStatus = 0 AND a.room_id = 22";
						$rooms_twentytwo_result = mysqli_query($connection, $room_twentytwo_query); 
					?>
					<?php if (mysqli_num_rows($rooms_twentytwo_result) > 0) {
                        	while ($rooms_twentytwo = mysqli_fetch_assoc($rooms_twentytwo_result)) { 
					?>
					<?php 	if ($rooms_twentytwo['status']  == 0) {	

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-blue"></em>
												<a class="btn btn-primary btn-lg" href="index.php?reservation&amp;room_id=22&amp;room_type_id=3">Room 404</a>
											</div>
										</div>
									  </div>';
							}else{

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-red"></em>
												<button class="btn btn-danger btn-lg" id="extendBtnTwentytwo" data-id="" data-toggle="modal" data-target="#extendHrsTwentytwo" href="">Room 404</button>
											</div>
										</div>
									  </div>';
							}
					?>
					<?php } } ?>

					<?php
						$room_twentythree_query = "
							SELECT 
							a.room_id,
							a.room_type_id,
							a.room_no,
							a.status,
							a.check_in_status,
							a.check_out_status,
							a.deleteStatus,
							b.room_type_id,
							b.room_type,
							b.price,
							b.max_person
						FROM 
							room a
						LEFT JOIN 
							room_type b ON b.room_type_id = a.room_type_id
						WHERE 
							deleteStatus = 0 AND a.room_id = 23";
						$rooms_twentythree_result = mysqli_query($connection, $room_twentythree_query); 
					?>
					<?php if (mysqli_num_rows($rooms_twentythree_result) > 0) {
                        	while ($rooms_twentythree = mysqli_fetch_assoc($rooms_twentythree_result)) { 
					?>
					<?php 	if ($rooms_twentythree['status']  == 0) {	

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-blue"></em>
												<a class="btn btn-primary btn-lg" href="index.php?reservation&amp;room_id=23&amp;room_type_id=3">Room 405</a>
											</div>
										</div>
									  </div>';
							}else{

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-red"></em>
												<button class="btn btn-danger btn-lg" id="extendBtnTwentythree" data-id="" data-toggle="modal" data-target="#extendHrsTwentythree" href="">Room 405</button>
											</div>
										</div>
									  </div>';
							}
					?>
					<?php } } ?>

					<?php
						$room_twentyfour_query = "
								SELECT 
								a.room_id,
								a.room_type_id,
								a.room_no,
								a.status,
								a.check_in_status,
								a.check_out_status,
								a.deleteStatus,
								b.room_type_id,
								b.room_type,
								b.price,
								b.max_person
							FROM 
								room a
							LEFT JOIN 
								room_type b ON b.room_type_id = a.room_type_id
							WHERE 
								deleteStatus = 0 AND a.room_id = 24";
						$rooms_twentyfour_result = mysqli_query($connection, $room_twentyfour_query); 
					?>
					<?php if (mysqli_num_rows($rooms_twentyfour_result) > 0) {
                        	while ($rooms_twentyfour = mysqli_fetch_assoc($rooms_twentyfour_result)) { 
					?>
					<?php 	if ($rooms_twentyfour['status']  == 0) {	

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-blue"></em>
												<a class="btn btn-primary btn-lg" href="index.php?reservation&amp;room_id=24&amp;room_type_id=3">Room 406</a>
											</div>
										</div>
									  </div>';
							}else{

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-red"></em>
												<button class="btn btn-danger btn-lg" id="extendBtnTwentyfour" data-id="" data-toggle="modal" data-target="#extendHrsTwentyfour" href="">Room 406</button>
											</div>
										</div>
									  </div>';
							}
					?>
					<?php } } ?>
			</div>
			<div class="row">
					<?php
						$room_twentyfive_query = "
								SELECT 
								a.room_id,
								a.room_type_id,
								a.room_no,
								a.status,
								a.check_in_status,
								a.check_out_status,
								a.deleteStatus,
								b.room_type_id,
								b.room_type,
								b.price,
								b.max_person
							FROM 
								room a
							LEFT JOIN 
								room_type b ON b.room_type_id = a.room_type_id
							WHERE 
								deleteStatus = 0 AND a.room_id = 25";
						$rooms_twentyfive_result = mysqli_query($connection, $room_twentyfive_query); 
					?>
					<?php if (mysqli_num_rows($rooms_twentyfive_result) > 0) {
                        	while ($rooms_twentyfive = mysqli_fetch_assoc($rooms_twentyfive_result)) { 
					?>
					<?php 	if ($rooms_twentyfive['status']  == 0) {	

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-blue"></em>
												<a class="btn btn-primary btn-lg" href="index.php?reservation&amp;room_id=25&amp;room_type_id=3">Room 407</a>
											</div>
										</div>
									  </div>';
							}else{

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-red"></em>
												<button class="btn btn-danger btn-lg" id="extendBtnTwentyfive" data-id="" data-toggle="modal" data-target="#extendHrsTwentyfive" href="">Room 407</button>
											</div>
										</div>
									  </div>';
							}
					?>
					<?php } } ?>
					
					<?php
						$room_twentysix_query = "
								SELECT 
								a.room_id,
								a.room_type_id,
								a.room_no,
								a.status,
								a.check_in_status,
								a.check_out_status,
								a.deleteStatus,
								b.room_type_id,
								b.room_type,
								b.price,
								b.max_person
							FROM 
								room a
							LEFT JOIN 
								room_type b ON b.room_type_id = a.room_type_id
							WHERE 
								deleteStatus = 0 AND a.room_id = 26";
						$rooms_twentysix_result = mysqli_query($connection, $room_twentysix_query); 
					?>
					<?php if (mysqli_num_rows($rooms_twentysix_result) > 0) {
                        	while ($rooms_twentysix = mysqli_fetch_assoc($rooms_twentysix_result)) { 
					?>
					<?php 	if ($rooms_twentysix['status']  == 0) {	

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-blue"></em>
												<a class="btn btn-primary btn-lg" href="index.php?reservation&amp;room_id=26&amp;room_type_id=3">Room 408</a>
											</div>
										</div>
									  </div>';
							}else{

								echo '<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fa fa-xl fa-bed color-red"></em>
												<button class="btn btn-danger btn-lg" id="extendBtnTwentysix" data-id="" data-toggle="modal" data-target="#extendHrsTwentysix" href="">Room 408</button>
											</div>
										</div>
									  </div>';
							}
					?>
					<?php } } ?>
			</div>
			<hr>

			<div class="row">
				<div class="col-xs-6 col-md-2 col-lg-2 no-padding">
					
				</div>

				<div class="col-xs-6 col-md-4 col-lg-4 no-padding">
					<div class="panel panel-red panel-widget border-right">
						<div class="row no-padding"><em class="fa fa-xl fa-money color-blue"></em>
							<div class="large">Php <?php include 'counters/count-thirdfloor.php'?></div>
							<div class="text-muted">Total Earnings 4th Floor</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>


<div id="extendHrsNineteen" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<?php
						$sqlNineteen = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person,
						c.check_out,
						c.payment_status,
						c.total_price,
						c.extend_hours,
						c.booking_date,
						c.booking_id
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					LEFT JOIN
						booking c ON c.room_id = a.room_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 19 AND c.payment_status = 0";
						$resultNineteen = mysqli_query($connection, $sqlNineteen);
						$fetchNineteen = mysqli_fetch_assoc($resultNineteen);

					?>

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Extend</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h4 class="modal-dialog text-center">Room 401 is about to End, Do you want to Extend?</h4>
                            <form role="form" id="extendFormNineteen">
                                <div class="payment-response"></div>
                                <div class="form-group col-lg-12">
                                    <label>Extended Minutes</label>
                                    <input type="number" class="form-control" id="extend_hours_nineteen"
                                           placeholder="Please Enter Minutes Here..">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Extend Price</label>
                                    <input type="number" class="form-control" id="extend_price_nineteen"
                                           placeholder="Please Enter Amounts Here..">
                                </div>
                                <input type="hidden" id="getExtendIdNineteen" value="<?php echo $fetchNineteen['booking_id'] ?>">
                                <button type="submit" class="btn btn-primary pull-right">Extend</button>
                                <!-- <button type="" class="btn btn-primary pull-right">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


	<div id="extendHrsTwenty" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<?php
						$sqlTwenty = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person,
						c.check_out,
						c.payment_status,
						c.total_price,
						c.extend_hours,
						c.booking_date,
						c.booking_id
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					LEFT JOIN
						booking c ON c.room_id = a.room_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 20 AND c.payment_status = 0";
						$resultTwenty = mysqli_query($connection, $sqlTwenty);
						$fetchTwenty = mysqli_fetch_assoc($resultTwenty);

					?>

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Extend</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h4 class="modal-dialog text-center">Room 402 is about to End, Do you want to Extend?</h4>
                            <form role="form" id="extendFormTwenty">
                                <div class="payment-response"></div>
                                <div class="form-group col-lg-12">
                                    <label>Extended Minutes</label>
                                    <input type="number" class="form-control" id="extend_hours_twenty"
                                           placeholder="Please Enter Minutes Here..">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Extend Price</label>
                                    <input type="number" class="form-control" id="extend_price_twenty"
                                           placeholder="Please Enter Amounts Here..">
                                </div>
                                <input type="hidden" id="getExtendIdTwenty" value="<?php echo $fetchTwenty['booking_id'] ?>">
                                <button type="submit" class="btn btn-primary pull-right">Extend</button>
                                <!-- <button type="" class="btn btn-primary pull-right">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


	<div id="extendHrsTwentyone" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<?php
						$sqlTwentyone = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person,
						c.check_out,
						c.payment_status,
						c.total_price,
						c.extend_hours,
						c.booking_date,
						c.booking_id
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					LEFT JOIN
						booking c ON c.room_id = a.room_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 21 AND c.payment_status = 0";
						$resultTwentyone = mysqli_query($connection, $sqlTwentyone);
						$fetchTwentyone = mysqli_fetch_assoc($resultTwentyone);

					?>

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Extend</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h4 class="modal-dialog text-center">Room 403 is about to End, Do you want to Extend?</h4>
                            <form role="form" id="extendFormTwentyone">
                                <div class="payment-response"></div>
                                <div class="form-group col-lg-12">
                                    <label>Extended Minutes</label>
                                    <input type="number" class="form-control" id="extend_hours_twentyone"
                                           placeholder="Please Enter Minutes Here..">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Extend Price</label>
                                    <input type="number" class="form-control" id="extend_price_twentyone"
                                           placeholder="Please Enter Amounts Here..">
                                </div>
                                <input type="hidden" id="getExtendIdTwentyone" value="<?php echo $fetchTwentyone['booking_id'] ?>">
                                <button type="submit" class="btn btn-primary pull-right">Extend</button>
                                <!-- <button type="" class="btn btn-primary pull-right">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


	<div id="extendHrsTwentytwo" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<?php
						$sqlTwentytwo = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person,
						c.check_out,
						c.payment_status,
						c.total_price,
						c.extend_hours,
						c.booking_date,
						c.booking_id
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					LEFT JOIN
						booking c ON c.room_id = a.room_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 22 AND c.payment_status = 0";
						$resultTwentytwo = mysqli_query($connection, $sqlTwentytwo);
						$fetchTwentytwo = mysqli_fetch_assoc($resultTwentytwo);

					?>

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Extend</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h4 class="modal-dialog text-center">Room 404 is about to End, Do you want to Extend?</h4>
                            <form role="form" id="extendFormTwentytwo">
                                <div class="payment-response"></div>
                                <div class="form-group col-lg-12">
                                    <label>Extended Minutes</label>
                                    <input type="number" class="form-control" id="extend_hours_twentytwo"
                                           placeholder="Please Enter Minutes Here..">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Extend Price</label>
                                    <input type="number" class="form-control" id="extend_price_twentytwo"
                                           placeholder="Please Enter Amounts Here..">
                                </div>
                                <input type="hidden" id="getExtendIdTwentytwo" value="<?php echo $fetchTwentytwo['booking_id'] ?>">
                                <button type="submit" class="btn btn-primary pull-right">Extend</button>
                                <!-- <button type="" class="btn btn-primary pull-right">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


	<div id="extendHrsTwentythree" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<?php
						$sqlTwentythree = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person,
						c.check_out,
						c.payment_status,
						c.total_price,
						c.extend_hours,
						c.booking_date,
						c.booking_id
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					LEFT JOIN
						booking c ON c.room_id = a.room_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 23 AND c.payment_status = 0";
						$resultTwentythree = mysqli_query($connection, $sqlTwentythree);
						$fetchTwentythree = mysqli_fetch_assoc($resultTwentythree);

					?>

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Extend</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h4 class="modal-dialog text-center">Room 405 is about to End, Do you want to Extend?</h4>
                            <form role="form" id="extendFormTwentythree">
                                <div class="payment-response"></div>
                                <div class="form-group col-lg-12">
                                    <label>Extended Minutes</label>
                                    <input type="number" class="form-control" id="extend_hours_twentythree"
                                           placeholder="Please Enter Minutes Here..">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Extend Price</label>
                                    <input type="number" class="form-control" id="extend_price_twentythree"
                                           placeholder="Please Enter Amounts Here..">
                                </div>
                                <input type="hidden" id="getExtendIdTwentythree" value="<?php echo $fetchTwentythree['booking_id'] ?>">
                                <button type="submit" class="btn btn-primary pull-right">Extend</button>
                                <!-- <button type="" class="btn btn-primary pull-right">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


	<div id="extendHrsTwentyfour" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<?php
						$sqlTwentyfour = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person,
						c.check_out,
						c.payment_status,
						c.total_price,
						c.extend_hours,
						c.booking_date,
						c.booking_id
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					LEFT JOIN
						booking c ON c.room_id = a.room_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 24 AND c.payment_status = 0";
						$resultTwentyfour = mysqli_query($connection, $sqlTwentyfour);
						$fetchTwentyfour = mysqli_fetch_assoc($resultTwentyfour);

					?>

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Extend</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h4 class="modal-dialog text-center">Room 406 is about to End, Do you want to Extend?</h4>
                            <form role="form" id="extendFormTwentyfour">
                                <div class="payment-response"></div>
                                <div class="form-group col-lg-12">
                                    <label>Extended Minutes</label>
                                    <input type="number" class="form-control" id="extend_hours_twentyfour"
                                           placeholder="Please Enter Minutes Here..">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Extend Price</label>
                                    <input type="number" class="form-control" id="extend_price_twentyfour"
                                           placeholder="Please Enter Amounts Here..">
                                </div>
                                <input type="hidden" id="getExtendIdTwentyfour" value="<?php echo $fetchTwentyfour['booking_id'] ?>">
                                <button type="submit" class="btn btn-primary pull-right">Extend</button>
                                <!-- <button type="" class="btn btn-primary pull-right">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


	<div id="extendHrsTwentyfive" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<?php
						$sqlTwentyfive = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person,
						c.check_out,
						c.payment_status,
						c.total_price,
						c.extend_hours,
						c.booking_date,
						c.booking_id
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					LEFT JOIN
						booking c ON c.room_id = a.room_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 25 AND c.payment_status = 0";
						$resultTwentyfive = mysqli_query($connection, $sqlTwentyfive);
						$fetchTwentyfive = mysqli_fetch_assoc($resultTwentyfive);

					?>

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Extend</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h4 class="modal-dialog text-center">Room 407 is about to End, Do you want to Extend?</h4>
                            <form role="form" id="extendFormTwentyfive">
                                <div class="payment-response"></div>
                                <div class="form-group col-lg-12">
                                    <label>Extended Minutes</label>
                                    <input type="number" class="form-control" id="extend_hours_twentyfive"
                                           placeholder="Please Enter Minutes Here..">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Extend Price</label>
                                    <input type="number" class="form-control" id="extend_price_twentyfive"
                                           placeholder="Please Enter Amounts Here..">
                                </div>
                                <input type="hidden" id="getExtendIdTwentyfive" value="<?php echo $fetchTwentyfive['booking_id'] ?>">
                                <button type="submit" class="btn btn-primary pull-right">Extend</button>
                                <!-- <button type="" class="btn btn-primary pull-right">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


	<div id="extendHrsTwentysix" class="modal fade" role="dialog">
        <div class="modal-dialog">
			<?php
						$sqlTwentysix = "
						SELECT 
						a.room_id,
						a.room_type_id,
						a.room_no,
						a.status,
						a.check_in_status,
						a.check_out_status,
						a.deleteStatus,
						b.room_type_id,
						b.room_type,
						b.price,
						b.max_person,
						c.check_out,
						c.payment_status,
						c.total_price,
						c.extend_hours,
						c.booking_date,
						c.booking_id
					FROM 
						room a
					LEFT JOIN 
						room_type b ON b.room_type_id = a.room_type_id
					LEFT JOIN
						booking c ON c.room_id = a.room_id
					WHERE 
						deleteStatus = 0 AND a.room_id = 26 AND c.payment_status = 0";
						$resultTwentysix = mysqli_query($connection, $sqlTwentysix);
						$fetchTwentysix = mysqli_fetch_assoc($resultTwentysix);

					?>

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center"><b>Extend</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h4 class="modal-dialog text-center">Room 408 is about to End, Do you want to Extend?</h4>
                            <form role="form" id="extendFormTwentysix">
                                <div class="payment-response"></div>
                                <div class="form-group col-lg-12">
                                    <label>Extended Minutes</label>
                                    <input type="number" class="form-control" id="extend_hours_twentysix"
                                           placeholder="Please Enter Minutes Here..">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Extend Price</label>
                                    <input type="number" class="form-control" id="extend_price_twentysix"
                                           placeholder="Please Enter Amounts Here..">
                                </div>
                                <input type="hidden" id="getExtendIdTwentysix" value="<?php echo $fetchTwentysix['booking_id'] ?>">
                                <button type="submit" class="btn btn-primary pull-right">Extend</button>
                                <!-- <button type="" class="btn btn-primary pull-right">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>