<?php date_default_timezone_set('Asia/Manila'); ?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#">
                    <em class="fa fa-history"></em>
                </a></li>
            <li class="active">SPA / SAUNA History</li>
        </ol>
    </div><!--/.row-->

    <br>

    <div class="row">
        <div class="col-lg-12">
            <div id="success"></div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">SPA / SAUNA History

                </div>
                <div class="panel-body">
                    <?php
                    if (isset($_GET['error'])) {
                        echo "<div class='alert alert-danger'>
                                <span class='glyphicon glyphicon-info-sign'></span> &nbsp; Error on Delete !
                            </div>";
                    }
                    if (isset($_GET['success'])) {
                        echo "<div class='alert alert-success'>
                                <span class='glyphicon glyphicon-info-sign'></span> &nbsp; Successfully Delete !
                            </div>";
                    }
                    ?>
                    <table class="table table-striped table-bordered table-responsive" cellspacing="0" width="100%"
                           id="rooms">
                        <thead>
                        <tr>
                            <th>SPA No</th>
                            <th>Customer Name</th>
                            <th>Check In</th>
                            <th>Check Out</th>
                            <th>Additional</th>
                            <th>Sauna/SPA Price</th>
                            <th>Add Ons Price</th>
                            <th>Total Price</th>
                            <th>Employee</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $userID = $_SESSION['user_id'];
                        // echo $userID;
                        if($_SESSION['restrictions'] == 0) {
                        $room_query = "
                                SELECT 
                                    a.id,
                                    a.customer_name,
                                    a.check_in,
                                    a.check_out,
                                    a.additional,
                                    a.sauna_price,
                                    a.add_price,
                                    a.total_price,
                                    u.name 
                                FROM 
                                    spa_sauna a 
                                LEFT JOIN 
                                    user u ON u.id = a.user_id
                                WHERE
                                    user_id = '$userID'  
                                    ";
                        $rooms_result = mysqli_query($connection, $room_query);
                        if (mysqli_num_rows($rooms_result) > 0) {
                            while ($rooms = mysqli_fetch_assoc($rooms_result)) { ?>
                                <tr>
                                    <td><?php echo $rooms['id'] ?></td>
                                    <td><?php echo $rooms['customer_name'] ?></td>
                                    <td><?php echo date('F j, Y, g:i a', strtotime($rooms['check_in'])) ?></td>
                                    <td><?php echo date('F j, Y, g:i a', strtotime($rooms['check_out'])) ?></td>
                                    <td><?php echo $rooms['additional'] ?></td>
                                    <td><?php echo $rooms['sauna_price'] ?></td>
                                    <td><?php echo $rooms['add_price'] ?></td>
                                    <td><?php echo $rooms['total_price'] ?></td>
                                    <td><?php echo $rooms['name'] ?></td>
                                </tr>
                            <?php }
                        } else {
                            echo "No Rooms";
                        }
                    } else {
                        $room_query2 = "
                        SELECT 
                           a.id,
                           a.customer_name,
                           a.check_in,
                           a.check_out,
                           a.additional,
                           a.sauna_price,
                           a.add_price,
                           a.total_price,
                           u.name 
                        FROM 
                            spa_sauna a 
                        LEFT JOIN 
                            user u ON u.id = a.user_id
                            ";
                $rooms_result2 = mysqli_query($connection, $room_query2);
                if (mysqli_num_rows($rooms_result2) > 0) {
                    while ($rooms2 = mysqli_fetch_assoc($rooms_result2)) { ?>
                        <tr>
                            <td><?php echo $rooms2['id'] ?></td>
                            <td><?php echo $rooms2['customer_name'] ?></td>
                            <td><?php echo date('F j, Y, g:i a', strtotime($rooms2['check_in'])) ?></td>
                            <td><?php echo date('F j, Y, g:i a', strtotime($rooms2['check_out'])) ?></td>
                            <td><?php echo $rooms2['additional'] ?></td>
                            <td><?php echo $rooms2['sauna_price'] ?></td>
                            <td><?php echo $rooms2['add_price'] ?></td>
                            <td><?php echo $rooms2['total_price'] ?></td>
                            <td><?php echo $rooms2['name'] ?></td>
                        </tr>
                    <?php }
                } else {
                    echo "No Rooms";
                }
                    } ?>

                        </tbody>
                    </table>
                     <?php
                            if($_SESSION['restrictions'] == 0){
                                $date = date('Y-m-d');
                                $query              = "SELECT SUM(a.total_price) AS total_count FROM spa_sauna a LEFT JOIN user b ON b.id = a.user_id WHERE a.user_id = $userID AND date = '$date'";
                                $rooms_result       = mysqli_query($connection, $query);
                                $customer_details   = mysqli_fetch_assoc($rooms_result);
                            }else{
                                $date = date('Y-m-d');
                                $query2              = "SELECT SUM(a.total_price) AS total_count2 FROM spa_sauna a LEFT JOIN user b ON b.id = a.user_id WHERE date = '$date'";
                                $rooms_result2       = mysqli_query($connection, $query2);
                                $customer_details2   = mysqli_fetch_assoc($rooms_result2);
                            }
                    ?>
                            <?php if($_SESSION['restrictions'] == 0){ ?>
	                        <h4 style="font-weight: bold">Total Amount : <span id="total"><?= $customer_details['total_count'] ?></span> /-</h4>
                            <?php }else{ ?>
                            <h4 style="font-weight: bold">Total Amount : <span id="total"><?= $customer_details2['total_count2'] ?></span> /-</h4>
                            <?php } ?>
                        </div>
                    
                   
                </div>
            </div>

        </div>
    </div>

</div>    <!--/.main-->